import { beforeAll } from "vitest";
import i18n from "./src/i18n";

beforeAll((): void => {
  i18n.setup();
  global.plugins = [i18n.vueI18n];
});
