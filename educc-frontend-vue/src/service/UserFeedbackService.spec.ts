// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockGet, mockPost } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForLatestUserFeedback, urlForNewUserFeedback } from "@/api/make_rest_urls";
import { SaveUserFeedbackRequest } from "@/dto/SaveUserFeedbackRequest";
import { SaveUserFeedbackResponse } from "@/dto/SaveUserFeedbackResponse";
import { defaultUserFeedback } from "@/utils/TestObjectFactory";
import { getLatestUserFeedback, saveUserFeedback } from "./UserFeedbackService";

describe("UserFeedbackService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("getLatestUserFeedback forwards the fetch result", async (): Promise<void> => {
    mockGet(urlForLatestUserFeedback(50)).willResolve([
      defaultUserFeedback("2024-09-27T11:22:08"),
      defaultUserFeedback(),
    ]);
    await expect(getLatestUserFeedback()).resolves.toEqual([
      defaultUserFeedback("2024-09-27T11:22:08"),
      defaultUserFeedback(),
    ]);
  });

  test("saveUserFeedback calls fetch with the correct arguments", async (): Promise<void> => {
    const userFeedback = defaultUserFeedback();
    const saveUserFeedbackRequest = new SaveUserFeedbackRequest(userFeedback.feedbackText);
    const fetchMock = mockPost(urlForNewUserFeedback()).willResolve(
      new SaveUserFeedbackResponse("OK", "message", userFeedback),
    );

    const fetchedUserFeedback = await saveUserFeedback(userFeedback.feedbackText);

    expect(fetchMock).toHaveFetchedTimes(1);
    expect(fetchMock).toHaveFetchedWithBody(JSON.stringify(saveUserFeedbackRequest));
    expect(fetchedUserFeedback).toStrictEqual(userFeedback);
  });
});
