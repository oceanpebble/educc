// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import type { FetchCurrencyListResponse } from "@/dto/FetchCurrencyListResponse";
import type { Currency } from "@/types/Currency";
import { urlForAllCurrencies } from "@/api/make_rest_urls";

export async function fetchCurrencyList(): Promise<Currency[]> {
  const response = (await (await fetch(urlForAllCurrencies())).json()) as FetchCurrencyListResponse;
  return response.currencies;
}
