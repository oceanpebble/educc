// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { SaveUserFeedbackResponse } from "@/dto/SaveUserFeedbackResponse";
import type { UserFeedback } from "@/types/UserFeedback";
import { urlForLatestUserFeedback, urlForNewUserFeedback } from "@/api/make_rest_urls";
import { SaveUserFeedbackRequest } from "@/dto/SaveUserFeedbackRequest";
import { authHeader } from "@/security/JWTHandler";

export async function saveUserFeedback(feedbackText: string): Promise<UserFeedback> {
  const saveUserFeedbackRequest = new SaveUserFeedbackRequest(feedbackText);
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: authHeader(),
    },
    body: JSON.stringify(saveUserFeedbackRequest),
  };
  const response = (await (await fetch(urlForNewUserFeedback(), requestOptions)).json()) as SaveUserFeedbackResponse;
  return response.userFeedback;
}

export async function getLatestUserFeedback(): Promise<UserFeedback[]> {
  const response = (await (await fetch(urlForLatestUserFeedback(50))).json()) as UserFeedback[];
  return response;
}
