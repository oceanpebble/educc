// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { SaveStoreResponse } from "@/dto/SaveStoreResponse";
import type { Store } from "@/types/Store";
import { urlForAllStores, urlForNewStore, urlForSingleStore } from "@/api/make_rest_urls";
import { SaveStoreRequest } from "@/dto/SaveStoreRequest";
import { authHeader } from "@/security/JWTHandler";

export async function getStoreById(id: number): Promise<Store> {
  return (await (await fetch(urlForSingleStore(id))).json()) as Store;
}

export async function fetchStoreList(): Promise<Store[]> {
  return (await (await fetch(urlForAllStores())).json()) as Store[];
}

export async function saveStore(
  city: string,
  country: string,
  houseNumber: string,
  name: string,
  state: string,
  street: string,
  zip: string,
): Promise<Store> {
  const saveStoreRequest = new SaveStoreRequest(city, country, houseNumber, name, state, street, zip);

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: authHeader(),
    },
    body: JSON.stringify(saveStoreRequest),
  };
  const response = (await (await fetch(urlForNewStore(), requestOptions)).json()) as SaveStoreResponse;
  return response.store;
}
