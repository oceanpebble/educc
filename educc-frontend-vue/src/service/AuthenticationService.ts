// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { urlForDeleteUser, urlForLoginUser, urlForRegisterUser } from "@/api/make_rest_urls";
import { DeleteUserRequest } from "@/dto/DeleteUserRequest";
import { DeleteUserResponse } from "@/dto/DeleteUserResponse";
import { LoginUserRequest } from "@/dto/LoginUserRequest";
import { LoginUserResponse } from "@/dto/LoginUserResponse";
import { RegisterUserRequest } from "@/dto/RegisterUserRequest";
import { RegisterUserResponse } from "@/dto/RegisterUserResponse";
import { authHeader, extractUsernameFromJWT } from "@/security/JWTHandler";

export function isUserLoggedIn(): boolean {
  return !!localStorage.getItem("user");
}

export async function registerUser(username: string, password: string): Promise<boolean> {
  const registerUserRequest = new RegisterUserRequest(username, password);
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(registerUserRequest),
  };
  const response = (await (await fetch(urlForRegisterUser(), requestOptions)).json()) as RegisterUserResponse;
  return response.httpStatus === "OK";
}

export async function loginUser(username: string, password: string): Promise<boolean> {
  const loginUserRequest = new LoginUserRequest(username, password);
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(loginUserRequest),
  };
  const response = (await (await fetch(urlForLoginUser(), requestOptions)).json()) as LoginUserResponse;

  if (response?.jwt) {
    localStorage.setItem("user", response.jwt);
    window.dispatchEvent(new CustomEvent("user-logged-in"));
    return true;
  }

  return false;
}

export function logoutUser(): void {
  localStorage.removeItem("user");
  window.dispatchEvent(new CustomEvent("user-logged-out"));
}

export async function deleteUser(): Promise<boolean> {
  const username = extractUsernameFromJWT();

  if (!username) {
    return false;
  }

  const deleteUserRequest = new DeleteUserRequest(username);
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json", Authorization: authHeader() },
    body: JSON.stringify(deleteUserRequest),
  };
  const response = (await (await fetch(urlForDeleteUser(), requestOptions)).json()) as DeleteUserResponse;
  const deletionSuccessful = response.httpStatus === "OK";

  if (deletionSuccessful) {
    logoutUser();
  }

  return deletionSuccessful;
}
