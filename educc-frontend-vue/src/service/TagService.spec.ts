// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockGet } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForAllTags } from "@/api/make_rest_urls";
import { FetchTagListResponse } from "@/dto/FetchTagListResponse";
import { fetchTagList } from "@/service/TagService";
import { defaultTag } from "@/utils/TestObjectFactory";

describe("TagService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("fetchTagList forwards the fetch result", async (): Promise<void> => {
    mockGet(urlForAllTags()).willResolve(
      new FetchTagListResponse("OK", "message", [defaultTag(), defaultTag("tag 2")]),
    );
    await expect(fetchTagList()).resolves.toEqual([defaultTag(), defaultTag("tag 2")]);
  });
});
