// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockGet } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForAllCurrencies } from "@/api/make_rest_urls";
import { FetchCurrencyListResponse } from "@/dto/FetchCurrencyListResponse";
import { fetchCurrencyList } from "@/service/CurrencyService";
import { defaultCurrency } from "@/utils/TestObjectFactory";

describe("CurrencyService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("fetchCurrencyList forwards the fetch result", async (): Promise<void> => {
    mockGet(urlForAllCurrencies()).willResolve(
      new FetchCurrencyListResponse("OK", "message", [defaultCurrency(), defaultCurrency("currency 2")]),
    );
    await expect(fetchCurrencyList()).resolves.toEqual([defaultCurrency(), defaultCurrency("currency 2")]);
  });
});
