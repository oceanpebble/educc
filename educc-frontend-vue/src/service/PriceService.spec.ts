// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockPost } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForNewPrice } from "@/api/make_rest_urls";
import { SavePriceRequest } from "@/dto/SavePriceRequest";
import { SavePriceResponse } from "@/dto/SavePriceResponse";
import { savePrice } from "@/service/PriceService";
import { defaultPrice } from "@/utils/TestObjectFactory";

describe("PriceService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("savePrice calls fetch with the correct arguments", async (): Promise<void> => {
    const price = defaultPrice();
    const savePriceRequest = new SavePriceRequest("1", "1337", "2", "2023-02-04", "3");
    const fetchMock = mockPost(urlForNewPrice()).willResolve(new SavePriceResponse("OK", "message", price));

    const fetchedPrice = await savePrice(
      savePriceRequest.currencyId,
      savePriceRequest.priceValue,
      savePriceRequest.productId,
      savePriceRequest.purchaseDate,
      savePriceRequest.storeId,
    );

    expect(fetchMock).toHaveFetchedTimes(1);
    expect(fetchMock).toHaveFetchedWithBody(JSON.stringify(savePriceRequest));
    expect(fetchedPrice).toStrictEqual(price);
  });
});
