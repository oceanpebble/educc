// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { SaveProductResponse } from "@/dto/SaveProductResponse";
import type { Product } from "@/types/Product";
import { urlForAllProducts, urlForNewProduct, urlForSingleProduct } from "@/api/make_rest_urls";
import { SaveProductRequest } from "@/dto/SaveProductRequest";
import { authHeader } from "@/security/JWTHandler";

export async function getProductById(id: number): Promise<Product> {
  return (await (await fetch(urlForSingleProduct(id))).json()) as Product;
}

export async function fetchProductList(): Promise<Product[]> {
  return (await (await fetch(urlForAllProducts())).json()) as Product[];
}

export async function saveProduct(
  brandName: string,
  name: string,
  storeId: string,
  tagIds: string[],
  volumeInMilliliter: string,
  weightInGram: string,
): Promise<Product> {
  const saveProductRequest = new SaveProductRequest(
    brandName,
    name,
    Number(storeId),
    tagIds.map((id): number => {
      return Number(id);
    }),
    Number(volumeInMilliliter),
    Number(weightInGram),
  );
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: authHeader(),
    },
    body: JSON.stringify(saveProductRequest),
  };
  const response = (await (await fetch(urlForNewProduct(), requestOptions)).json()) as SaveProductResponse;
  return response.product;
}
