// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockGet, mockPost } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForAllProducts, urlForNewProduct, urlForSingleProduct } from "@/api/make_rest_urls";
import { SaveProductRequest } from "@/dto/SaveProductRequest";
import { SaveProductResponse } from "@/dto/SaveProductResponse";
import { fetchProductList, getProductById, saveProduct } from "@/service/ProductService";
import { defaultProduct } from "@/utils/TestObjectFactory";

describe("ProductService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("getProductById forwards the correct product", async (): Promise<void> => {
    mockGet(urlForSingleProduct(1)).willResolve(defaultProduct());
    mockGet(urlForSingleProduct(2)).willResolve(defaultProduct(2, "store 2"));
    await expect(getProductById(1)).resolves.toEqual(defaultProduct());
  });

  test("fetchProductList forwards the fetch result", async (): Promise<void> => {
    mockGet(urlForAllProducts()).willResolve([defaultProduct(), defaultProduct(2, "product 2")]);
    await expect(fetchProductList()).resolves.toEqual([defaultProduct(), defaultProduct(2, "product 2")]);
  });

  test("saveProduct calls fetch with the correct arguments", async (): Promise<void> => {
    const product = defaultProduct();
    const saveProductRequest = new SaveProductRequest("container brand", "product name", 1, [2, 3], 1000, 1000);
    const fetchMock = mockPost(urlForNewProduct()).willResolve(new SaveProductResponse("OK", "message", product));

    const fetchedProduct = await saveProduct(
      saveProductRequest.brandName,
      saveProductRequest.name,
      String(saveProductRequest.storeId),
      saveProductRequest.tagIds.map((tag): string => {
        return String(tag);
      }),
      String(saveProductRequest.volumeInMilliliter),
      String(saveProductRequest.weightInGram),
    );

    expect(fetchMock).toHaveFetchedTimes(1);
    expect(fetchMock).toHaveFetchedWithBody(JSON.stringify(saveProductRequest));
    expect(fetchedProduct).toStrictEqual(product);
  });
});
