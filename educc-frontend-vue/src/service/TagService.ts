// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import type { FetchTagListResponse } from "@/dto/FetchTagListResponse";
import type { Tag } from "@/types/Tag";
import { urlForAllTags } from "@/api/make_rest_urls";

export async function fetchTagList(): Promise<Tag[]> {
  const response = (await (await fetch(urlForAllTags())).json()) as FetchTagListResponse;
  return response.tags;
}
