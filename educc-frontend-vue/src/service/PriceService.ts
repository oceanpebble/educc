// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import type { SavePriceResponse } from "@/dto/SavePriceResponse";
import type { Price } from "@/types/Price";
import { urlForNewPrice } from "@/api/make_rest_urls";
import { SavePriceRequest } from "@/dto/SavePriceRequest";
import { authHeader } from "@/security/JWTHandler";

export async function savePrice(
  currencyId: string,
  priceValue: string,
  productId: string,
  purchaseDate: string,
  storeId: string,
): Promise<Price> {
  const savePriceRequest = new SavePriceRequest(currencyId, priceValue, productId, purchaseDate, storeId);
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: authHeader(),
    },
    body: JSON.stringify(savePriceRequest),
  };
  const response = (await (await fetch(urlForNewPrice(), requestOptions)).json()) as SavePriceResponse;
  return response.price;
}
