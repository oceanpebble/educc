// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockPost } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test, vi } from "vitest";
import { urlForDeleteUser, urlForLoginUser, urlForRegisterUser } from "@/api/make_rest_urls";
import { DeleteUserResponse } from "@/dto/DeleteUserResponse";
import { LoginUserResponse } from "@/dto/LoginUserResponse";
import { RegisterUserResponse } from "@/dto/RegisterUserResponse";
import * as JWTHandler from "@/security/JWTHandler";
import { deleteUser, loginUser, logoutUser, registerUser } from "@/service/AuthenticationService";

describe("AuthenticationService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("registerUser returns true for a valid request", async (): Promise<void> => {
    mockPost(urlForRegisterUser()).willResolve(new RegisterUserResponse("OK", "success"));
    await expect(registerUser("", "")).resolves.toBe(true);
  });

  test("registerUser returns false for an ivalid request", async (): Promise<void> => {
    mockPost(urlForRegisterUser()).willResolve(new RegisterUserResponse("BAD_REQUEST", "failure"));
    await expect(registerUser("", "")).resolves.toBe(false);
  });

  test("registerUser returns false if POST fails", async (): Promise<void> => {
    mockPost(urlForRegisterUser()).willFail();
    await expect(registerUser("", "")).resolves.toBe(false);
  });

  test("loginUser returns true for a valid request", async (): Promise<void> => {
    mockPost(urlForLoginUser()).willResolve(new LoginUserResponse("OK", "message", "a jwt"));
    await expect(loginUser("", "")).resolves.toBe(true);
  });

  test("loginUser returns false for an ivalid request", async (): Promise<void> => {
    mockPost(urlForLoginUser()).willResolve(new LoginUserResponse("BAD_REQUEST", "message", null));
    await expect(loginUser("", "")).resolves.toBe(false);
  });

  test("loginUser returns false if POST fails", async (): Promise<void> => {
    mockPost(urlForLoginUser()).willFail();
    await expect(loginUser("", "")).resolves.toBe(false);
  });

  test("logoutUser removes JWT from local storage", (): void => {
    localStorage.setItem("user", "bliblablub");
    logoutUser();
    expect(localStorage.getItem("user")).toBe(null);
  });

  test("logoutUser dispatches window event", (): void => {
    const dispatchEventSpy = vi.spyOn(window, "dispatchEvent");
    logoutUser();
    expect(dispatchEventSpy).toHaveBeenCalledOnce();
  });

  test("deleteUser returns true for a valid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("OK", "success"));
    await expect(deleteUser()).resolves.toBe(true);
  });

  test("deleteUser returns false for an invalid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("BAD_REQUEST", "failure"));
    await expect(deleteUser()).resolves.toBe(false);
  });

  test("deleteUser removes JWT from local storage for a valid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    localStorage.setItem("user", "bliblablub");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("OK", "success"));

    await deleteUser();

    expect(localStorage.getItem("user")).toBe(null);
  });

  test("deleteUser does not remove JWT from local storage for an invalid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    localStorage.setItem("user", "bliblablub");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("BAD_REQUEST", "failure"));

    await deleteUser();

    expect(localStorage.getItem("user")).toBe("bliblablub");
  });

  test("deleteUser dispatches window event for a valid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    const dispatchEventSpy = vi.spyOn(window, "dispatchEvent");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("OK", "success"));

    await deleteUser();

    expect(dispatchEventSpy).toHaveBeenCalledOnce();
  });

  test("deleteUser does not dispatch window event for an invalid request", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce("bliblablub");
    const dispatchEventSpy = vi.spyOn(window, "dispatchEvent");
    mockPost(urlForDeleteUser()).willResolve(new DeleteUserResponse("BAD_REQUEST", "failure"));

    await deleteUser();

    expect(dispatchEventSpy).not.toHaveBeenCalled();
  });

  test("deleteUser does not dispatch window event if there is no logged in user", async (): Promise<void> => {
    vi.spyOn(JWTHandler, "extractUsernameFromJWT").mockReturnValueOnce(undefined);
    const dispatchEventSpy = vi.spyOn(window, "dispatchEvent");

    const deleteResult = await deleteUser();

    expect(deleteResult).toBe(false);
    expect(dispatchEventSpy).not.toHaveBeenCalled();
  });
});
