// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
/**
 * @vitest-environment happy-dom
 */
import "node-fetch";
import { mockFetch, mockGet, mockPost } from "vi-fetch";
import "vi-fetch/setup";
import { beforeEach, describe, expect, test } from "vitest";
import { urlForAllStores, urlForNewStore, urlForSingleStore } from "@/api/make_rest_urls";
import { SaveStoreRequest } from "@/dto/SaveStoreRequest";
import { SaveStoreResponse } from "@/dto/SaveStoreResponse";
import { fetchStoreList, getStoreById, saveStore } from "@/service/StoreService";
import { defaultStore } from "@/utils/TestObjectFactory";

describe("StoreService", (): void => {
  beforeEach((): void => {
    mockFetch.clearAll();
  });

  test("getStoreById forwards the correct store", async (): Promise<void> => {
    mockGet(urlForSingleStore(1)).willResolve(defaultStore());
    mockGet(urlForSingleStore(2)).willResolve(defaultStore(1, "store 2"));
    await expect(getStoreById(1)).resolves.toEqual(defaultStore());
  });

  test("fetchStoreList forwards the fetch result", async (): Promise<void> => {
    mockGet(urlForAllStores()).willResolve([defaultStore(), defaultStore(1, "store 2")]);
    await expect(fetchStoreList()).resolves.toEqual([defaultStore(), defaultStore(1, "store 2")]);
  });

  test("saveStore calls fetch with the correct arguments", async (): Promise<void> => {
    const store = defaultStore();
    const saveStoreRequest = new SaveStoreRequest(
      store.name,
      store.address.street,
      store.address.houseNumber,
      store.address.zip,
      store.address.city,
      store.address.state,
      store.address.country,
    );
    const fetchMock = mockPost(urlForNewStore()).willResolve(new SaveStoreResponse("OK", "message", store));

    const fetchedStore = await saveStore(
      store.name,
      store.address.street,
      store.address.houseNumber,
      store.address.zip,
      store.address.city,
      store.address.state,
      store.address.country,
    );

    expect(fetchMock).toHaveFetchedTimes(1);
    expect(fetchMock).toHaveFetchedWithBody(JSON.stringify(saveStoreRequest));
    expect(fetchedStore).toStrictEqual(store);
  });
});
