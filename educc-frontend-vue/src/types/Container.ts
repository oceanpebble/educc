// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

export class Container {
  readonly brand: string;
  readonly volumeInMilliliter: number;
  readonly weightInGram: number;

  constructor(brand: string, volumeInMilliliter: number, weightInGram: number) {
    this.brand = brand;
    this.volumeInMilliliter = volumeInMilliliter;
    this.weightInGram = weightInGram;
  }
}
