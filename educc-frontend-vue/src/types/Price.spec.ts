// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test } from "vitest";
import * as PriceGetters from "@/types/Price";
import { defaultPrice, defaultPriceWithOnlyValidFromDate } from "@/utils/TestObjectFactory";

describe("Price", (): void => {
  test("priceInclUnit returns the formatted price with its unit", (): void => {
    const price = defaultPrice();
    const actualValue = PriceGetters.priceInclUnit(price);
    expect(actualValue).toEqual("0.99 currency name");
  });

  test("priceValidity for a price with validFrom and validTo dates returns a string with both dates", (): void => {
    const price = defaultPrice();
    const actualValue = PriceGetters.priceValidity(price);
    expect(actualValue).toEqual("2022-12-01 to 2022-12-03");
  });

  test("priceValidity for a price with only validFrom date returns a string with validFrom only", (): void => {
    const price = defaultPriceWithOnlyValidFromDate();
    const actualValue = PriceGetters.priceValidity(price);
    expect(actualValue).toEqual("2022-12-01");
  });
});
