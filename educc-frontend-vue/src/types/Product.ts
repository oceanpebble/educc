// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Container } from "@/types/Container";
import type { Price } from "@/types/Price";
import * as PriceGetters from "@/types/Price";

export class Product {
  readonly container: Container;
  readonly id: number;
  readonly name: string;
  readonly prices: Price[];
  readonly store: string;
  readonly tags: string[];

  constructor(container: Container, id: number, name: string, prices: Price[], store: string, tags: string[]) {
    this.container = container;
    this.id = id;
    this.name = name;
    this.prices = prices;
    this.store = store;
    this.tags = tags;
  }
}

export function brandName(product: Product): string {
  return product.container.brand;
}

export function containerSizeInclUnit(product: Product): string {
  return product.container.volumeInMilliliter !== 0
    ? product.container.volumeInMilliliter + " ml"
    : product.container.weightInGram + " g";
}

export function hasAtLeastOnePrice(product: Product): boolean {
  return product.prices.length > 0;
}

export function hasAtLeastOneTag(product: Product): boolean {
  return product.tags.length > 0;
}

export function productName(product: Product): string {
  return product.name;
}

export function productPriceInclUnit(product: Product): string {
  const price = product.prices[product.prices.length - 1];
  return PriceGetters.priceInclUnit(price);
}

export function storeName(product: Product): string {
  return product.store;
}

export function tagList(product: Product): string {
  return product.tags.join(", ");
}
