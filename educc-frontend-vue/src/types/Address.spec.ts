// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test } from "vitest";
import * as AddressGetters from "@/types/Address";
import { defaultAddress } from "@/utils/TestObjectFactory";

describe("Address", (): void => {
  test("addressCity returns zip and city", (): void => {
    const address = defaultAddress();
    const actualValue = AddressGetters.addressCity(address);
    expect(actualValue).toEqual("address zip address city");
  });

  test("addressCountry returns state and country", (): void => {
    const address = defaultAddress();
    const actualValue = AddressGetters.addressCountry(address);
    expect(actualValue).toEqual("address state, address country");
  });

  test("addressStreet returns street and house number", (): void => {
    const address = defaultAddress();
    const actualValue = AddressGetters.addressStreet(address);
    expect(actualValue).toEqual("address street address housenumber");
  });
});
