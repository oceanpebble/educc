// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

export class Currency {
  readonly id: number;
  readonly iso4217AlphabeticCode: string;
  readonly iso4217NumericCode: string;
  readonly minorUnit: number;
  readonly name: string;
  readonly sign: string;

  constructor(
    id: number,
    iso4217AlphabeticCode: string,
    iso4217NumericCode: string,
    minorUnit: number,
    name: string,
    sign: string,
  ) {
    this.id = id;
    this.iso4217AlphabeticCode = iso4217AlphabeticCode;
    this.iso4217NumericCode = iso4217NumericCode;
    this.minorUnit = minorUnit;
    this.name = name;
    this.sign = sign;
  }
}
