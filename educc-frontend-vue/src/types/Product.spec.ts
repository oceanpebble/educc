// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test } from "vitest";
import * as ProductGetters from "@/types/Product";
import {
  defaultProduct,
  defaultProductWith0mlContainer,
  defaultProductWithNoPrices,
  defaultProductWithNoTags,
  defaultProductWithThreePrices,
} from "@/utils/TestObjectFactory";

describe("Product", (): void => {
  test("brandName returns the brand name", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.brandName(product);
    expect(actualValue).toBe("container brand");
  });

  test("containerSizeInclUnit returns container size in ml if available", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.containerSizeInclUnit(product);
    expect(actualValue).toBe("1000 ml");
  });

  test("containerSizeInclUnit returns container size in g if ml not available", (): void => {
    const product = defaultProductWith0mlContainer();
    const actualValue = ProductGetters.containerSizeInclUnit(product);
    expect(actualValue).toBe("1000 g");
  });

  test("hasAtLeastOnePrice returns false for a product without price", (): void => {
    const product = defaultProductWithNoPrices();
    const actualValue = ProductGetters.hasAtLeastOnePrice(product);
    expect(actualValue).toBe(false);
  });

  test("hasAtLeastOnePrice returns true for a product with a price", (): void => {
    const product = defaultProductWithThreePrices();
    const actualValue = ProductGetters.hasAtLeastOnePrice(product);
    expect(actualValue).toBe(true);
  });

  test("hasAtLeastOneTag returns false for a product without tag", (): void => {
    const product = defaultProductWithNoTags();
    const actualValue = ProductGetters.hasAtLeastOneTag(product);
    expect(actualValue).toBe(false);
  });

  test("hasAtLeastOneTag returns true for a product with a tag", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.hasAtLeastOneTag(product);
    expect(actualValue).toBe(true);
  });

  test("productName returns the product name", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.productName(product);
    expect(actualValue).toBe("product name");
  });

  test("productPriceInclUnit returns the latest price", (): void => {
    const product = defaultProductWithThreePrices();
    const actualValue = ProductGetters.productPriceInclUnit(product);
    expect(actualValue).toBe("0.89 currency name");
  });

  test("storeName returns the store name", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.storeName(product);
    expect(actualValue).toBe("store name");
  });

  test("tagList returns empty string for product without tag", (): void => {
    const product = defaultProductWithNoTags();
    const actualValue = ProductGetters.tagList(product);
    expect(actualValue).toBe("");
  });

  test("tagList returns string with comma separated tags for product with tags", (): void => {
    const product = defaultProduct();
    const actualValue = ProductGetters.tagList(product);
    expect(actualValue).toBe("foo tag, bar tag");
  });
});
