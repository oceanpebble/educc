// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { afterEach, beforeAll, describe, expect, test, vi } from "vitest";
import * as AddressGetters from "@/types/Address";
import * as StoreGetters from "@/types/Store";
import { defaultStore } from "@/utils/TestObjectFactory";

describe("forwards to address calls to", (): void => {
  beforeAll((): void => {
    vi.mock("./Address");
  });

  afterEach((): void => {
    vi.resetAllMocks();
  });

  test("storeAddressCity", (): void => {
    const mockedFunction = vi.mocked(AddressGetters.addressCity);
    const store = defaultStore();
    StoreGetters.storeAddressCity(store);
    expect(mockedFunction).toHaveBeenCalledOnce();
  });

  test("storeAddressCountry", (): void => {
    const mockedFunction = vi.mocked(AddressGetters.addressCountry);
    const store = defaultStore();
    StoreGetters.storeAddressCountry(store);
    expect(mockedFunction).toHaveBeenCalledOnce();
  });

  test("storeAddressStreet", (): void => {
    const mockedFunction = vi.mocked(AddressGetters.addressStreet);
    const store = defaultStore();
    StoreGetters.storeAddressStreet(store);
    expect(mockedFunction).toHaveBeenCalledOnce();
  });
});
