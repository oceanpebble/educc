// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Weekday } from "@/types/Weekday";
import i18n from "@/i18n";

export class OpeningHour {
  readonly beginHour: number;
  readonly beginMinute: number;
  readonly endHour: number;
  readonly endMinute: number;
  readonly id: number;
  readonly weekday: Weekday;

  constructor(
    beginHour: number,
    beginMinute: number,
    endHour: number,
    endMinute: number,
    id: number,
    weekday: Weekday,
  ) {
    this.beginHour = beginHour;
    this.beginMinute = beginMinute;
    this.endHour = endHour;
    this.endMinute = endMinute;
    this.id = id;
    this.weekday = weekday;
  }
}

export function toString(openingHour: OpeningHour): string {
  return (
    i18n.vueI18nGlobal.t(`weekdays.${openingHour.weekday.toLowerCase()}`) +
    ": " +
    beginTime(openingHour) +
    " - " +
    endTime(openingHour)
  );
}

function beginTime(openingHour: OpeningHour): string {
  const beginHour = prependZero(openingHour.beginHour);
  const beginMinute = prependZero(openingHour.beginMinute);
  return `${beginHour}:${beginMinute}`;
}

function endTime(openingHour: OpeningHour): string {
  const endHour = prependZero(openingHour.endHour);
  const endMinute = prependZero(openingHour.endMinute);
  return `${endHour}:${endMinute}`;
}

function prependZero(value: number): string {
  return value <= 9 ? `0${value}` : `${value}`;
}
