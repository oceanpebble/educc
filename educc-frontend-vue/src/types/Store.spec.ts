// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test } from "vitest";
import * as StoreGetters from "@/types/Store";
import { defaultStore } from "@/utils/TestObjectFactory";

describe("Store", (): void => {
  test("storeName returns the store name", (): void => {
    const store = defaultStore();
    const actualValue = StoreGetters.storeName(store);
    expect(actualValue).toBe("store name");
  });
});
