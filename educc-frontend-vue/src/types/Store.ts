// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { OpeningHour } from "./OpeningHour";
import type { Address } from "@/types/Address";
import type { Product } from "@/types/Product";
import * as AddressGetters from "@/types/Address";

export class Store {
  readonly address: Address;
  readonly id: number;
  readonly name: string;
  readonly openingHours: OpeningHour[];
  readonly products: Product[];

  constructor(address: Address, id: number, name: string, openingHours: OpeningHour[], products: Product[]) {
    this.address = address;
    this.id = id;
    this.name = name;
    this.openingHours = openingHours;
    this.products = products;
  }
}

export function storeAddressCity(store: Store): string {
  return AddressGetters.addressCity(store.address);
}

export function storeAddressCountry(store: Store): string {
  return AddressGetters.addressCountry(store.address);
}

export function storeAddressStreet(store: Store): string {
  return AddressGetters.addressStreet(store.address);
}

export function storeName(store: Store): string {
  return store.name;
}
