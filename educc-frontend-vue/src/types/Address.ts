// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

export class Address {
  readonly city: string;
  readonly country: string;
  readonly houseNumber: string;
  readonly state: string;
  readonly street: string;
  readonly zip: string;

  constructor(city: string, country: string, houseNumber: string, state: string, street: string, zip: string) {
    this.city = city;
    this.country = country;
    this.houseNumber = houseNumber;
    this.state = state;
    this.street = street;
    this.zip = zip;
  }
}

export function addressCity(address: Address): string {
  return address.zip + " " + address.city;
}

export function addressCountry(address: Address): string {
  return address.state + ", " + address.country;
}

export function addressStreet(address: Address): string {
  return address.street + " " + address.houseNumber;
}
