// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test } from "vitest";
import * as OpeningHourGetters from "@/types/OpeningHour";
import { defaultOpeningHour } from "@/utils/TestObjectFactory";

describe("OpeningHours", (): void => {
  test("toString creates a string representation of day, start time and and time", (): void => {
    const openingHour = defaultOpeningHour();
    const result = OpeningHourGetters.toString(openingHour);
    expect(result).toStrictEqual("Monday: 09:00 - 17:00");
  });
});
