// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
export class UserFeedback {
  readonly id: number;
  readonly feedbackText: string;
  readonly timeStamp: string;

  constructor(id: number, feedbackText: string, timeStamp: string) {
    this.id = id;
    this.feedbackText = feedbackText;
    this.timeStamp = timeStamp;
  }
}

export function timeStamp(userFeedback: UserFeedback): string {
  return new Date(userFeedback.timeStamp).toLocaleString().split("T")[0];
}
