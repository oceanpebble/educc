// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Currency } from "@/types/Currency";
import i18n from "@/i18n";

export class Price {
  readonly currency: Currency;
  readonly id: number;
  readonly priceInCurrencyMinor: number;
  readonly priceInCurrencyMinorPerKilogram: number;
  readonly priceInCurrencyMinorPerLiter: number;
  readonly priceInCurrencyMinorPerPiece: number;
  readonly validFrom: string;
  readonly validTo: string | null;

  constructor(
    currency: Currency,
    id: number,
    priceInCurrencyMinor: number,
    priceInCurrencyMinorPerKilogram: number,
    priceInCurrencyMinorPerLiter: number,
    priceInCurrencyMinorPerPiece: number,
    validFrom: string,
    validTo: string | null,
  ) {
    this.currency = currency;
    this.id = id;
    this.priceInCurrencyMinor = priceInCurrencyMinor;
    this.priceInCurrencyMinorPerKilogram = priceInCurrencyMinorPerKilogram;
    this.priceInCurrencyMinorPerLiter = priceInCurrencyMinorPerLiter;
    this.priceInCurrencyMinorPerPiece = priceInCurrencyMinorPerPiece;
    this.validFrom = validFrom;
    this.validTo = validTo;
  }
}

export function priceInclUnit(price: Price): string {
  return (
    i18n.vueI18nGlobal.n(price.priceInCurrencyMinor / Math.pow(10, price.currency.minorUnit)) +
    " " +
    price.currency.name
  );
}

export function priceValidity(price: Price): string {
  return (
    new Date(price.validFrom).toISOString().split("T")[0] +
    (price.validTo !== null
      ? " " + i18n.vueI18nGlobal.t("to") + " " + new Date(price.validTo).toISOString().split("T")[0]
      : "")
  );
}
