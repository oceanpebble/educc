// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { afterEach, beforeAll, describe, expect, test, vi } from "vitest";
import * as PriceGetters from "@/types/Price";
import * as ProductGetters from "@/types/Product";
import { defaultProduct } from "@/utils/TestObjectFactory";

describe("forwards to price calls to", (): void => {
  beforeAll((): void => {
    vi.mock("./Price");
  });

  afterEach((): void => {
    vi.resetAllMocks();
  });

  test("priceInclUnit", (): void => {
    const mockedFunction = vi.mocked(PriceGetters.priceInclUnit);
    const product = defaultProduct();
    ProductGetters.productPriceInclUnit(product);
    expect(mockedFunction).toHaveBeenCalledOnce();
  });
});
