// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { createApp } from "vue";
import i18n from "@/i18n";
import router from "@/router";
import App from "./App.vue";

i18n.setup();
createApp(App).use(i18n.vueI18n).use(router).mount("#app");
