// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { DOMWrapper, VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, beforeEach, describe, expect, test, vi } from "vitest";
import i18n from "@/i18n";
import { saveProduct } from "@/service/ProductService";
import { fetchStoreList } from "@/service/StoreService";
import { fetchTagList } from "@/service/TagService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertInputPlaceholder, assertInputValue, setInputValue } from "@/utils/HTMLInputElementUtils";
import { assertOptionText } from "@/utils/HTMLOptionElementUtils";
import { assertSelectValue, setSelectValue } from "@/utils/HTMLSelectElementUtils";
import { defaultStore } from "@/utils/TestObjectFactory";
import NewProductView from "@/views/NewProductView.vue";

describe("NewProductView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/ProductService");
    vi.mock("@/service/StoreService");
    vi.mock("@/service/TagService");
  });

  beforeEach((): void => {
    vi.mocked(fetchStoreList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([defaultStore(0, "store 1"), defaultStore(1, "store 2"), defaultStore(2, "store 3")]);
      }),
    );
    vi.mocked(fetchTagList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([
          defaultStore(0, "tag 1"),
          defaultStore(1, "tag 2"),
          defaultStore(2, "tag 3"),
          defaultStore(3, "tag 4"),
        ]);
      }),
    );
  });

  test("has all the necessary input fields for a new product", async (): Promise<void> => {
    const newProductViewWrapper = await shallowMountNewProductView();

    const inputFieldWrappers = newProductViewWrapper.findAll("input");
    expect(inputFieldWrappers).toHaveLength(4);
    assertInputPlaceholder(inputFieldWrappers[0], "name");
    assertInputPlaceholder(inputFieldWrappers[1], "volume in ml");
    assertInputPlaceholder(inputFieldWrappers[2], "weight in g");
    assertInputPlaceholder(inputFieldWrappers[3], "brand name");
  });

  test("tag input is a multiselect combobox prefilled with existing tag names", async (): Promise<void> => {
    const newProductViewWrapper = await shallowMountNewProductView();
    await flushPromises();

    const selectFieldWrappers = newProductViewWrapper.findAll("select");
    const options = selectFieldWrappers[0].findAll("option");

    expect(options).toHaveLength(4);
    assertOptionText(options, 0, "tag 1");
    assertOptionText(options, 1, "tag 2");
    assertOptionText(options, 2, "tag 3");
    assertOptionText(options, 3, "tag 4");
  });

  test("store input is a combobox prefilled with existing store names", async (): Promise<void> => {
    const newProductViewWrapper = await shallowMountNewProductView();

    const selectFieldWrappers = newProductViewWrapper.findAll("select");
    const options = selectFieldWrappers[1].findAll("option");

    expect(options).toHaveLength(3);
    assertOptionText(options, 0, "store 1");
    assertOptionText(options, 1, "store 2");
    assertOptionText(options, 2, "store 3");
  });

  test("has a submit button", async (): Promise<void> => {
    const newProductViewWrapper = await shallowMountNewProductView();
    const submitButtonWrapper = newProductViewWrapper.find("button");

    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls ProductService function with correct arguments", async (): Promise<void> => {
    const saveProductMock = vi.mocked(saveProduct);
    const newProductViewWrapper = await shallowMountNewProductView();

    const inputFieldWrappers = newProductViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const tagFieldWrapper = newProductViewWrapper.findAll("select")[0];
    await tagFieldWrapper.setValue(["2", "3"]);
    const storeFieldWrapper = newProductViewWrapper.findAll("select")[1];
    await setSelectValue(storeFieldWrapper, "1");
    const formWrapper = newProductViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(saveProductMock).toHaveBeenCalledOnce();
    expect(saveProductMock).toHaveBeenCalledWith("brand name", "product name", "1", ["2", "3"], "1000", "2000");
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const newProductViewWrapper = await shallowMountNewProductView();

    const inputFieldWrappers = newProductViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const tagFieldWrapper = newProductViewWrapper.findAll("select")[0];
    await tagFieldWrapper.setValue([2, 3]);
    const storeFieldWrapper = newProductViewWrapper.findAll("select")[1];
    await setSelectValue(storeFieldWrapper, "1");
    const formWrapper = newProductViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertInputValue(inputFieldWrappers[0], "");
    assertInputValue(inputFieldWrappers[1], "");
    assertInputValue(inputFieldWrappers[2], "");
    assertInputValue(inputFieldWrappers[3], "");
    expect(tagFieldWrapper.element.value).toBe("");
    assertSelectValue(storeFieldWrapper, "");
  });
});

async function shallowMountNewProductView(): Promise<VueWrapper<InstanceType<typeof NewProductView>>> {
  const wrapper = shallowMount<typeof NewProductView>(NewProductView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
  await flushPromises();
  return wrapper;
}

async function setDefaultInputValues(inputFieldWrappers: DOMWrapper<HTMLInputElement>[]): Promise<void> {
  await setInputValue(inputFieldWrappers[0], "product name");
  await setInputValue(inputFieldWrappers[1], "1000");
  await setInputValue(inputFieldWrappers[2], "2000");
  await setInputValue(inputFieldWrappers[3], "brand name");
}
