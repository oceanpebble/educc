// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Product } from "@/types/Product";
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { afterEach, beforeAll, describe, expect, test, vi } from "vitest";
import ProductSummary from "@/atoms/ProductSummary.vue";
import i18n from "@/i18n";
import { fetchProductList } from "@/service/ProductService";
import { defaultProduct } from "@/utils/TestObjectFactory";
import ProductsView from "@/views/ProductsView.vue";

describe("ProductsView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/ProductService");
  });

  afterEach((): void => {
    vi.restoreAllMocks();
  });

  test("shows no product summary when no products are fetched", async (): Promise<void> => {
    mockReturnForFetchProductList([]);
    const productsViewWrapper = await shallowMountProductsView();
    const productSummaryWrappers = productsViewWrapper.findAllComponents(ProductSummary);

    expect(productsViewWrapper.text()).toContain("List of all 0 products");
    expect(productSummaryWrappers).toHaveLength(0);
  });

  test("shows one product summary when one product is fetched", async (): Promise<void> => {
    mockReturnForFetchProductList([defaultProduct()]);
    const productsViewWrapper = await shallowMountProductsView();
    const productSummaryWrappers = productsViewWrapper.findAllComponents(ProductSummary);

    expect(productsViewWrapper.text()).toContain("List of all 1 products");
    expect(productSummaryWrappers).toHaveLength(1);
    expect(productSummaryWrappers[0].props().product.name).toBe("product name");
  });

  test("shows two product summaries when two products are fetched", async (): Promise<void> => {
    mockReturnForFetchProductList([defaultProduct(), defaultProduct(2, "product 2")]);
    const productsViewWrapper = await shallowMountProductsView();
    const productSummaryWrappers = productsViewWrapper.findAllComponents(ProductSummary);

    expect(productsViewWrapper.text()).toContain("List of all 2 products");
    expect(productSummaryWrappers).toHaveLength(2);
    expect(productSummaryWrappers[0].props().product.name).toBe("product name");
    expect(productSummaryWrappers[1].props().product.name).toBe("product 2");
  });

  async function shallowMountProductsView(): Promise<VueWrapper<InstanceType<typeof ProductsView>>> {
    const productsViewWrapper = shallowMount<typeof ProductsView>(ProductsView, {
      global: {
        plugins: [i18n.vueI18n],
      },
    });
    await flushPromises();
    return productsViewWrapper;
  }

  function mockReturnForFetchProductList(products: Product[]): void {
    vi.mocked(fetchProductList).mockReturnValue(
      new Promise((resolve): void => {
        resolve(products);
      }),
    );
  }
});
