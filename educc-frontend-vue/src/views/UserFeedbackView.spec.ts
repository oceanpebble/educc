// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { type VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, describe, expect, test, vi } from "vitest";
import UserFeedbackSummary from "@/atoms/UserFeedbackSummary.vue";
import i18n from "@/i18n";
import { getLatestUserFeedback, saveUserFeedback } from "@/service/UserFeedbackService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertTextAreaPlaceholder, assertTextAreaValue, setTextAreaValue } from "@/utils/HTMLTextAreaElementUtils";
import { defaultUserFeedback } from "@/utils/TestObjectFactory";
import UserFeedbackView from "@/views/UserFeedbackView.vue";

describe("UserFeedbackView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/UserFeedbackService");
  });

  test("has all the necessary input fields for new feedback when the user is logged in", async (): Promise<void> => {
    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const textAreaWrapper = userFeedbackViewWrapper.find("textarea");
    assertTextAreaPlaceholder(textAreaWrapper, "Your message");
  });

  test("has a submit button", async (): Promise<void> => {
    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();
    const submitButtonWrapper = userFeedbackViewWrapper.find("button");

    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls UserFeedbackService function with correct arguments", async (): Promise<void> => {
    const saveUserFeedbackMock = vi.mocked(saveUserFeedback);
    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const textAreaWrapper = userFeedbackViewWrapper.find("textarea");
    await setTextAreaValue(textAreaWrapper, "great stuff");
    const formWrapper = userFeedbackViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(saveUserFeedbackMock).toHaveBeenCalledOnce();
    expect(saveUserFeedbackMock).toHaveBeenCalledWith("great stuff");
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const textAreaWrapper = userFeedbackViewWrapper.find("textarea");
    await setTextAreaValue(textAreaWrapper, "great stuff");
    const formWrapper = userFeedbackViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertTextAreaValue(textAreaWrapper, "");
  });

  test("show message when input data invalid", async (): Promise<void> => {
    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const formWrapper = userFeedbackViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);
    const successMessage = userFeedbackViewWrapper.find(".data_invalid_message");

    expect(successMessage.isVisible()).toBe(true);
  });

  test("does not contain a UserFeedbackSummary when none exist", async (): Promise<void> => {
    vi.mocked(getLatestUserFeedback).mockResolvedValue([]);

    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const userFeedbackSummaries = userFeedbackViewWrapper.findAllComponents(UserFeedbackSummary);
    expect(userFeedbackSummaries.length).equals(0);
  });

  test("does contain a UserFeedbackSummary for every UserFeedback", async (): Promise<void> => {
    vi.mocked(getLatestUserFeedback).mockResolvedValue([
      defaultUserFeedback(),
      defaultUserFeedback("2024-10-04T11:07:13"),
    ]);

    const userFeedbackViewWrapper = await shallowMountUserFeedbackView();

    const userFeedbackSummaries = userFeedbackViewWrapper.findAllComponents(UserFeedbackSummary);
    expect(userFeedbackSummaries.length).equals(2);
    expect(userFeedbackSummaries[0].props().userFeedback).toEqual(defaultUserFeedback());
    expect(userFeedbackSummaries[1].props().userFeedback).toEqual(defaultUserFeedback("2024-10-04T11:07:13"));
  });
});

async function shallowMountUserFeedbackView(): Promise<VueWrapper<InstanceType<typeof UserFeedbackView>>> {
  const wrapper = shallowMount<typeof UserFeedbackView>(UserFeedbackView, {
    global: {
      plugins: [i18n.vueI18n],
      stubs: { UserFeedbackSummary: true },
    },
  });
  await flushPromises();
  return wrapper;
}
