// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, describe, expect, test, vi } from "vitest";
import i18n from "@/i18n";
import { registerUser } from "@/service/AuthenticationService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertInputPlaceholder, assertInputValue, setInputValue } from "@/utils/HTMLInputElementUtils";
import RegisterUserView from "@/views/RegisterUserView.vue";

describe("RegisterUserView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/AuthenticationService");
  });

  test("has all the necessary input fields for a new price", async (): Promise<void> => {
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const inputFieldWrappers = registerUserViewWrapper.findAll("input");
    expect(inputFieldWrappers).toHaveLength(2);
    assertInputPlaceholder(inputFieldWrappers[0], "name");
    assertInputPlaceholder(inputFieldWrappers[1], "password");
  });

  test("has a submit button", async (): Promise<void> => {
    const registerUserViewWrapper = await shallowMountRegisterUserView();
    const submitButtonWrapper = registerUserViewWrapper.find("button");

    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls AuthenticationService function with correct arguments", async (): Promise<void> => {
    const registerUserMock = vi.mocked(registerUser);
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const inputFieldWrappers = registerUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = registerUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(registerUserMock).toHaveBeenCalledOnce();
    expect(registerUserMock).toHaveBeenCalledWith("a user name", "super duper password");
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const inputFieldWrappers = registerUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = registerUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertInputValue(inputFieldWrappers[0], "");
    assertInputValue(inputFieldWrappers[1], "");
  });

  test("show message when input data invalid", async (): Promise<void> => {
    mockRegisterUserResult(true);
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const formWrapper = registerUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);
    const successMessage = registerUserViewWrapper.find(".data_invalid_message");

    expect(successMessage.isVisible()).toBe(true);
  });

  test("show message when registrations attempt failed", async (): Promise<void> => {
    mockRegisterUserResult(false);
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const inputFieldWrappers = registerUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = registerUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);
    const failureMessage = registerUserViewWrapper.find(".failure_message");

    expect(failureMessage.isVisible()).toBe(true);
  });

  test("show message when registration attempt succeeded", async (): Promise<void> => {
    mockRegisterUserResult(true);
    const registerUserViewWrapper = await shallowMountRegisterUserView();

    const inputFieldWrappers = registerUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = registerUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);
    const successMessage = registerUserViewWrapper.find(".success_message");

    expect(successMessage.isVisible()).toBe(true);
  });
});

async function shallowMountRegisterUserView(): Promise<VueWrapper<InstanceType<typeof RegisterUserView>>> {
  const wrapper = shallowMount<typeof RegisterUserView>(RegisterUserView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
  await flushPromises();
  return wrapper;
}

function mockRegisterUserResult(result: boolean): void {
  vi.mocked(registerUser).mockResolvedValue(result);
}
