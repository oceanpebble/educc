// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import { DOMWrapper, VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, beforeEach, describe, expect, test, vi } from "vitest";
import i18n from "@/i18n";
import { fetchCurrencyList } from "@/service/CurrencyService";
import { savePrice } from "@/service/PriceService";
import { fetchProductList } from "@/service/ProductService";
import { fetchStoreList } from "@/service/StoreService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertInputPlaceholder, assertInputValue, setInputValue } from "@/utils/HTMLInputElementUtils";
import { assertOptionText } from "@/utils/HTMLOptionElementUtils";
import {
  assertSelectDisabled,
  assertSelectEnabled,
  assertSelectValue,
  setSelectValue,
} from "@/utils/HTMLSelectElementUtils";
import { defaultCurrency, defaultProduct, defaultStore, defaultStoreWithTwoProducts } from "@/utils/TestObjectFactory";
import NewPriceView from "@/views/NewPriceView.vue";

describe("NewPriceView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/CurrencyService");
    vi.mock("@/service/PriceService");
    vi.mock("@/service/ProductService");
    vi.mock("@/service/StoreService");
  });

  beforeEach((): void => {
    vi.mocked(fetchCurrencyList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([defaultCurrency("currency 1"), defaultCurrency("currency 2"), defaultCurrency("currency 3")]);
      }),
    );
    vi.mocked(fetchProductList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([
          defaultProduct(0, "product 1", "store 1"),
          defaultProduct(1, "product 2", "store 2"),
          defaultProduct(2, "product 3", "store name"),
        ]);
      }),
    );
    vi.mocked(fetchStoreList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([
          defaultStore(0, "store 1"),
          defaultStore(1, "store 2"),
          defaultStore(2, "store 3"),
          defaultStoreWithTwoProducts(3),
        ]);
      }),
    );
  });

  test("store input is a combobox prefilled with existing store names", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const selectFieldWrappers = newPriceViewWrapper.findAll("select");
    const options = selectFieldWrappers[0].findAll("option");

    expect(options).toHaveLength(4);
    assertOptionText(options, 0, "store 1");
    assertOptionText(options, 1, "store 2");
    assertOptionText(options, 2, "store 3");
    assertOptionText(options, 3, "store name");
  });

  test("product input is an empty, disabled combobox, if no store was selected", async (): Promise<void> => {
    vi.mocked(fetchProductList).mockReturnValue(
      new Promise((resolve): void => {
        resolve([]);
      }),
    );

    const newPriceViewWrapper = await shallowMountNewPriceView();

    const selectFieldWrappers = newPriceViewWrapper.findAll("select");
    const options = selectFieldWrappers[1].findAll("option");

    assertSelectDisabled(selectFieldWrappers[1]);
    expect(options).toHaveLength(0);
  });

  test("product input is a combobox prefilled with a store's products, if a store was selected", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const selectFieldWrappers = newPriceViewWrapper.findAll("select");
    await setSelectValue(selectFieldWrappers[0], "3");
    const options = selectFieldWrappers[1].findAll("option");

    assertSelectEnabled(selectFieldWrappers[1]);
    expect(options).toHaveLength(1);
    assertOptionText(options, 0, "product 3 (container brand)");
  });

  test("has all the necessary input fields for a new price", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const inputFieldWrappers = newPriceViewWrapper.findAll("input");
    expect(inputFieldWrappers).toHaveLength(2);
    assertInputPlaceholder(inputFieldWrappers[0], "price");
    assertInputPlaceholder(inputFieldWrappers[1], "purchase date");
  });

  test("currency input is a combobox prefilled with existing currency names", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const selectFieldWrappers = newPriceViewWrapper.findAll("select");
    const options = selectFieldWrappers[2].findAll("option");

    expect(options).toHaveLength(3);
    assertOptionText(options, 0, "currency 1");
    assertOptionText(options, 1, "currency 2");
    assertOptionText(options, 2, "currency 3");
  });

  test("has a submit button", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();
    const submitButtonWrapper = newPriceViewWrapper.find("button");

    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls PriceService function with correct arguments", async (): Promise<void> => {
    const savePriceMock = vi.mocked(savePrice);
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const inputFieldWrappers = newPriceViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const storeFieldWrapper = newPriceViewWrapper.findAll("select")[0];
    await setSelectValue(storeFieldWrapper, "1");
    const productFieldWrapper = newPriceViewWrapper.findAll("select")[1];
    await setSelectValue(productFieldWrapper, "1");
    const currencyFieldWrapper = newPriceViewWrapper.findAll("select")[2];
    await setSelectValue(currencyFieldWrapper, "1");
    const formWrapper = newPriceViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(savePriceMock).toHaveBeenCalledOnce();
    expect(savePriceMock).toHaveBeenCalledWith("1", "13.37", "1", "2023-02-06", "1");
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const newPriceViewWrapper = await shallowMountNewPriceView();

    const inputFieldWrappers = newPriceViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const storeFieldWrapper = newPriceViewWrapper.findAll("select")[0];
    await setSelectValue(storeFieldWrapper, "1");
    const productFieldWrapper = newPriceViewWrapper.findAll("select")[1];
    await setSelectValue(productFieldWrapper, "1");
    const currencyFieldWrapper = newPriceViewWrapper.findAll("select")[2];
    await setSelectValue(currencyFieldWrapper, "1");
    const formWrapper = newPriceViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertInputValue(inputFieldWrappers[0], "");
    assertInputValue(inputFieldWrappers[1], "");
    assertSelectValue(currencyFieldWrapper, "");
    assertSelectValue(productFieldWrapper, "");
    assertSelectValue(storeFieldWrapper, "");
  });
});

async function shallowMountNewPriceView(): Promise<VueWrapper<InstanceType<typeof NewPriceView>>> {
  const wrapper = shallowMount<typeof NewPriceView>(NewPriceView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
  await flushPromises();
  return wrapper;
}

async function setDefaultInputValues(inputFieldWrappers: DOMWrapper<HTMLInputElement>[]): Promise<void> {
  await setInputValue(inputFieldWrappers[0], "13.37");
  await setInputValue(inputFieldWrappers[1], "2023-02-06");
}
