// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Product } from "@/types/Product";
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { afterEach, beforeAll, beforeEach, describe, expect, test, vi } from "vitest";
import { createRouterMock, getRouter, injectRouterMock } from "vue-router-mock";
import i18n from "@/i18n";
import { pathForSingleProductById } from "@/router/make_router_paths";
import { getProductById } from "@/service/ProductService";
import { defaultProduct, defaultProductWithNoPrices, defaultProductWithThreePrices } from "@/utils/TestObjectFactory";
import ProductView from "@/views/ProductView.vue";

describe("ProductView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/ProductService");
  });

  beforeEach((): void => {
    injectRouterMock(
      createRouterMock({
        initialLocation: pathForSingleProductById(1),
        runInComponentGuards: true,
      }),
    );
  });

  afterEach((): void => {
    vi.restoreAllMocks();
  });

  test("shows details of product for given id", async (): Promise<void> => {
    mockReturnForGetProductById(defaultProductWithNoPrices());
    const productViewWrapper = await shallowMountProductView();

    expect(productViewWrapper.html()).toContain("product name");
    expect(productViewWrapper.html()).toContain("Product");
    expect(productViewWrapper.html()).toContain("Store: store name");
    expect(productViewWrapper.html()).not.toContain("Prices");
  });

  test("shows one price with both dates if the product has one with a price with both dates", async (): Promise<void> => {
    mockReturnForGetProductById(defaultProduct());
    const productViewWrapper = await shallowMountProductView();

    expect(productViewWrapper.html()).toContain("product name");
    expect(productViewWrapper.html()).toContain("Product");
    expect(productViewWrapper.html()).toContain("Price");
    expect(productViewWrapper.html()).toContain("0.99 currency name 2022-12-01 to 2022-12-03");
  });

  test("shows three prices, last one with only on date, if the procuts has has the appropriate prices", async (): Promise<void> => {
    mockReturnForGetProductById(defaultProductWithThreePrices());
    const productViewWrapper = await shallowMountProductView();

    expect(productViewWrapper.html()).toContain("product name");
    expect(productViewWrapper.html()).toContain("Product");
    expect(productViewWrapper.html()).toContain("Prices");
    expect(productViewWrapper.html()).toContain("0.49 currency name 2018-06-22 to 2021-12-08");
    expect(productViewWrapper.html()).toContain("0.79 currency name 2021-12-09 to 2022-04-23");
    expect(productViewWrapper.html()).toContain("0.89 currency name 2022-04-24");
  });

  test("switches to other product when url changes", async (): Promise<void> => {
    mockReturnForGetProductById(defaultProduct());
    const productViewWrapper = await shallowMountProductView();

    await getRouter().push(pathForSingleProductById(2));
    expect(productViewWrapper.html()).toContain("product 2");
  });

  async function shallowMountProductView(): Promise<VueWrapper<InstanceType<typeof ProductView>>> {
    const productViewWrapper = shallowMount<typeof ProductView>(ProductView, {
      props: {
        id: 1,
      },
      global: {
        plugins: [i18n.vueI18n],
      },
    });
    await flushPromises();
    return productViewWrapper;
  }

  function mockReturnForGetProductById(product: Product): void {
    vi.mocked(getProductById).mockImplementation(async (id: number): Promise<Product> => {
      return new Promise((resolve): void => {
        resolve(id === 1 ? product : defaultProduct(2, "product 2"));
      });
    });
  }
});
