// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Store } from "@/types/Store";
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { afterEach, beforeAll, describe, expect, test, vi } from "vitest";
import StoreSummary from "@/atoms/StoreSummary.vue";
import i18n from "@/i18n";
import { fetchStoreList } from "@/service/StoreService";
import { defaultStore } from "@/utils/TestObjectFactory";
import StoresView from "@/views/StoresView.vue";

describe("StoresView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/StoreService");
  });

  afterEach((): void => {
    vi.restoreAllMocks();
  });

  test("shows no store summary when no stores are fetched", async (): Promise<void> => {
    mockReturnForFetchStoreList([]);
    const storesViewWrapper = await shallowMountStoresView();
    const storeSummaryWrappers = storesViewWrapper.findAllComponents(StoreSummary);

    expect(storesViewWrapper.text()).toContain("List of all 0 stores");
    expect(storeSummaryWrappers).toHaveLength(0);
  });

  test("shows one store summary when one store is fetched", async (): Promise<void> => {
    mockReturnForFetchStoreList([defaultStore()]);
    const storesViewWrapper = await shallowMountStoresView();
    const storeSummaryWrappers = storesViewWrapper.findAllComponents(StoreSummary);

    expect(storesViewWrapper.text()).toContain("List of all 1 stores");
    expect(storeSummaryWrappers).toHaveLength(1);
    expect(storeSummaryWrappers[0].props().store.name).toBe("store name");
  });

  test("shows two store summaries when two stores are fetched", async (): Promise<void> => {
    mockReturnForFetchStoreList([defaultStore(), defaultStore(0, "store 2")]);
    const storesViewWrapper = await shallowMountStoresView();
    const storeSummaryWrappers = storesViewWrapper.findAllComponents(StoreSummary);

    expect(storesViewWrapper.text()).toContain("List of all 2 stores");
    expect(storeSummaryWrappers).toHaveLength(2);
    expect(storeSummaryWrappers[0].props().store.name).toBe("store name");
    expect(storeSummaryWrappers[1].props().store.name).toBe("store 2");
  });

  async function shallowMountStoresView(): Promise<VueWrapper<InstanceType<typeof StoresView>>> {
    const storesViewWrapper = shallowMount<typeof StoresView>(StoresView, {
      global: { plugins: [i18n.vueI18n] },
    });
    await flushPromises();
    return storesViewWrapper;
  }

  function mockReturnForFetchStoreList(stores: Store[]): void {
    vi.mocked(fetchStoreList).mockReturnValue(
      new Promise((resolve): void => {
        resolve(stores);
      }),
    );
  }
});
