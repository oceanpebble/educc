// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { DOMWrapper, VueWrapper, shallowMount } from "@vue/test-utils";
import { describe, expect, test, vi } from "vitest";
import i18n from "@/i18n";
import { saveStore } from "@/service/StoreService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertInputPlaceholder, assertInputValue, setInputValue } from "@/utils/HTMLInputElementUtils";
import NewStoreView from "@/views/NewStoreView.vue";

describe("NewStoreView", async (): Promise<void> => {
  test("has all the necessary input fields for a new store", async (): Promise<void> => {
    const newStoreViewWrapper = await shallowMountNewStoreView();

    const inputFieldWrappers = newStoreViewWrapper.findAll("input");
    expect(inputFieldWrappers).toHaveLength(7);
    assertInputPlaceholder(inputFieldWrappers[0], "name");
    assertInputPlaceholder(inputFieldWrappers[1], "street");
    assertInputPlaceholder(inputFieldWrappers[2], "house number");
    assertInputPlaceholder(inputFieldWrappers[3], "zip");
    assertInputPlaceholder(inputFieldWrappers[4], "city");
    assertInputPlaceholder(inputFieldWrappers[5], "state");
    assertInputPlaceholder(inputFieldWrappers[6], "country");
  });

  test("has a submit button", async (): Promise<void> => {
    const newStoreViewWrapper = await shallowMountNewStoreView();
    const submitButtonWrapper = newStoreViewWrapper.find("button");
    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls StoreService function with correct arguments", async (): Promise<void> => {
    vi.mock("@/service/StoreService");
    const saveStoreMock = vi.mocked(saveStore);
    const newStoreViewWrapper = await shallowMountNewStoreView();

    const inputFieldWrappers = newStoreViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const formWrapper = newStoreViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(saveStoreMock).toHaveBeenCalledOnce();
    expect(saveStoreMock).toHaveBeenCalledWith(
      "store city",
      "store country",
      "store house number",
      "store name",
      "store state",
      "store street",
      "store zip",
    );
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const newStoreViewWrapper = await shallowMountNewStoreView();

    const inputFieldWrappers = newStoreViewWrapper.findAll("input");
    await setDefaultInputValues(inputFieldWrappers);
    const formWrapper = newStoreViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertInputValue(inputFieldWrappers[0], "");
    assertInputValue(inputFieldWrappers[1], "");
    assertInputValue(inputFieldWrappers[2], "");
    assertInputValue(inputFieldWrappers[3], "");
    assertInputValue(inputFieldWrappers[4], "");
    assertInputValue(inputFieldWrappers[5], "");
    assertInputValue(inputFieldWrappers[6], "");
  });
});

async function shallowMountNewStoreView(): Promise<VueWrapper<InstanceType<typeof NewStoreView>>> {
  return shallowMount<typeof NewStoreView>(NewStoreView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
}

async function setDefaultInputValues(inputFieldWrappers: DOMWrapper<HTMLInputElement>[]): Promise<void> {
  await setInputValue(inputFieldWrappers[0], "store name");
  await setInputValue(inputFieldWrappers[1], "store street");
  await setInputValue(inputFieldWrappers[2], "store house number");
  await setInputValue(inputFieldWrappers[3], "store zip");
  await setInputValue(inputFieldWrappers[4], "store city");
  await setInputValue(inputFieldWrappers[5], "store state");
  await setInputValue(inputFieldWrappers[6], "store country");
}
