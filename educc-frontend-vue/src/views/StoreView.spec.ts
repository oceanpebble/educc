// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Store } from "@/types/Store";
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { afterEach, beforeAll, beforeEach, describe, expect, test, vi } from "vitest";
import { createRouterMock, getRouter, injectRouterMock } from "vue-router-mock";
import OpeningHours from "@/atoms/OpeningHours.vue";
import ProductSummary from "@/atoms/ProductSummary.vue";
import i18n from "@/i18n";
import { pathForSingleStoreById } from "@/router/make_router_paths";
import { getStoreById } from "@/service/StoreService";
import {
  defaultStore,
  defaultStoreWithOneProduct,
  defaultStoreWithOpeningHours,
  defaultStoreWithTwoProducts,
} from "@/utils/TestObjectFactory";
import StoreView from "@/views/StoreView.vue";

describe("StoreView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/StoreService");
  });

  beforeEach((): void => {
    injectRouterMock(
      createRouterMock({
        initialLocation: pathForSingleStoreById(1),
        runInComponentGuards: true,
      }),
    );
  });

  afterEach((): void => {
    vi.restoreAllMocks();
  });

  test("shows details of store for given id", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStore());
    const storeViewWrapper = await shallowMountStoreView();

    expect(storeViewWrapper.html()).toContain("store name");
    expect(storeViewWrapper.html()).toContain("Address");
    expect(storeViewWrapper.html()).toContain("address country");
    expect(storeViewWrapper.html()).not.toContain("Products");
    expect(storeViewWrapper.findAllComponents(ProductSummary)).toHaveLength(0);
  });

  test("does not show products if the store has no products", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStore());
    const storeViewWrapper = await shallowMountStoreView();
    const productSummaryWrappers = storeViewWrapper.findAllComponents(ProductSummary);
    expect(productSummaryWrappers).toHaveLength(0);
  });

  test("shows one product summary if the store has one product", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStoreWithOneProduct());
    const storeViewWrapper = await shallowMountStoreView();
    const productSummaryWrappers = storeViewWrapper.findAllComponents(ProductSummary);

    expect(storeViewWrapper.html()).toContain("store name");
    expect(storeViewWrapper.html()).toContain("Product");
    expect(productSummaryWrappers).toHaveLength(1);
    expect(productSummaryWrappers[0].props().product.name).toContain("product name");
  });

  test("shows two product summaries if the store has two products", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStoreWithTwoProducts());
    const storeViewWrapper = await shallowMountStoreView();
    const productSummaryWrappers = storeViewWrapper.findAllComponents(ProductSummary);

    expect(storeViewWrapper.html()).toContain("store name");
    expect(storeViewWrapper.html()).toContain("Products");
    expect(productSummaryWrappers).toHaveLength(2);
    expect(productSummaryWrappers[0].props().product.name).toContain("product name");
    expect(productSummaryWrappers[1].props().product.name).toContain("product 2");
  });

  test("does not show opening hours if the store has no opening hours", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStore());
    const storeViewWrapper = await shallowMountStoreView();
    const openingHourWrappers = storeViewWrapper.findAllComponents(OpeningHours);
    expect(openingHourWrappers).toHaveLength(0);
  });

  test("shows opening hours if the store has opening hours", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStoreWithOpeningHours());
    const storeViewWrapper = await shallowMountStoreView();
    const openingHourWrappers = storeViewWrapper.findAllComponents(OpeningHours);
    expect(openingHourWrappers).toHaveLength(1);
  });

  test("switches to other store when url changes", async (): Promise<void> => {
    mockReturnForGetStoreById(defaultStore());
    const storeViewWrapper = await shallowMountStoreView();

    await getRouter().push(pathForSingleStoreById(2));
    expect(storeViewWrapper.html()).toContain("store 2");
  });

  async function shallowMountStoreView(): Promise<VueWrapper<InstanceType<typeof StoreView>>> {
    const storeViewWrapper = shallowMount<typeof StoreView>(StoreView, {
      props: {
        id: 1,
      },
      global: {
        plugins: [i18n.vueI18n],
      },
    });
    await flushPromises();
    return storeViewWrapper;
  }

  function mockReturnForGetStoreById(store: Store): void {
    vi.mocked(getStoreById).mockImplementation(async (id: number): Promise<Store> => {
      return new Promise((resolve): void => {
        resolve(id === 1 ? store : defaultStore(0, "store 2"));
      });
    });
  }
});
