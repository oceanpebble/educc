// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, beforeEach, describe, expect, test, vi } from "vitest";
import { createRouterMock, getRouter, injectRouterMock } from "vue-router-mock";
import i18n from "@/i18n";
import { RoutePaths } from "@/router/route_constants";
import { loginUser } from "@/service/AuthenticationService";
import { assertButtonText } from "@/utils/HTMLButtonElementUtils";
import { triggerSubmitPrevent } from "@/utils/HTMLFormElementUtils";
import { assertInputPlaceholder, assertInputValue, setInputValue } from "@/utils/HTMLInputElementUtils";
import LoginUserView from "@/views/LoginUserView.vue";

describe("LoginUserView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/AuthenticationService");
  });

  beforeEach((): void => {
    mockRouter(RoutePaths.LOGIN_USER);
  });

  test("has all the necessary input fields for a user login", async (): Promise<void> => {
    const loginUserViewWrapper = await shallowMountLoginUserView();

    const inputFieldWrappers = loginUserViewWrapper.findAll("input");
    expect(inputFieldWrappers).toHaveLength(2);
    assertInputPlaceholder(inputFieldWrappers[0], "name");
    assertInputPlaceholder(inputFieldWrappers[1], "password");
  });

  test("has a submit button", async (): Promise<void> => {
    const loginUserViewWrapper = await shallowMountLoginUserView();
    const submitButtonWrapper = loginUserViewWrapper.find("button");

    expect(submitButtonWrapper.exists()).toBe(true);
    assertButtonText(submitButtonWrapper, "Submit");
  });

  test("click on submit button calls AuthenticationService function with correct arguments", async (): Promise<void> => {
    const loginUserMock = vi.mocked(loginUser);
    const loginUserViewWrapper = await shallowMountLoginUserView();

    const inputFieldWrappers = loginUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = loginUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(loginUserMock).toHaveBeenCalledOnce();
    expect(loginUserMock).toHaveBeenCalledWith("a user name", "super duper password");
  });

  test("click on submit button clears input fields", async (): Promise<void> => {
    const loginUserViewWrapper = await shallowMountLoginUserView();

    const inputFieldWrappers = loginUserViewWrapper.findAll("input");
    await setInputValue(inputFieldWrappers[0], "a user name");
    await setInputValue(inputFieldWrappers[1], "super duper password");
    const formWrapper = loginUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    assertInputValue(inputFieldWrappers[0], "");
    assertInputValue(inputFieldWrappers[1], "");
  });

  test("show message when login attempt failed", async (): Promise<void> => {
    mockLoginUserResult(false);
    const loginUserViewWrapper = await shallowMountLoginUserView();

    const formWrapper = loginUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);
    const failureMessage = loginUserViewWrapper.find(".failure_message");

    expect(failureMessage.isVisible()).toBe(true);
  });

  test("forwards to start page when login attempt succeeded", async (): Promise<void> => {
    mockLoginUserResult(true);
    const loginUserViewWrapper = await shallowMountLoginUserView();

    const formWrapper = loginUserViewWrapper.find("form");
    await triggerSubmitPrevent(formWrapper);

    expect(getRouter().push).toHaveBeenCalledOnce();
    expect(getRouter().push).toHaveBeenCalledWith("/");
  });
});

async function shallowMountLoginUserView(): Promise<VueWrapper<InstanceType<typeof LoginUserView>>> {
  const wrapper = shallowMount<typeof LoginUserView>(LoginUserView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
  await flushPromises();
  return wrapper;
}

function mockLoginUserResult(result: boolean): void {
  vi.mocked(loginUser).mockResolvedValue(result);
}

function mockRouter(initialLocation: string): void {
  injectRouterMock(
    createRouterMock({
      initialLocation: initialLocation,
      runInComponentGuards: true,
    }),
  );
}
