// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { VueWrapper, flushPromises, shallowMount } from "@vue/test-utils";
import { beforeAll, describe, expect, test, vi } from "vitest";
import i18n from "@/i18n";
import { logoutUser } from "@/service/AuthenticationService";
import LogoutUserView from "@/views/LogoutUserView.vue";

describe("LogoutUserView", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/AuthenticationService");
  });

  test("call logout on mount", async (): Promise<void> => {
    const logoutUserMock = vi.mocked(logoutUser);
    await shallowMountLogoutUserView();
    expect(logoutUserMock).toHaveBeenCalledOnce();
  });

  test("show message on mount", async (): Promise<void> => {
    const logoutUserViewWrapper = await shallowMountLogoutUserView();
    const successMessage = logoutUserViewWrapper.find(".success_message");
    expect(successMessage.isVisible()).toBe(true);
  });
});

async function shallowMountLogoutUserView(): Promise<VueWrapper<InstanceType<typeof LogoutUserView>>> {
  const wrapper = shallowMount<typeof LogoutUserView>(LogoutUserView, {
    global: {
      plugins: [i18n.vueI18n],
    },
  });
  await flushPromises();
  return wrapper;
}
