// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";
import { expect } from "vitest";

export function assertSelectDisabled(selectWrapper: DOMWrapper<HTMLSelectElement>): void {
  expect(selectWrapper.element.disabled).toBe(true);
}

export function assertSelectEnabled(selectWrapper: DOMWrapper<HTMLSelectElement>): void {
  expect(selectWrapper.element.disabled).toBe(false);
}

export function assertSelectValue(selectWrapper: DOMWrapper<HTMLSelectElement>, value: string): void {
  expect(selectWrapper.element.value).toBe(value);
}

export async function setSelectValue(selectWrapper: DOMWrapper<HTMLSelectElement>, value: string): Promise<void> {
  await selectWrapper.setValue(value);
}
