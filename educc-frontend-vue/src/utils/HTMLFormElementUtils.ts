// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";

export async function triggerSubmitPrevent(formWrapper: DOMWrapper<HTMLFormElement>): Promise<void> {
  await formWrapper.trigger("submit.prevent");
}
