// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { Address } from "@/types/Address";
import { Container } from "@/types/Container";
import { Currency } from "@/types/Currency";
import { OpeningHour } from "@/types/OpeningHour";
import { Price } from "@/types/Price";
import { Product } from "@/types/Product";
import { Store } from "@/types/Store";
import { Tag } from "@/types/Tag";
import { UserFeedback } from "@/types/UserFeedback";
import { Weekday } from "@/types/Weekday";

export function defaultAddress(): Address {
  return new Address(
    "address city",
    "address country",
    "address housenumber",
    "address state",
    "address street",
    "address zip",
  );
}

export function defaultContainer(): Container {
  return new Container("container brand", 1000, 1000);
}

export function defaultCurrency(name = "currency name"): Currency {
  return new Currency(1, "currency alpha code", "currency numeric code", 2, name, "currency sign");
}

export function defaultPrice(): Price {
  return new Price(defaultCurrency(), 1, 99, 4, 3, 5, "2022-12-01", "2022-12-03");
}

export function defaultPriceWithOnlyValidFromDate(): Price {
  return new Price(defaultCurrency(), 1, 99, 4, 3, 5, "2022-12-01", null);
}

export function defaultProduct(id = 1, name = "product name", storeName = "store name"): Product {
  return new Product(defaultContainer(), id, name, [defaultPrice()], storeName, ["foo tag", "bar tag"]);
}

export function defaultProductWith0mlContainer(): Product {
  return new Product(new Container("container brand", 0, 1000), 0, "product name", [defaultPrice()], "store name", [
    "foo tag",
    "bar tag",
  ]);
}

export function defaultProductWithNoPrices(): Product {
  return new Product(defaultContainer(), 0, "product name", [], "store name", ["foo tag", "bar tag"]);
}

export function defaultProductWithNoTags(): Product {
  return new Product(defaultContainer(), 0, "product name", [defaultPrice()], "store name", []);
}

export function defaultProductWithThreePrices(): Product {
  const oldPrice = new Price(defaultCurrency(), 1, 49, 4, 3, 5, "2018-06-22", "2021-12-08");
  const middlePrice = new Price(defaultCurrency(), 2, 79, 7, 6, 8, "2021-12-09", "2022-04-23");
  const newPrice = new Price(defaultCurrency(), 3, 89, 10, 9, 11, "2022-04-24", null);

  return new Product(defaultContainer(), 1, "product name", [oldPrice, middlePrice, newPrice], "store name", [
    "foo tag",
    "bar tag",
  ]);
}

export function defaultStore(id = 0, name = "store name"): Store {
  return new Store(defaultAddress(), id, name, [], []);
}

export function defaultStoreWithOneProduct(): Store {
  return new Store(defaultAddress(), 0, "store name", [], [defaultProduct()]);
}

export function defaultStoreWithTwoProducts(id = 0): Store {
  return new Store(defaultAddress(), id, "store name", [], [defaultProduct(), defaultProduct(2, "product 2")]);
}

export function defaultStoreWithOpeningHours(): Store {
  return new Store(defaultAddress(), 0, "store name", [defaultOpeningHour()], []);
}

export function defaultTag(name = "tag name"): Tag {
  return new Tag(0, name);
}

export function defaultUserFeedback(timeStamp = "2024-09-27T11:20:50"): UserFeedback {
  return new UserFeedback(0, "amazing", timeStamp);
}

export function defaultOpeningHour(weekday: Weekday = Weekday.MONDAY): OpeningHour {
  return new OpeningHour(9, 0, 17, 0, 0, weekday);
}
