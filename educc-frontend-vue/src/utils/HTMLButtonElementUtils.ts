// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";
import { expect } from "vitest";

export function assertButtonText(buttonWrapper: DOMWrapper<HTMLButtonElement>, text: string): void {
  expect(buttonWrapper.text()).toBe(text);
}
