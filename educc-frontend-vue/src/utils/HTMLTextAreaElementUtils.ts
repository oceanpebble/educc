// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";
import { expect } from "vitest";

export function assertTextAreaPlaceholder(textAreaWrapper: DOMWrapper<HTMLTextAreaElement>, placeholder: string): void {
  expect(textAreaWrapper.attributes().placeholder).toBe(placeholder);
}

export function assertTextAreaValue(textAreaWrapper: DOMWrapper<HTMLTextAreaElement>, value: string): void {
  expect(textAreaWrapper.element.value).toBe(value);
}

export async function setTextAreaValue(textAreaWrapper: DOMWrapper<HTMLTextAreaElement>, value: string): Promise<void> {
  await textAreaWrapper.setValue(value);
}
