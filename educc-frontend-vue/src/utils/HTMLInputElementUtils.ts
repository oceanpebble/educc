// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";
import { expect } from "vitest";

export function assertInputPlaceholder(inputFieldWrapper: DOMWrapper<HTMLInputElement>, placeholder: string): void {
  expect(inputFieldWrapper.attributes().placeholder).toBe(placeholder);
}

export function assertInputValue(inputFieldWrapper: DOMWrapper<HTMLInputElement>, value: string): void {
  expect(inputFieldWrapper.element.value).toBe(value);
}

export async function setInputValue(inputFieldWrapper: DOMWrapper<HTMLInputElement>, value: string): Promise<void> {
  await inputFieldWrapper.setValue(value);
}
