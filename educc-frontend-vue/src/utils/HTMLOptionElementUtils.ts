// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import type { DOMWrapper } from "@vue/test-utils";
import { expect } from "vitest";

export function assertOptionText(optionWrappers: DOMWrapper<HTMLOptionElement>[], index: number, text: string): void {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  expect(optionWrappers.at(index)!.text()).toBe(text);
}
