// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { describe, expect, test, vi } from "vitest";
import { authHeader, extractUsernameFromJWT } from "@/security/JWTHandler";

describe("JWTHandler", (): void => {
  const validJWT =
    "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmdWJhciIsImlhdCI6MTcwNzQ2MzU0NCwiZXhwIjoxNzA3NTQ5OTQ0fQ.cpHzFei-wrxGW937fHmMsqHcQUv2eyAXRKbVFA-U1y4";

  test("extractUsernameFromJWT returns undefined from an invalid token", (): void => {
    vi.spyOn(Storage.prototype, "getItem").mockReturnValue(null);
    const extractedUsername = extractUsernameFromJWT();
    expect(extractedUsername).toBe(undefined);
  });

  test("extractUsernameFromJWT extracts the user name from a valid token", (): void => {
    vi.spyOn(Storage.prototype, "getItem").mockReturnValue(validJWT);
    const extractedUsername = extractUsernameFromJWT();
    expect(extractedUsername).toBe("fubar");
  });

  test("authHeader creates correct value for Authentication header", (): void => {
    vi.spyOn(Storage.prototype, "getItem").mockReturnValue(validJWT);
    const createdAuthHeader = authHeader();
    expect(createdAuthHeader).toBe("Bearer " + validJWT);
  });
});
