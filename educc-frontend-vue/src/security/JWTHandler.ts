// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { jwtDecode } from "jwt-decode";

export function extractUsernameFromJWT(): string | undefined {
  const jwt = localStorage.getItem("user");
  if (jwt) {
    const decodedJWT = jwtDecode(jwt);
    return decodedJWT.sub;
  }
  return undefined;
}

export function authHeader(): string {
  const jwt = localStorage.getItem("user") ?? "";
  return "Bearer " + jwt;
}
