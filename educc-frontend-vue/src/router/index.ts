// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { type RouteRecordRaw, createRouter, createWebHistory } from "vue-router";
import i18n from "@/i18n";
import { RouteNames, RoutePaths } from "@/router/route_constants";
import { isUserLoggedIn } from "@/service/AuthenticationService";
import DeleteUserView from "@/views/DeleteUserView.vue";
import HomeView from "@/views/HomeView.vue";
import LoginUserView from "@/views/LoginUserView.vue";
import LogoutUserView from "@/views/LogoutUserView.vue";
import NewPriceView from "@/views/NewPriceView.vue";
import NewProductView from "@/views/NewProductView.vue";
import NewStoreView from "@/views/NewStoreView.vue";
import NotFoundView from "@/views/NotFoundView.vue";
import ProductView from "@/views/ProductView.vue";
import ProductsView from "@/views/ProductsView.vue";
import RegisterUserView from "@/views/RegisterUserView.vue";
import StoreView from "@/views/StoreView.vue";
import StoresView from "@/views/StoresView.vue";
import UserFeedbackView from "@/views/UserFeedbackView.vue";

export const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: `/${i18n.defaultLocale}`,
  },
  {
    path: "/:locale",
    children: [
      {
        path: RoutePaths.ALL_PRODUCTS,
        name: RouteNames.ALL_PRODUCTS,
        component: ProductsView,
      },
      {
        path: RoutePaths.ALL_STORES,
        name: RouteNames.ALL_STORES,
        component: StoresView,
      },
      {
        path: RoutePaths.DELETE_USER,
        name: RouteNames.DELETE_USER,
        component: DeleteUserView,
      },
      {
        path: RoutePaths.HOME,
        name: RouteNames.HOME,
        component: HomeView,
      },
      {
        path: RoutePaths.LOGIN_USER,
        name: RouteNames.LOGIN_USER,
        component: LoginUserView,
      },
      {
        path: RoutePaths.LOGOUT_USER,
        name: RouteNames.LOGOUT_USER,
        component: LogoutUserView,
      },
      {
        path: RoutePaths.NEW_PRICE,
        name: RouteNames.NEW_PRICE,
        component: NewPriceView,
        meta: {
          requiresLogin: true,
        },
      },
      {
        path: RoutePaths.NEW_PRODUCT,
        name: RouteNames.NEW_PRODUCT,
        component: NewProductView,
        meta: {
          requiresLogin: true,
        },
      },
      {
        path: RoutePaths.NEW_STORE,
        name: RouteNames.NEW_STORE,
        component: NewStoreView,
        meta: {
          requiresLogin: true,
        },
      },
      {
        path: RoutePaths.REGISTER_USER,
        name: RouteNames.REGISTER_USER,
        component: RegisterUserView,
      },
      {
        path: RoutePaths.SINGLE_PRODUCT,
        name: RouteNames.SINGLE_PRODUCT,
        component: ProductView,
        props: (route): { id: number } => ({ id: Number(route.params.id) }),
      },
      {
        path: RoutePaths.SINGLE_STORE,
        name: RouteNames.SINGLE_STORE,
        component: StoreView,
        props: (route): { id: number } => ({ id: Number(route.params.id) }),
      },
      {
        path: RoutePaths.USER_FEEDBACK,
        name: RouteNames.USER_FEEDBACK,
        component: UserFeedbackView,
        meta: {
          requiresLogin: true,
        },
      },
    ],
  },
  {
    path: RoutePaths.NOT_FOUND,
    component: NotFoundView,
    name: RouteNames.NOT_FOUND,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next): Promise<void> => {
  if (to.matched.some((record): unknown => record.meta.requiresLogin) && !isUserLoggedIn()) {
    next(String(to.params.locale));
  } else {
    const newLocale = String(to.params.locale);
    const prevLocale = String(from.params.locale);

    if (newLocale !== prevLocale) {
      await i18n.loadMessagesFor(newLocale);
      i18n.setLocale(newLocale);
    }

    next();
  }
});

export default router;
