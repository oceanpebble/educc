// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { beforeAll, describe, expect, test, vi } from "vitest";
import router from "@/router/index";
import { RoutePaths } from "@/router/route_constants";
import { isUserLoggedIn } from "@/service/AuthenticationService";

describe("router", (): void => {
  beforeAll((): void => {
    vi.mock("@/service/AuthenticationService");
  });

  test("requires login for specific routes", (): void => {
    const result = router
      .getRoutes()
      .filter((route): boolean => !!route.meta["requiresLogin"])
      .map((route): string => route.name!.toString());
    expect(result).toEqual(routesThatRequireLogin);
  });

  describe("beforeEach navigation guard", (): void => {
    test.each(routesThatRequireLogin)(
      "forwards to home when navigating to %s as a logged out user",
      async (route: string): Promise<void> => {
        vi.mocked(isUserLoggedIn).mockReturnValue(false);
        await router.push("/en-US/" + route);
        expect(router.currentRoute.value.fullPath).toBe("/en-US");
      },
    );

    test.each(routesThatRequireLogin)(
      "forwards to the specified route when navigating to %s as a logged in user",
      async (route: string): Promise<void> => {
        vi.mocked(isUserLoggedIn).mockReturnValue(true);
        await router.push("/en-US/" + route);
        expect(router.currentRoute.value.fullPath).toBe("/en-US/" + route);
      },
    );

    test("switches locale when target route has a different locale", async (): Promise<void> => {
      await router.push("/en-US");
      await router.push("/de-DE");
      expect(router.currentRoute.value.fullPath).toBe("/de-DE");
    });
  });
});

const routesThatRequireLogin = [
  RoutePaths.NEW_PRICE,
  RoutePaths.NEW_PRODUCT,
  RoutePaths.NEW_STORE,
  RoutePaths.USER_FEEDBACK,
];
