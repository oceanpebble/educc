// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { RoutePaths } from "@/router/route_constants";

export function pathForAllProducts(): string {
  return RoutePaths.ALL_PRODUCTS;
}

export function pathForAllStores(): string {
  return RoutePaths.ALL_STORES;
}

export function pathForSingleProductById(id: number): string {
  return RoutePaths.SINGLE_PRODUCT.replace(":id", String(id));
}

export function pathForSingleStoreById(id: number): string {
  return RoutePaths.SINGLE_STORE.replace(":id", String(id));
}
