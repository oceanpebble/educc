// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

export enum RouteNames {
  HOME = "home",
  ALL_PRODUCTS = "products",
  ALL_STORES = "stores",
  DELETE_USER = "delete_user",
  LOGIN_USER = "login_user",
  LOGOUT_USER = "logout_user",
  NEW_PRICE = "new_price",
  NEW_PRODUCT = "new_product",
  NEW_STORE = "new_store",
  NOT_FOUND = "not_found",
  REGISTER_USER = " register_user",
  SINGLE_PRODUCT = "product",
  SINGLE_STORE = "store",
  USER_FEEDBACK = "user_feedback",
}

export enum RoutePaths {
  HOME = "",
  ALL_PRODUCTS = "products",
  ALL_STORES = "stores",
  DELETE_USER = "delete_user",
  LOGIN_USER = "login_user",
  LOGOUT_USER = "logout_user",
  NEW_PRICE = "new_price",
  NEW_PRODUCT = "new_product",
  NEW_STORE = "new_store",
  NOT_FOUND = "/:catchAll(.*)",
  REGISTER_USER = "register_user",
  SINGLE_PRODUCT = "products/:id",
  SINGLE_STORE = "stores/:id",
  USER_FEEDBACK = "user_feedback",
}
