// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT
import { nextTick } from "vue";
import { type Composer, type I18n, createI18n } from "vue-i18n";
import defaultMessages from "@/i18n/messages/messages_en-US.json";

const defaultLocale = "en-US";
let _i18n: I18n;
let _composer: Composer;

function setup(options = { locale: defaultLocale }): I18n {
  _i18n = createI18n<false>({
    legacy: false,
    locale: options.locale,
    fallbackLocale: defaultLocale,
    messages: { [defaultLocale]: defaultMessages },
  });

  _composer = _i18n.global as unknown as Composer;
  setLocale(options.locale);
  return _i18n;
}

function setLocale(newLocale: string): void {
  _composer.locale.value = newLocale;
}

const supportedLocales = [
  { code: "en-US", name: "English (USA)" },
  { code: "de-DE", name: "Deutsch (Deutschland)" },
];

async function loadMessagesFor(locale: string): Promise<void> {
  const messages = await import(`./messages/messages_${locale}.json`);
  (_i18n.global as unknown as Composer).setLocaleMessage(locale, messages.default);
  return nextTick();
}

export default {
  get vueI18n(): I18n {
    return _i18n;
  },
  get vueI18nGlobal(): Composer {
    return _composer;
  },
  get vueI18nLocale(): string {
    return _composer.locale.value;
  },
  loadMessagesFor,
  setup,
  setLocale,
  defaultLocale,
  supportedLocales,
};
