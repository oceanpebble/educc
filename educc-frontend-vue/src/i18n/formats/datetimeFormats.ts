// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

export default {
  "en-US": {
    full: {
      dateStyle: "full",
      timeStyle: "full",
    },
    short: {
      year: "numeric",
      month: "short",
      day: "numeric",
    },
  },
  "de-DE": {
    full: {
      dateStyle: "full",
      timeStyle: "full",
    },
    short: {
      year: "numeric",
      month: "short",
      day: "numeric",
    },
  },
};
