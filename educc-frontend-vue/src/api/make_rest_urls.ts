// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import { ApiPaths, TARGET_HOST } from "@/api/api_constants";

const SLASH = "/";

export function urlForAllCurrencies(): string {
  return TARGET_HOST + ApiPaths.CURRENCIES;
}

export function urlForAllProducts(): string {
  return TARGET_HOST + ApiPaths.PRODUCTS;
}

export function urlForAllStores(): string {
  return TARGET_HOST + ApiPaths.STORES;
}

export function urlForAllTags(): string {
  return TARGET_HOST + ApiPaths.TAGS;
}

export function urlForDeleteUser(): string {
  return TARGET_HOST + ApiPaths.AUTH + SLASH + "delete";
}

export function urlForLatestUserFeedback(count: number): string {
  return TARGET_HOST + ApiPaths.USERFEEDBACK + SLASH + "latest" + SLASH + count;
}

export function urlForLoginUser(): string {
  return TARGET_HOST + ApiPaths.AUTH + SLASH + "login";
}

export function urlForNewPrice(): string {
  return TARGET_HOST + ApiPaths.PRICES;
}

export function urlForNewProduct(): string {
  return TARGET_HOST + ApiPaths.PRODUCTS;
}

export function urlForNewStore(): string {
  return TARGET_HOST + ApiPaths.STORES;
}

export function urlForNewUserFeedback(): string {
  return TARGET_HOST + ApiPaths.USERFEEDBACK;
}

export function urlForRegisterUser(): string {
  return TARGET_HOST + ApiPaths.AUTH + SLASH + "register";
}

export function urlForSingleProduct(id: number): string {
  return TARGET_HOST + ApiPaths.PRODUCTS + SLASH + id;
}

export function urlForSingleStore(id: number): string {
  return TARGET_HOST + ApiPaths.STORES + SLASH + id;
}
