// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

export const TARGET_HOST = "http://localhost:8090";

export enum ApiPaths {
  AUTH = "/auth",
  CURRENCIES = "/currencies",
  PRICES = "/prices",
  PRODUCTS = "/products",
  STORES = "/stores",
  TAGS = "/tags",
  USERFEEDBACK = "/userfeedback",
}
