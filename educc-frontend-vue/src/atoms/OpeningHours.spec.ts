// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { type VueWrapper, shallowMount } from "@vue/test-utils";
import { describe, expect, test } from "vitest";
import OpeningHours from "@/atoms/OpeningHours.vue";
import { OpeningHour } from "@/types/OpeningHour";
import { Weekday } from "@/types/Weekday";
import { defaultOpeningHour } from "@/utils/TestObjectFactory";

describe("OpeningHours", (): void => {
  test("accepts opening hours as an argument", (): void => {
    const openingHoursWrapper = shallowMountOpeningHours([defaultOpeningHour()]);
    expect(openingHoursWrapper.props().openingHours).toStrictEqual([defaultOpeningHour()]);
  });

  test("displays a line for every opening hour", (): void => {
    const openingHoursWrapper = shallowMountOpeningHours([defaultOpeningHour(), defaultOpeningHour(Weekday.TUESDAY)]);
    expect(openingHoursWrapper.text()).toContain("Monday: 09:00 - 17:00");
    expect(openingHoursWrapper.text()).toContain("Tuesday: 09:00 - 17:00");
  });
});

function shallowMountOpeningHours(openingHours: OpeningHour[]): VueWrapper<InstanceType<typeof OpeningHours>> {
  return shallowMount<typeof OpeningHours>(OpeningHours, {
    props: {
      openingHours: openingHours,
    },
  });
}
