// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Product } from "@/types/Product";
import { VueWrapper, shallowMount } from "@vue/test-utils";
import { describe, expect, test } from "vitest";
import { createRouterMock, getRouter, injectRouterMock } from "vue-router-mock";
import ProductSummary from "@/atoms/ProductSummary.vue";
import i18n from "@/i18n";
import { pathForAllProducts } from "@/router/make_router_paths";
import { RouteNames } from "@/router/route_constants";
import {
  defaultProduct,
  defaultProductWithNoPrices,
  defaultProductWithNoTags,
  defaultProductWithThreePrices,
} from "@/utils/TestObjectFactory";

describe("ProductSummary", (): void => {
  test("accepts a product as an argument", (): void => {
    const productSummaryWrapper = shallowMountProductSummary(defaultProduct());
    expect(productSummaryWrapper.props().product).toStrictEqual(defaultProduct());
  });

  test("displays the product's data", (): void => {
    const productSummaryWrapper = shallowMountProductSummary(defaultProduct());

    expect(productSummaryWrapper.html()).toContain("product name (container brand)");
    expect(productSummaryWrapper.html()).toContain("Store: store name");
    expect(productSummaryWrapper.html()).toContain("Container: 1000 ml");
    expect(productSummaryWrapper.html()).toContain("Price: 0.99 currency name");
    expect(productSummaryWrapper.html()).toContain("Tags: foo tag, bar tag");
  });

  test("displays the latest price if the product has more than one price", (): void => {
    const productSummaryWrapper = shallowMountProductSummary(defaultProductWithThreePrices());
    expect(productSummaryWrapper.html()).toContain("Price: 0.89 currency name");
  });

  test("displays no price if the product has no price", (): void => {
    const productSummaryWrapper = shallowMountProductSummary(defaultProductWithNoPrices());
    expect(productSummaryWrapper.html()).not.toContain("Price:");
  });

  test("displays no tags if the product has no tag", (): void => {
    const productSummaryWrapper = shallowMountProductSummary(defaultProductWithNoTags());
    expect(productSummaryWrapper.html()).not.toContain("Tags:");
  });

  test("clicking the summary forwards to detail view", async (): Promise<void> => {
    injectRouterMock(
      createRouterMock({
        initialLocation: pathForAllProducts(),
      }),
    );

    const productSummaryWrapper = shallowMountProductSummary(defaultProduct());
    await productSummaryWrapper.trigger("click");

    expect(getRouter().push).toHaveBeenCalledWith({
      name: RouteNames.SINGLE_PRODUCT,
      params: { id: `${defaultProduct().id}` },
    });
  });
});

function shallowMountProductSummary(product: Product): VueWrapper<InstanceType<typeof ProductSummary>> {
  return shallowMount<typeof ProductSummary>(ProductSummary, {
    props: {
      product: product,
    },
    global: {
      plugins: [i18n.vueI18n],
    },
  });
}
