// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Store } from "@/types/Store";
import { VueWrapper, shallowMount } from "@vue/test-utils";
import { describe, expect, test } from "vitest";
import { createRouterMock, getRouter, injectRouterMock } from "vue-router-mock";
import StoreSummary from "@/atoms/StoreSummary.vue";
import { pathForAllStores } from "@/router/make_router_paths";
import { RouteNames } from "@/router/route_constants";
import { defaultStore } from "@/utils/TestObjectFactory";

describe("StoreSummary", (): void => {
  test("accepts a store as an argument", (): void => {
    const storeSummaryWrapper = shallowMountStoreSummary(defaultStore());
    expect(storeSummaryWrapper.props().store).toStrictEqual(defaultStore());
  });

  test("displays the store's data", (): void => {
    const storeSummaryWrapper = shallowMountStoreSummary(defaultStore());

    expect(storeSummaryWrapper.html()).toContain("store name");
    expect(storeSummaryWrapper.html()).toContain("address street address housenumber");
    expect(storeSummaryWrapper.html()).toContain("address zip address city");
    expect(storeSummaryWrapper.html()).toContain("address state");
    expect(storeSummaryWrapper.html()).toContain("address country");
  });

  test("clicking the summary forwards to detail view", async (): Promise<void> => {
    injectRouterMock(
      createRouterMock({
        initialLocation: pathForAllStores(),
      }),
    );

    const storeSummaryWrapper = shallowMountStoreSummary(defaultStore());
    await storeSummaryWrapper.trigger("click");

    expect(getRouter().push).toHaveBeenCalledWith({
      name: RouteNames.SINGLE_STORE,
      params: { id: `${defaultStore().id}` },
    });
  });
});

function shallowMountStoreSummary(store: Store): VueWrapper<InstanceType<typeof StoreSummary>> {
  return shallowMount<typeof StoreSummary>(StoreSummary, {
    props: {
      store: store,
    },
  });
}
