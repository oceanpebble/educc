// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { type VueWrapper, shallowMount } from "@vue/test-utils";
import { describe, expect, test } from "vitest";
import UserFeedbackSummary from "@/atoms//UserFeedbackSummary.vue";
import i18n from "@/i18n";
import { UserFeedback } from "@/types/UserFeedback";
import { defaultUserFeedback } from "@/utils/TestObjectFactory";

describe("UserFeedbackSummary", (): void => {
  test("accepts a user feedback as an argument", (): void => {
    const userFeedbackSummaryWrapper = shallowMountUserFeedbackSummary(defaultUserFeedback());
    expect(userFeedbackSummaryWrapper.props().userFeedback).toStrictEqual(defaultUserFeedback());
  });

  test("displays the user feedback data", (): void => {
    const userFeedbackSummaryWrapper = shallowMountUserFeedbackSummary(defaultUserFeedback());

    expect(userFeedbackSummaryWrapper.html()).toContain("amazing");
    expect(userFeedbackSummaryWrapper.html()).toContain("9/27/2024, 11:20:50 AM");
  });
});

function shallowMountUserFeedbackSummary(
  userFeedback: UserFeedback,
): VueWrapper<InstanceType<typeof UserFeedbackSummary>> {
  return shallowMount<typeof UserFeedbackSummary>(UserFeedbackSummary, {
    props: {
      userFeedback: userFeedback,
    },
    global: {
      plugins: [i18n.vueI18n],
    },
  });
}
