// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { UserFeedback } from "@/types/UserFeedback";
import { HttpResponse } from "@/dto/HttpResponse";

export class SaveUserFeedbackResponse extends HttpResponse {
  readonly userFeedback: UserFeedback;

  constructor(httpStatus: string, message: string, userFeedback: UserFeedback) {
    super(httpStatus, message);
    this.userFeedback = userFeedback;
  }
}
