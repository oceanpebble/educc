// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

export class SavePriceRequest {
  readonly currencyId: string;
  readonly priceValue: string;
  readonly productId: string;
  readonly purchaseDate: string;
  readonly storeId: string;

  constructor(currencyId: string, priceValue: string, productId: string, purchaseDate: string, storeId: string) {
    this.currencyId = currencyId;
    this.priceValue = priceValue;
    this.productId = productId;
    this.purchaseDate = purchaseDate;
    this.storeId = storeId;
  }
}
