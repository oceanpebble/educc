// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

export class DeleteUserRequest {
  readonly username: string;

  constructor(username: string) {
    this.username = username;
  }
}
