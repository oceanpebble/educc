// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Currency } from "@/types/Currency";
import { HttpResponse } from "@/dto/HttpResponse";

export class FetchCurrencyListResponse extends HttpResponse {
  readonly currencies: Currency[];

  constructor(httpStatus: string, message: string, currencies: Currency[]) {
    super(httpStatus, message);
    this.currencies = currencies;
  }
}
