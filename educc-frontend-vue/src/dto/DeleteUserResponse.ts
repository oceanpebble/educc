// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { HttpResponse } from "@/dto/HttpResponse";

export class DeleteUserResponse extends HttpResponse {}
