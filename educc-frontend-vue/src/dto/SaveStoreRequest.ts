// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

export class SaveStoreRequest {
  readonly city: string;
  readonly country: string;
  readonly houseNumber: string;
  readonly name: string;
  readonly state: string;
  readonly street: string;
  readonly zip: string;

  constructor(
    city: string,
    country: string,
    houseNumber: string,
    name: string,
    state: string,
    street: string,
    zip: string,
  ) {
    this.city = city;
    this.country = country;
    this.houseNumber = houseNumber;
    this.name = name;
    this.state = state;
    this.street = street;
    this.zip = zip;
  }
}
