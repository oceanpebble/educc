// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

export class HttpResponse {
  readonly httpStatus: string;
  readonly message: string;

  constructor(httpStatus: string, messsage: string) {
    this.httpStatus = httpStatus;
    this.message = messsage;
  }
}
