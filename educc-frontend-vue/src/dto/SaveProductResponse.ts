// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Product } from "@/types/Product";
import { HttpResponse } from "@/dto/HttpResponse";

export class SaveProductResponse extends HttpResponse {
  readonly product: Product;

  constructor(httpStatus: string, message: string, product: Product) {
    super(httpStatus, message);
    this.product = product;
  }
}
