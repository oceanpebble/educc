// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Price } from "@/types/Price";
import { HttpResponse } from "@/dto/HttpResponse";

export class SavePriceResponse extends HttpResponse {
  readonly price: Price;

  constructor(httpStatus: string, message: string, price: Price) {
    super(httpStatus, message);
    this.price = price;
  }
}
