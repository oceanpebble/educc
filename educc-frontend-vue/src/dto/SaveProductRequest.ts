// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

export class SaveProductRequest {
  readonly brandName: string;
  readonly name: string;
  readonly storeId: number;
  readonly tagIds: number[];
  readonly volumeInMilliliter: number;
  readonly weightInGram: number;

  constructor(
    brandName: string,
    name: string,
    storeId: number,
    tagIds: number[],
    volumeInMilliliter: number,
    weightInGram: number,
  ) {
    this.brandName = brandName;
    this.name = name;
    this.storeId = storeId;
    this.tagIds = tagIds;
    this.volumeInMilliliter = volumeInMilliliter;
    this.weightInGram = weightInGram;
  }
}
