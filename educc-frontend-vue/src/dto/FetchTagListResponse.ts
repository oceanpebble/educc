// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Tag } from "@/types/Tag";
import { HttpResponse } from "@/dto/HttpResponse";

export class FetchTagListResponse extends HttpResponse {
  readonly tags: Tag[];

  constructor(httpStatus: string, message: string, tags: Tag[]) {
    super(httpStatus, message);
    this.tags = tags;
  }
}
