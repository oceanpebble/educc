// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import { HttpResponse } from "@/dto/HttpResponse";

export class LoginUserResponse extends HttpResponse {
  readonly jwt: string | null;

  constructor(httpStatus: string, message: string, jwt: string | null) {
    super(httpStatus, message);
    this.jwt = jwt;
  }
}
