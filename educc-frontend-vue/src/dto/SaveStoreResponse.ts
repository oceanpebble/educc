// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT
import type { Store } from "@/types/Store";
import { HttpResponse } from "@/dto/HttpResponse";

export class SaveStoreResponse extends HttpResponse {
  readonly store: Store;

  constructor(httpStatus: string, message: string, store: Store) {
    super(httpStatus, message);
    this.store = store;
  }
}
