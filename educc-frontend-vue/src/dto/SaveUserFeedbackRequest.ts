// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

export class SaveUserFeedbackRequest {
  readonly feedbackText: string;

  constructor(feedbackText: string) {
    this.feedbackText = feedbackText;
  }
}
