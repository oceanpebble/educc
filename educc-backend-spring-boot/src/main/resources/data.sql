INSERT INTO STORE(name, city, country, state, house_number, street, zip) VALUES('REWE Markt Esser', 'Rheinbach', 'Germany', 'North Rhine-Westphalia', '3', 'An der alten Molkerei', '53359');
INSERT INTO STORE(name, city, country, state, house_number, street, zip) VALUES('Naturkiste Rheinbach', 'Rheinbach', 'Germany', 'North Rhine-Westphalia', '7', 'Vor dem Voigtstor', '53359');
INSERT INTO STORE(name, city, country, state, house_number, street, zip) VALUES('Hit Rheinbach', 'Rheinbach', 'Germany', 'North Rhine-Westphalia', '8', 'Meckenheimer Straße', '53359');
INSERT INTO STORE(name, city, country, state, house_number, street, zip) VALUES('Obi Rheinbach', 'Rheinbach', 'Germany', 'North Rhine-Westphalia', '1-5', 'An den Märkten', '53359');
INSERT INTO STORE(name, city, country, state, house_number, street, zip) VALUES('Moderna Reinigung & Schneiderei', 'Rheinbach', 'Germany', 'North Rhine-Westphalia', '2', 'Aachenerstraße', '53359');

INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(0, 09, 30, 18, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(1, 09, 30, 18, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(2, 09, 30, 18, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(3, 09, 30, 18, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(4, 09, 30, 18, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(5, 10, 00, 14, 00); -- Naturkiste Rheinbach
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(0, 09, 00, 13, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(0, 14, 00, 18, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(1, 09, 00, 13, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(1, 14, 00, 18, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(2, 09, 00, 13, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(2, 14, 00, 18, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(3, 09, 00, 13, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(3, 14, 00, 18, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(4, 09, 00, 13, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(4, 14, 00, 18, 00); -- Moderna Schneiderei
INSERT INTO OPENINGHOURS(weekday, begin_hour, begin_minute, end_hour, end_minute) VALUES(5, 09, 00, 13, 00); -- Moderna Schneiderei

INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 1);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 2);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 3);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 4);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 5);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(2, 6);  -- Naturkiste Rheinbach
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 7);  -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 8);  -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 9);  -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 10); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 11); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 12); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 13); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 14); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 15); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 16); -- Moderna Schneiderei
INSERT INTO STORE_HOURS(store_id, hours_id) VALUES(5, 17); -- Moderna Schneiderei

INSERT INTO CURRENCY(iso4217numeric_code, iso4217alphabetic_code, minor_unit, name, sign) VALUES('978', 'EUR', 2, 'Euro', '€');
INSERT INTO CURRENCY(iso4217numeric_code, iso4217alphabetic_code, minor_unit, name, sign) VALUES('840', 'USD', 2, 'United States dollar', '$');
INSERT INTO CURRENCY(iso4217numeric_code, iso4217alphabetic_code, minor_unit, name, sign) VALUES('756', 'CHF', 2, 'Swiss franc', null);
INSERT INTO CURRENCY(iso4217numeric_code, iso4217alphabetic_code, minor_unit, name, sign) VALUES('826', 'GBP', 2, 'Sterling', '£');

INSERT INTO PRICE(price_in_currency_minor, price_in_currency_minor_per_liter, price_in_currency_minor_per_kilogram, price_in_currency_minor_per_piece, valid_from, valid_to, currency_id) VALUES(399, 0, 867, 399, '2022-03-24', '2022-04-28', 1);
INSERT INTO PRICE(price_in_currency_minor, price_in_currency_minor_per_liter, price_in_currency_minor_per_kilogram, price_in_currency_minor_per_piece, valid_from, valid_to, currency_id) VALUES(429, 0, 933, 429, '2022-04-29', null, 1);
INSERT INTO PRICE(price_in_currency_minor, price_in_currency_minor_per_liter, price_in_currency_minor_per_kilogram, price_in_currency_minor_per_piece, valid_from, valid_to, currency_id) VALUES(299, 0, 867, 0, '2022-03-24', null, 1);
INSERT INTO PRICE(price_in_currency_minor, price_in_currency_minor_per_liter, price_in_currency_minor_per_kilogram, price_in_currency_minor_per_piece, valid_from, valid_to, currency_id) VALUES(139, 139, 0, 0, '2022-03-24', null, 1);
INSERT INTO PRICE(price_in_currency_minor, price_in_currency_minor_per_liter, price_in_currency_minor_per_kilogram, price_in_currency_minor_per_piece, valid_from, valid_to, currency_id) VALUES(849, 0, 110, 0, '2022-03-24', null, 1);

INSERT INTO PRODUCT(name, brand, volume_in_milliliter, weight_in_gram, store_id) VALUES('Tiefkühlpizza Salami', 'Gustavo Gusto', 0, 460, 1);
INSERT INTO PRODUCT(name, brand, volume_in_milliliter, weight_in_gram, store_id) VALUES('Chicken Wings TK', 'ja!', 0, 750, 3);
INSERT INTO PRODUCT(name, brand, volume_in_milliliter, weight_in_gram, store_id) VALUES('Orangen-Direktsaft', 'Rewe Beste Wahl', 1000, 0, 3);
INSERT INTO PRODUCT(name, brand, volume_in_milliliter, weight_in_gram, store_id) VALUES('Dünnbettmörtel', 'Ytong', 0, 5000, 4);

INSERT INTO PRODUCT_PRICES(product_id, price_id) VALUES(1, 1);
INSERT INTO PRODUCT_PRICES(product_id, price_id) VALUES(1, 2);
INSERT INTO PRODUCT_PRICES(product_id, price_id) VALUES(2, 3);
INSERT INTO PRODUCT_PRICES(product_id, price_id) VALUES(3, 4);
INSERT INTO PRODUCT_PRICES(product_id, price_id) VALUES(4, 5);

INSERT INTO STORE_PRODUCTS(store_id, product_id) VALUES(1, 1);
INSERT INTO STORE_PRODUCTS(store_id, product_id) VALUES(3, 2);
INSERT INTO STORE_PRODUCTS(store_id, product_id) VALUES(3, 3);
INSERT INTO STORE_PRODUCTS(store_id, product_id) VALUES(4, 4);

INSERT INTO TAG(name) VALUES('Flour');
INSERT INTO TAG(name) VALUES('Fruit');
INSERT INTO TAG(name) VALUES('Sweets');
INSERT INTO TAG(name) VALUES('Milk');
INSERT INTO TAG(name) VALUES('Dairy');
INSERT INTO TAG(name) VALUES('Hygiene');
INSERT INTO TAG(name) VALUES('Cereal');
INSERT INTO TAG(name) VALUES('Breakfast');
INSERT INTO TAG(name) VALUES('Food');
INSERT INTO TAG(name) VALUES('Drink');
INSERT INTO TAG(name) VALUES('Building Material');

INSERT INTO PRODUCTS_TAGS(product_id, tag_id) VALUES(1, 9);
INSERT INTO PRODUCTS_TAGS(product_id, tag_id) VALUES(2, 9);
INSERT INTO PRODUCTS_TAGS(product_id, tag_id) VALUES(3, 10);
INSERT INTO PRODUCTS_TAGS(product_id, tag_id) VALUES(4, 11);

INSERT INTO USERS(username, password) VALUES('fubar', '$2a$10$DmgCiMxh5GjzTABtc1X4s.NZigHlLdZWiFUntih0ldirrZ6DJKlLW');