// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
// See https://en.wikipedia.org/wiki/ISO_4217
public class Currency {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String iso4217AlphabeticCode;
	private String iso4217NumericCode;
	private String name;
	private int minorUnit;
	private String sign;
}
