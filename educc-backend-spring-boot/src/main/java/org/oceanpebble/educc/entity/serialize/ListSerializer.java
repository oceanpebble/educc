// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public abstract class ListSerializer<X extends Object, Y extends Object> extends StdSerializer<List<X>> {

	private static final long serialVersionUID = 1L;

	public ListSerializer() {
		this(null);
	}

	public ListSerializer(final Class<List<X>> t) {
		super(t);
	}

	@Override
	public void serialize(final List<X> inputObjects, final JsonGenerator generator, final SerializerProvider provider)
			throws IOException, JsonProcessingException {
		if (inputObjects == null) {
			generator.writeString("");
			return;
		}

		final List<Y> resultObjects = inputObjects.stream().map(this::getValue).collect(toList());
		generator.writeObject(resultObjects);
	}

	protected abstract Y getValue(X object);
}
