// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import org.oceanpebble.educc.entity.Product;

public class ProductsToIdsSerializer extends ListSerializer<Product, Long> {

	private static final long serialVersionUID = 1L;

	@Override
	protected Long getValue(final Product object) {
		return object.getId();
	}
}
