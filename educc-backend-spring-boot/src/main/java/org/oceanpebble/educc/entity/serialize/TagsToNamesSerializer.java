// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import org.oceanpebble.educc.entity.Tag;

public class TagsToNamesSerializer extends ListSerializer<Tag, String> {

	private static final long serialVersionUID = 1L;

	@Override
	protected String getValue(final Tag object) {
		return object.getName();
	}
}
