// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Price {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private int priceInCurrencyMinor;
	private int priceInCurrencyMinorPerLiter;
	private int priceInCurrencyMinorPerKilogram;
	private int priceInCurrencyMinorPerPiece;
	private Date validFrom;
	private Date validTo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference(value = "product_prices")
	private Product product;

	@ManyToOne
	private Currency currency;
}
