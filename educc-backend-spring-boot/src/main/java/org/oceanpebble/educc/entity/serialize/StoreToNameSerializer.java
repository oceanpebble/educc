// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import java.io.IOException;

import org.oceanpebble.educc.entity.Store;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class StoreToNameSerializer extends StdSerializer<Store> {

	private static final long serialVersionUID = 1L;

	public StoreToNameSerializer() {
		this(null);
	}

	protected StoreToNameSerializer(final Class<Store> t) {
		super(t);
	}

	@Override
	public void serialize(final Store value, final JsonGenerator generator, final SerializerProvider provider)
			throws IOException {
		if (value == null) {
			generator.writeString("");
			return;
		}

		final String resultObject = value.getName();
		generator.writeObject(resultObject);
	}
}
