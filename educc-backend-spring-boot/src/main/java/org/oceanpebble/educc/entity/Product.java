// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity;

import java.util.List;

import org.oceanpebble.educc.entity.embeddable.Container;
import org.oceanpebble.educc.entity.serialize.StoreToNameSerializer;
import org.oceanpebble.educc.entity.serialize.TagsToNamesSerializer;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@Embedded
	private Container container;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonSerialize(using = StoreToNameSerializer.class)
	private Store store;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "product_prices", joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "price_id", referencedColumnName = "id"))
	@JsonManagedReference(value = "product_prices")
	private List<Price> prices;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "products_tags", joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
	@JsonSerialize(using = TagsToNamesSerializer.class)
	private List<Tag> tags;

	public Long getStoreId() {
		return getStore() != null ? getStore().getId() : null;
	}

	public String getStoreName() {
		return getStore() != null ? getStore().getName() : null;
	}
}
