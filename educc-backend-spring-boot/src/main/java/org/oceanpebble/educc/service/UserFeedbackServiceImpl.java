// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import org.oceanpebble.educc.dto.SaveUserFeedbackRequest;
import org.oceanpebble.educc.dto.SaveUserFeedbackRequestConverter;
import org.oceanpebble.educc.entity.UserFeedback;
import org.oceanpebble.educc.repository.UserFeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class UserFeedbackServiceImpl implements UserFeedbackService {

	@Autowired
	private UserFeedbackRepository userFeedbackRepository;

	private final SaveUserFeedbackRequestConverter saveUserFeedbackRequestConverter = new SaveUserFeedbackRequestConverter();

	@Override
	public UserFeedback saveUserFeedback(final SaveUserFeedbackRequest saveUserFeedbackRequest) {
		final UserFeedback userFeedback = saveUserFeedbackRequestConverter.makeEntityFrom(saveUserFeedbackRequest);
		return userFeedbackRepository.save(userFeedback);
	}

	@Override
	public Page<UserFeedback> getLatestUserFeedback(final int n) {
		final Pageable pageRequest = PageRequest.of(0, n, Sort.by(Direction.DESC, "timeStamp"));
		return userFeedbackRepository.findAll(pageRequest);
	}
}
