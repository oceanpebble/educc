// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.entity.Tag;

public interface TagService {

	Tag saveTag(Tag tag);

	Tag getTagById(Long tagId);

	List<Tag> fetchTagList();

	Tag updateTag(Tag tag, Long tagId);

	void deleteTagById(Long tagId);
}
