// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.entity.User;

public interface UserService {

	boolean existsByUsername(String username);

	User findByUsername(String username);

	User saveUser(RegisterUserRequest registerUserDTO);

	void deleteUser(DeleteUserRequest deleteUserRequest);
}
