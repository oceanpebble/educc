// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.dto.SavePriceRequest;
import org.oceanpebble.educc.entity.Price;

public interface PriceService {

	Price savePrice(SavePriceRequest savePriceRequest);

	Price getPriceById(Long priceId);

	List<Price> fetchPriceList();

	Price updatePrice(Price price, Long priceId);

	void deletePriceById(Long priceId);

}
