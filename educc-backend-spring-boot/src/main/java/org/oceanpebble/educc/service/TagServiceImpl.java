// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;
import java.util.Objects;

import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagServiceImpl implements TagService {

	@Autowired
	private TagRepository tagRepository;

	@Override
	public Tag saveTag(final Tag tag) {
		return tagRepository.save(tag);
	}

	@Override
	public Tag getTagById(final Long tagId) {
		return tagRepository.findById(tagId).orElse(null);
	}

	@Override
	public List<Tag> fetchTagList() {
		return tagRepository.findAll();
	}

	@Override
	public Tag updateTag(final Tag tag, final Long tagId) {
		final Tag tagFromDB = tagRepository.findById(tagId).get();

		if (Objects.nonNull(tag.getName()) && !"".equalsIgnoreCase(tag.getName())) {
			tagFromDB.setName(tag.getName());
		}

		return tagRepository.save(tagFromDB);
	}

	@Override
	public void deleteTagById(final Long tagId) {
		tagRepository.deleteById(tagId);
	}
}
