// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.dto.SaveProductRequest;
import org.oceanpebble.educc.entity.Product;

public interface ProductService {

	Product saveProduct(SaveProductRequest saveProductRequest);

	Product getProductById(Long productId);

	List<Product> fetchProductList();

	Product updateProduct(Product product, Long productId);

	void deleteProductById(Long productId);

}
