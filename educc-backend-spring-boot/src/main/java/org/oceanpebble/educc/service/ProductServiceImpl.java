// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.oceanpebble.educc.dto.SaveProductRequest;
import org.oceanpebble.educc.dto.SaveProductRequestConverter;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.repository.ProductRepository;
import org.oceanpebble.educc.repository.StoreRepository;
import org.oceanpebble.educc.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private StoreRepository storeRepository;

	@Autowired
	private TagRepository tagRepository;

	private final SaveProductRequestConverter saveProductDTOConverter = new SaveProductRequestConverter();

	@Override
	public Product saveProduct(final SaveProductRequest saveProductRequest) {
		final Store storeFromDB = storeRepository.findById(saveProductRequest.getStoreId()).orElse(null);
		final List<Tag> tagsFromDB = tagRepository.findAllById(Arrays.asList(saveProductRequest.getTagIds()));
		final Product product = saveProductDTOConverter.makeEntityFrom(saveProductRequest, storeFromDB, tagsFromDB);
		return writeProductToDB(product);
	}

	@Override
	public Product getProductById(final Long productId) {
		return productRepository.findById(productId).orElse(null);
	}

	@Override
	public List<Product> fetchProductList() {
		return productRepository.findAll();
	}

	@Override
	public Product updateProduct(final Product product, final Long productId) {
		final Product productFromDB = productRepository.findById(productId).get();

		if (Objects.nonNull(product.getName()) && !"".equalsIgnoreCase(product.getName())) {
			productFromDB.setName(product.getName());
		}

		return productRepository.save(productFromDB);
	}

	@Override
	public void deleteProductById(final Long productId) {
		productRepository.deleteById(productId);
	}

	private Product writeProductToDB(final Product product) {
		final Product newProduct = productRepository.save(product);
		productRepository.flush();
		return newProduct;
	}
}
