// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequestConverter;
import org.oceanpebble.educc.entity.User;
import org.oceanpebble.educc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	private final RegisterUserRequestConverter registerUserRequestConverter = new RegisterUserRequestConverter();

	@Override
	public boolean existsByUsername(final String username) {
		return userRepository.existsByUsername(username);
	}

	@Override
	public User findByUsername(final String username) {
		return userRepository.findByUsername(username).orElse(null);
	}

	@Override
	public User saveUser(final RegisterUserRequest registerUserRequest) {
		final User user = registerUserRequestConverter.makeEntityFrom(registerUserRequest, passwordEncoder);
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public void deleteUser(final DeleteUserRequest deleteUserRequest) {
		userRepository.deleteByUsername(deleteUserRequest.getUsername());
	}
}
