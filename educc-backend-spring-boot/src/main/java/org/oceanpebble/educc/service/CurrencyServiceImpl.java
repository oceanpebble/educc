// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;
import java.util.Objects;

import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyRepository currencyRepository;

	@Override
	public Currency saveCurrency(final Currency currency) {
		return currencyRepository.save(currency);
	}

	@Override
	public Currency getCurrencyById(final Long currencyId) {
		return currencyRepository.findById(currencyId).orElse(null);
	}

	@Override
	public List<Currency> fetchCurrencyList() {
		return currencyRepository.findAll();
	}

	@Override
	public Currency updateCurrency(final Currency currency, final Long currencyId) {
		final Currency currencyFromDB = currencyRepository.findById(currencyId).get();

		if (Objects.nonNull(currency.getName()) && !"".equalsIgnoreCase(currency.getName())) {
			currencyFromDB.setName(currency.getName());
		}

		return currencyRepository.save(currencyFromDB);
	}

	@Override
	public void deleteCurrencyById(final Long currencyId) {
		currencyRepository.deleteById(currencyId);
	}
}
