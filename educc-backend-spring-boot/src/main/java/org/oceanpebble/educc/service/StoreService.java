// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.dto.SaveStoreRequest;
import org.oceanpebble.educc.entity.Store;

public interface StoreService {

	Store saveStore(SaveStoreRequest saveStoreRequest);

	Store getStoreById(Long storeId);

	List<Store> fetchStoreList();

	Store updateStore(Store store, Long storeId);

	void deleteStoreById(Long storeId);

}
