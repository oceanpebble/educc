// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;
import java.util.Objects;

import org.oceanpebble.educc.dto.SaveStoreRequest;
import org.oceanpebble.educc.dto.SaveStoreRequestConverter;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	private StoreRepository storeRepository;

	private final SaveStoreRequestConverter saveStoreRequestConverter = new SaveStoreRequestConverter();

	@Override
	public Store saveStore(final SaveStoreRequest saveStoreRequest) {
		final Store store = saveStoreRequestConverter.makeEntityFrom(saveStoreRequest);
		return writeStoreToDB(store);
	}

	@Override
	public Store getStoreById(final Long storeId) {
		return storeRepository.findById(storeId).orElse(null);
	}

	@Override
	public List<Store> fetchStoreList() {
		return storeRepository.findAll();
	}

	@Override
	public Store updateStore(final Store store, final Long storeId) {
		final Store storeFromDB = storeRepository.findById(storeId).get();

		if (Objects.nonNull(store.getName()) && !"".equalsIgnoreCase(store.getName())) {
			storeFromDB.setName(store.getName());
		}

		return storeRepository.save(storeFromDB);
	}

	@Override
	public void deleteStoreById(final Long storeId) {
		storeRepository.deleteById(storeId);
	}

	private Store writeStoreToDB(final Store store) {
		final Store newStore = storeRepository.save(store);
		storeRepository.flush();
		return newStore;
	}
}
