// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import org.oceanpebble.educc.dto.SaveUserFeedbackRequest;
import org.oceanpebble.educc.entity.UserFeedback;
import org.springframework.data.domain.Page;

public interface UserFeedbackService {

	UserFeedback saveUserFeedback(SaveUserFeedbackRequest saveUserFeedbackRequest);

	Page<UserFeedback> getLatestUserFeedback(int n);
}
