// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.dto.SavePriceRequest;
import org.oceanpebble.educc.dto.SavePriceRequestConverter;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.repository.CurrencyRepository;
import org.oceanpebble.educc.repository.PriceRepository;
import org.oceanpebble.educc.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceServiceImpl implements PriceService {

	@Autowired
	private PriceRepository priceRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CurrencyRepository currencyRepository;

	private final SavePriceRequestConverter savePriceDTOConverter = new SavePriceRequestConverter();

	@Override
	public Price savePrice(final SavePriceRequest savePriceRequest) {
		final Product productFromDB = productRepository.findById(savePriceRequest.getProductId()).orElse(null);
		final Currency currencyFromDB = currencyRepository.findById(savePriceRequest.getCurrencyId()).orElse(null);
		final Price price = savePriceDTOConverter.makeEntityFrom(savePriceRequest, productFromDB, currencyFromDB);
		return writePriceToDB(price, productFromDB);
	}

	@Override
	public Price getPriceById(final Long priceId) {
		return priceRepository.findById(priceId).orElse(null);
	}

	@Override
	public List<Price> fetchPriceList() {
		return priceRepository.findAll();
	}

	@Override
	public Price updatePrice(final Price price, final Long priceId) {
		final Price priceFromDB = priceRepository.findById(priceId).get();
		return priceRepository.save(priceFromDB);
	}

	@Override
	public void deletePriceById(final Long priceId) {
		priceRepository.deleteById(priceId);
	}

	private Price writePriceToDB(final Price price, final Product productFromDB) {
		final Price newPrice = priceRepository.save(price);
		priceRepository.flush();
		productFromDB.getPrices().add(newPrice);
		productRepository.flush();
		return newPrice;
	}
}
