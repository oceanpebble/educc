// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import java.util.List;

import org.oceanpebble.educc.entity.Currency;

public interface CurrencyService {

	Currency saveCurrency(Currency currency);

	Currency getCurrencyById(Long currencyId);

	List<Currency> fetchCurrencyList();

	Currency updateCurrency(Currency currency, Long currencyId);

	void deleteCurrencyById(Long currencyId);

}
