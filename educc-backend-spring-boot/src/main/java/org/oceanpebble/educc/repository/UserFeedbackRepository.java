// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.repository;

import org.oceanpebble.educc.entity.UserFeedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserFeedbackRepository extends JpaRepository<UserFeedback, Long> {

}
