// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaveProductRequest {

	private String brandName;
	private String name;
	private Long storeId;
	private Long[] tagIds;
	private int volumeInMilliliter;
	private int weightInGram;
}
