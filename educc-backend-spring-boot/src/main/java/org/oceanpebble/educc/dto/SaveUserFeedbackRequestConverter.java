// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import java.time.Instant;
import java.util.Date;

import org.oceanpebble.educc.entity.UserFeedback;

public class SaveUserFeedbackRequestConverter {

	public UserFeedback makeEntityFrom(final SaveUserFeedbackRequest saveUserFeedbackRequest) {
		final String feedbackText = saveUserFeedbackRequest.getFeedbackText();
		final Date timeStamp = Date.from(Instant.now());
		return new UserFeedback(0L, feedbackText, timeStamp);
	}
}
