// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.UserFeedback;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SaveUserFeedbackResponse extends HttpResponse {

	private UserFeedback userFeedback;

	public SaveUserFeedbackResponse(final HttpStatus httpStatus, final String message,
			final UserFeedback userFeedback) {
		super(httpStatus, message);
		this.userFeedback = userFeedback;
	}
}
