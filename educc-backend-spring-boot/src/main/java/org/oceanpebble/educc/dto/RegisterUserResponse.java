// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RegisterUserResponse extends HttpResponse {

	public RegisterUserResponse(final HttpStatus httpRequest, final String message) {
		super(httpRequest, message);
	}
}
