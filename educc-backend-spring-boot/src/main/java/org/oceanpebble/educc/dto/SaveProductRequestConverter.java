// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static java.util.Collections.emptyList;

import java.util.List;

import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.entity.embeddable.Container;

public class SaveProductRequestConverter {

	public Product makeEntityFrom(final SaveProductRequest saveProductDTO, final Store store, final List<Tag> tags) {
		return new Product(
				0L, saveProductDTO.getName(), new Container(saveProductDTO.getBrandName(),
						saveProductDTO.getVolumeInMilliliter(), saveProductDTO.getWeightInGram()),
				store, emptyList(), tags);
	}
}
