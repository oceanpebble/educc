// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import java.util.List;

import org.oceanpebble.educc.entity.Currency;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FetchCurrencyListResponse extends HttpResponse {

	private List<Currency> currencies;

	public FetchCurrencyListResponse(final HttpStatus httpStatus, final String message,
			final List<Currency> currencies) {
		super(httpStatus, message);
		this.currencies = currencies;
	}
}
