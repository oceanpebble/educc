// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.Price;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SavePriceResponse extends HttpResponse {

	private Price price;

	public SavePriceResponse(final HttpStatus httpStatus, final String message, final Price price) {
		super(httpStatus, message);
		this.price = price;
	}
}
