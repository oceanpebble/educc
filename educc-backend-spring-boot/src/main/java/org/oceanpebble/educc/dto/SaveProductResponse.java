// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.Product;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SaveProductResponse extends HttpResponse {

	private Product product;

	public SaveProductResponse(final HttpStatus httpStatus, final String message, final Product product) {
		super(httpStatus, message);
		this.product = product;
	}
}
