// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.Store;
import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SaveStoreResponse extends HttpResponse {

	private Store store;

	public SaveStoreResponse(final HttpStatus httpStatus, final String message, final Store store) {
		super(httpStatus, message);
		this.store = store;
	}
}
