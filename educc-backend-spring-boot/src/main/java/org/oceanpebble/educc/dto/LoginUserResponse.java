// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoginUserResponse extends HttpResponse {

	private String jwt;

	public LoginUserResponse(final HttpStatus httpStatus, final String message, final String jwt) {
		super(httpStatus, message);
		this.jwt = jwt;
	}
}
