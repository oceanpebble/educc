// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;

public class RegisterUserRequestConverter {

	public User makeEntityFrom(final RegisterUserRequest registerUserRequest, final PasswordEncoder passwordEncoder) {
		return new User(0L, registerUserRequest.getUsername(),
				passwordEncoder.encode(registerUserRequest.getPassword()));
	}
}
