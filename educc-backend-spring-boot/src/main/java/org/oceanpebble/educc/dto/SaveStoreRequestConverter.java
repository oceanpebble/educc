// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static java.util.Collections.emptyList;

import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.embeddable.Address;

public class SaveStoreRequestConverter {

	public Store makeEntityFrom(final SaveStoreRequest saveStoreRequest) {
		final Address address = new Address(saveStoreRequest.getCity(), saveStoreRequest.getCountry(),
				saveStoreRequest.getHouseNumber(), saveStoreRequest.getState(), saveStoreRequest.getStreet(),
				saveStoreRequest.getZip());
		return new Store(0L, saveStoreRequest.getName(), address, emptyList(), emptyList());
	}
}
