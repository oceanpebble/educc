// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.entity.Product;

public class SavePriceRequestConverter {

	public Price makeEntityFrom(final SavePriceRequest savePriceDTO, final Product product, final Currency currency) {
		if (product.getStoreId() != savePriceDTO.getStoreId()) {
			return null;
		}
		final int priceValue = (int) (Double.valueOf(savePriceDTO.getPriceValue()).doubleValue() * 100);
		return new Price(0L, priceValue, priceValue, priceValue, priceValue, savePriceDTO.getPurchaseDate(), null,
				product, currency);
	}
}
