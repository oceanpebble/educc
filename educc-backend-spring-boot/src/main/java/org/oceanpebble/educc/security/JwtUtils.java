// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.security;

import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtUtils {

	@Value("${org.oceanpebble.educc.jwtSecret}")
	private String jwtSecret;

	@Value("${org.oceanpebble.educc.jwtExpirationMs}")
	private long jwtExpirationMs;

	public String generateJwt(final Authentication authentication) {
		final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		return Jwts.builder().subject(userDetails.getUsername()).issuedAt(new Date())
				.expiration(new Date(new Date().getTime() + jwtExpirationMs)).signWith(key()).compact();
	}

	private SecretKey key() {
		return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
	}

	public String getUsernameFromJwt(final String token) {
		return Jwts.parser().verifyWith(key()).build().parseSignedClaims(token).getPayload().getSubject();
	}

	public boolean validateJwt(final String jwt) {
		try {
			Jwts.parser().verifyWith(key()).build().parse(jwt);
			return true;
		} catch (final Exception e) {
			return false;
		}
	}
}
