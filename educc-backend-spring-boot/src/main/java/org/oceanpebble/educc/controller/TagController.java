// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import java.util.List;

import org.oceanpebble.educc.dto.FetchTagListResponse;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TagController {

	@Autowired
	private TagService tagService;

	@PostMapping("/tags")
	public Tag saveTag(@RequestBody final Tag tag) {
		return tagService.saveTag(tag);
	}

	@GetMapping("/tags/{id}")
	public Tag getTagById(@PathVariable("id") final Long tagId) {
		return tagService.getTagById(tagId);
	}

	@GetMapping("/tags")
	public FetchTagListResponse fetchTagList() {
		final List<Tag> tagList = tagService.fetchTagList();
		return new FetchTagListResponse(HttpStatus.OK, "List of all tags retrieved successfully", tagList);
	}

	@PutMapping("/tags/{id}")
	public Tag updateTag(@RequestBody final Tag tag, @PathVariable("id") final Long tagId) {
		return tagService.updateTag(tag, tagId);
	}

	@DeleteMapping("/tags/{id}")
	public String deleteTagById(@PathVariable("id") final Long tagId) {
		tagService.deleteTagById(tagId);
		return "Deleted successfully";
	}
}
