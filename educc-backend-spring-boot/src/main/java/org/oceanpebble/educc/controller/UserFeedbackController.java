// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import java.util.List;

import org.oceanpebble.educc.dto.SaveUserFeedbackRequest;
import org.oceanpebble.educc.dto.SaveUserFeedbackResponse;
import org.oceanpebble.educc.entity.UserFeedback;
import org.oceanpebble.educc.service.UserFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserFeedbackController {

	@Autowired
	private UserFeedbackService userFeedbackService;

	@PostMapping("/userfeedback")
	public SaveUserFeedbackResponse saveUserFeedback(
			@RequestBody final SaveUserFeedbackRequest saveUserFeedbackRequest) {
		final UserFeedback userFeedback = userFeedbackService.saveUserFeedback(saveUserFeedbackRequest);
		return new SaveUserFeedbackResponse(HttpStatus.OK, "User feedback saved successfully", userFeedback);
	}

	@GetMapping("/userfeedback/latest/{count}")
	public List<UserFeedback> getLatestUserFeedback(@PathVariable final int count) {
		final Page<UserFeedback> userFeedback = userFeedbackService.getLatestUserFeedback(count);
		return userFeedback.toList();
	}
}
