// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.DeleteUserResponse;
import org.oceanpebble.educc.dto.LoginUserRequest;
import org.oceanpebble.educc.dto.LoginUserResponse;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.dto.RegisterUserResponse;
import org.oceanpebble.educc.security.JwtUtils;
import org.oceanpebble.educc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtUtils jwtUtils;

	@PostMapping("/auth/register")
	public RegisterUserResponse registerUser(@RequestBody final RegisterUserRequest registerUserRequest) {
		if (userService.existsByUsername(registerUserRequest.getUsername()))
			return new RegisterUserResponse(HttpStatus.BAD_REQUEST, "A user with this name already exists");

		userService.saveUser(registerUserRequest);
		return new RegisterUserResponse(HttpStatus.OK, "User registered successfully");
	}

	@PostMapping("/auth/login")
	public LoginUserResponse loginUser(@RequestBody final LoginUserRequest loginUserRequest) {
		if (!userService.existsByUsername(loginUserRequest.getUsername()))
			return new LoginUserResponse(HttpStatus.BAD_REQUEST, "A user with this name does not exist", null);

		try {
			final Authentication authenticationToken = new UsernamePasswordAuthenticationToken(
					loginUserRequest.getUsername(), loginUserRequest.getPassword());
			final Authentication authentication = authenticationManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final String jwt = jwtUtils.generateJwt(authentication);
			return new LoginUserResponse(HttpStatus.OK, "User logged in successfully", jwt);
		} catch (final Exception e) {
			return new LoginUserResponse(HttpStatus.BAD_REQUEST, "There was an error during login", null);
		}
	}

	@PostMapping("/auth/delete")
	public DeleteUserResponse deleteUser(@RequestBody final DeleteUserRequest deleteUserRequest) {
		if (!userService.existsByUsername(deleteUserRequest.getUsername()))
			return new DeleteUserResponse(HttpStatus.BAD_REQUEST, "A user with this name does not exist");

		userService.deleteUser(deleteUserRequest);
		return new DeleteUserResponse(HttpStatus.OK, "User deleted successfully");
	}
}
