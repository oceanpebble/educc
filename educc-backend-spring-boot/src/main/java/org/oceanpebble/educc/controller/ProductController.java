// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import java.util.List;

import org.oceanpebble.educc.dto.SaveProductRequest;
import org.oceanpebble.educc.dto.SaveProductResponse;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping("/products")
	public SaveProductResponse saveProduct(@RequestBody final SaveProductRequest saveProductDTO) {
		final Product product = productService.saveProduct(saveProductDTO);
		return new SaveProductResponse(HttpStatus.OK, "Product saved successfully", product);
	}

	@GetMapping("/products/{id}")
	public Product getProductById(@PathVariable("id") final Long productId) {
		return productService.getProductById(productId);
	}

	@GetMapping("/products")
	public List<Product> fetchProductList() {
		return productService.fetchProductList();
	}

	@PutMapping("/products/{id}")
	public Product updateProduct(@RequestBody final Product product, @PathVariable("id") final Long productId) {
		return productService.updateProduct(product, productId);
	}

	@DeleteMapping("/products/{id}")
	public String deleteProductById(@PathVariable("id") final Long productId) {
		productService.deleteProductById(productId);
		return "Deleted successfully";
	}
}
