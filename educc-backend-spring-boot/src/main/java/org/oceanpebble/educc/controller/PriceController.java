// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import java.util.List;

import org.oceanpebble.educc.dto.SavePriceRequest;
import org.oceanpebble.educc.dto.SavePriceResponse;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PriceController {

	@Autowired
	private PriceService priceService;

	@PostMapping("/prices")
	public SavePriceResponse savePrice(@RequestBody final SavePriceRequest savePriceDTO) {
		final Price price = priceService.savePrice(savePriceDTO);
		return new SavePriceResponse(HttpStatus.OK, "Price saved successfully", price);
	}

	@GetMapping("/prices/{id}")
	public Price getPriceById(@PathVariable("id") final Long priceId) {
		return priceService.getPriceById(priceId);
	}

	@GetMapping("/prices")
	public List<Price> fetchPriceList() {
		return priceService.fetchPriceList();
	}

	@PutMapping("/prices/{id}")
	public Price updatePrice(@RequestBody final Price price, @PathVariable("id") final Long priceId) {
		return priceService.updatePrice(price, priceId);
	}

	@DeleteMapping("/prices/{id}")
	public String deletePriceById(@PathVariable("id") final Long priceId) {
		priceService.deletePriceById(priceId);
		return "Deleted successfully";
	}
}
