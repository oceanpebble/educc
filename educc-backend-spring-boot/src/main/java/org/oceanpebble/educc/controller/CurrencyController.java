// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import java.util.List;

import org.oceanpebble.educc.dto.FetchCurrencyListResponse;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyController {

	@Autowired
	private CurrencyService currencyService;

	@PostMapping("/currencies")
	public Currency saveCurrency(@RequestBody final Currency currency) {
		return currencyService.saveCurrency(currency);
	}

	@GetMapping("/currencies/{id}")
	public Currency getCurrencyById(@PathVariable("id") final Long currencyId) {
		return currencyService.getCurrencyById(currencyId);
	}

	@GetMapping("/currencies")
	public FetchCurrencyListResponse fetchCurrencyList() {
		final List<Currency> currencyList = currencyService.fetchCurrencyList();
		return new FetchCurrencyListResponse(HttpStatus.OK, "List of all currencies retrieved successfully",
				currencyList);
	}

	@PutMapping("/currencies/{id}")
	public Currency updateCurrency(@RequestBody final Currency currency, @PathVariable("id") final Long currencyId) {
		return currencyService.updateCurrency(currency, currencyId);
	}

	@DeleteMapping("/currencies/{id}")
	public String deleteCurrencyById(@PathVariable("id") final Long currencyId) {
		currencyService.deleteCurrencyById(currencyId);
		return "Deleted successfully";
	}
}
