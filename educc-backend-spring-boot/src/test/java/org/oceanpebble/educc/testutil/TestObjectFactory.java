// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.testutil;

import static java.util.Collections.emptyList;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.LoginUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.entity.embeddable.Address;
import org.oceanpebble.educc.entity.embeddable.Container;

public class TestObjectFactory {

	public static Currency emptyCurrency(final Long id, final String name) {
		return new Currency(id, "", "", name, 2, "");
	}

	public static Price emptyPrice(final Long id, final int priceInCurrencyMinor) {
		return new Price(id, priceInCurrencyMinor, priceInCurrencyMinor, priceInCurrencyMinor, priceInCurrencyMinor,
				Date.valueOf(LocalDate.of(2023, 1, 10)), Date.valueOf(LocalDate.of(2023, 1, 22)), null, null);
	}

	public static Product emptyProduct(final Long id, final String name) {
		return new Product(id, name, new Container(), emptyStore(1L, "foo store"), new ArrayList<>(), emptyList());
	}

	public static Store emptyStore(final Long id, final String name) {
		return new Store(id, name, new Address(), emptyList(), emptyList());
	}

	public static Tag emptyTag(final Long id, final String name) {
		return new Tag(id, name, emptyList());
	}

	public static RegisterUserRequest defaultRegisterUserRequest() {
		return new RegisterUserRequest("foo", "bar");
	}

	public static DeleteUserRequest defaultDeleteUserRequest() {
		return new DeleteUserRequest("foo");
	}

	public static LoginUserRequest defaultLoginUserRequest() {
		return new LoginUserRequest("username", "password");
	}
}
