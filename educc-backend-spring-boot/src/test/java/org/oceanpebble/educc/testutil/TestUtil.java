// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.testutil;

public class TestUtil {

	public static String jsonArrayWithElements(final String... jsonElements) {
		return "[" + String.join(",", jsonElements) + "]";
	}

	public static String quotedString(final String value) {
		return "\"" + value + "\"";
	}
}
