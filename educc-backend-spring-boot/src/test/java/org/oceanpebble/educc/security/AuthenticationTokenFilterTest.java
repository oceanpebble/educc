// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@ExtendWith(MockitoExtension.class)
class AuthenticationTokenFilterTest {

	@Mock
	private JwtUtils jwtUtils;

	@Mock
	private UserDetailsService userDetailsService;

	@InjectMocks
	private final AuthenticationTokenFilter authenticationTokenFilter = new AuthenticationTokenFilter();

	@BeforeEach
	void setup() {
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	@Test
	void givenAllNullInputs_whenDoFilterInternal_thenDontPutAuthenticationTokenInSecurityContextHolder()
			throws ServletException, IOException {
		final HttpServletRequest request = new MockHttpServletRequest();
		final HttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		authenticationTokenFilter.doFilterInternal(request, response, filterChain);

		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
	}

	@Test
	void givenNonParseableJwt_whenDoFilterInternal_thenDontPutAuthenticationTokenInSecurityContextHolder()
			throws ServletException, IOException {
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "foo");
		final HttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		authenticationTokenFilter.doFilterInternal(request, response, filterChain);

		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
	}

	@Test
	void givenParseableButInvalidJwt_whenDoFilterInternal_thenDontPutAuthenticationTokenInSecurityContextHolder()
			throws ServletException, IOException {
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer foo");
		final HttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		authenticationTokenFilter.doFilterInternal(request, response, filterChain);

		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
	}

	@Test
	void givenValidJwtForNonExistingUser_whenDoFilterInternal_thenDontPutAuthenticationTokenInSecurityContextHolder()
			throws ServletException, IOException {
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer foo");
		final HttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		doReturn(true).when(jwtUtils).validateJwt(anyString());
		doReturn(null).when(userDetailsService).loadUserByUsername(null);

		authenticationTokenFilter.doFilterInternal(request, response, filterChain);

		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
	}

	@Test
	void givenValidJwtForExistingUser_whenDoFilterInternal_thenDontPutAuthenticationTokenInSecurityContextHolder()
			throws ServletException, IOException {
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer foo");
		final HttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();
		final UserDetails userDetails = new UserDetailsImpl(1L, "username", "password");

		doReturn(true).when(jwtUtils).validateJwt(anyString());
		doReturn("foo").when(jwtUtils).getUsernameFromJwt(anyString());
		doReturn(userDetails).when(userDetailsService).loadUserByUsername("foo");

		authenticationTokenFilter.doFilterInternal(request, response, filterChain);

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		assertThat(authentication).isNotNull();
		assertThat(authentication).isInstanceOf(UsernamePasswordAuthenticationToken.class);
		assertThat(authentication.getPrincipal()).isEqualTo(userDetails);
		assertThat(authentication.isAuthenticated()).isTrue();
	}
}
