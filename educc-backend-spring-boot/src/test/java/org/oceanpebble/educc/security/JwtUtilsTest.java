// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import io.jsonwebtoken.ExpiredJwtException;

class JwtUtilsTest {

	private final JwtUtils jwtUtils = new JwtUtils();

	private final String expiredJwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImlhdCI6MTcwNDkyMTgyNiwiZXhwIjoxNzA0OTIxODI2fQ.FSchkhPxkhxOj_okcrMlZaYAl8uJzemLR6C2HfhchAE";

	@BeforeEach
	void setup() {
		ReflectionTestUtils.setField(jwtUtils, "jwtSecret", "===============jwtSecretForTests===============");
		ReflectionTestUtils.setField(jwtUtils, "jwtExpirationMs", 1000L);
	}

	@Test
	void givenValidData_whenGenerateJwsToken_thenATokenIsGenerated() {
		final Authentication authentication = mock(Authentication.class);
		final UserDetails userDetails = new UserDetailsImpl(0L, "username", "password");
		doReturn(userDetails).when(authentication).getPrincipal();

		final String jwt = jwtUtils.generateJwt(authentication);

		assertThat(jwt).isNotEmpty();
	}

	@Test
	void givenAValidJwt_whenGetUsernameFromJwtToken_thenTheUsernameIsCorrectlyExtracted() {
		final Authentication authentication = mock(Authentication.class);
		final UserDetails userDetails = new UserDetailsImpl(0L, "username", "password");
		doReturn(userDetails).when(authentication).getPrincipal();
		final String jwt = jwtUtils.generateJwt(authentication);

		final String username = jwtUtils.getUsernameFromJwt(jwt);
		assertThat(username).isEqualTo("username");
	}

	@Test
	void givenAnExpiredJwt_whenGetUsernameFromJwtToken_thenThrowException() {
		assertThrows(ExpiredJwtException.class, () -> jwtUtils.getUsernameFromJwt(expiredJwt));
	}

	@Test
	void givenAMalformedJwt_whenValidateJwt_thenReturnFalse() {
		final String malformedJwt = ";ojasdp8asd;fjjhas df;asdf8yaps98dyf asudhf";
		final boolean isJwtValid = jwtUtils.validateJwt(malformedJwt);
		assertThat(isJwtValid).isFalse();
	}

	@Test
	void givenAMnExpiredJwt_whenValidateJwt_thenReturnFalse() {
		final boolean isJwtValid = jwtUtils.validateJwt(expiredJwt);
		assertThat(isJwtValid).isFalse();
	}

	@Test
	void givenAValidJwt_whenValidateJwt_thenReturnTrue() {
		final Authentication authentication = mock(Authentication.class);
		final UserDetails userDetails = new UserDetailsImpl(0L, "username", "password");
		doReturn(userDetails).when(authentication).getPrincipal();
		final String jwt = jwtUtils.generateJwt(authentication);

		final boolean isJwtValid = jwtUtils.validateJwt(jwt);

		assertThat(isJwtValid).isTrue();

	}
}
