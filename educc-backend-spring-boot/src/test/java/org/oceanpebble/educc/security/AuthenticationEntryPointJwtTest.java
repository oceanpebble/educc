// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.security;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;

class AuthenticationEntryPointJwtTest {

	private final AuthenticationEntryPointJwt authenticationEntryPointJwt = new AuthenticationEntryPointJwt();

	@Test
	void whenCommence_thenSendError() throws IOException, ServletException {
		final HttpServletResponse responseMock = mock(HttpServletResponse.class);
		authenticationEntryPointJwt.commence(null, responseMock, null);
		verify(responseMock).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized");
	}
}
