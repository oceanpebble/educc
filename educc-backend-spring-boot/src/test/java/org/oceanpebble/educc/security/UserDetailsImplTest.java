// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.security;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.User;

class UserDetailsImplTest {

	private final UserDetailsImpl userDetails = new UserDetailsImpl(0L, "username", "password");

	@Test
	void givenAnyUserDetails_whenGetAuthorities_thenReturnEmptyList() {
		final Collection<?> authorities = userDetails.getAuthorities();
		assertThat(authorities).isEmpty();
	}

	@Test
	void givenAnyUserDetails_whenIsAccountNonExpired_thenReturnTrue() {
		final boolean isAccountNonExpired = userDetails.isAccountNonExpired();
		assertThat(isAccountNonExpired).isTrue();
	}

	@Test
	void givenAnyUserDetails_whenIsAccountNonLocked_thenReturnTrue() {
		final boolean isAccountNonLocked = userDetails.isAccountNonLocked();
		assertThat(isAccountNonLocked).isTrue();
	}

	@Test
	void givenAnyUserDetails_whenIsCredentialsNonExpred_thenReturnTrue() {
		final boolean isCredentialsNonExpired = userDetails.isCredentialsNonExpired();
		assertThat(isCredentialsNonExpired).isTrue();
	}

	@Test
	void givenAnyUserDetails_whenIsEnabled_thenReturnTrue() {
		final boolean isEnabled = userDetails.isEnabled();
		assertThat(isEnabled).isTrue();
	}

	@Test
	void givenAnyUser_whenBuildFromUser_thenReturnCorrectUserDetails() {
		final User user = new User(1L, "a user name", "super duper password");

		final UserDetailsImpl userDetails = UserDetailsImpl.from(user);

		assertThat(userDetails.getId()).isEqualTo(1L);
		assertThat(userDetails.getUsername()).isEqualTo("a user name");
		assertThat(userDetails.getPassword()).isEqualTo("super duper password");
	}
}
