// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.service.UserFeedbackService;

public class UserFeedbackControllerTestBase {

	protected static final String apiPrefixUserFeedback = "/userfeedback";
	protected static final String apiPathLatest = "/latest";
	protected static final String apiSuffixCount1 = "/1";

	@Mock
	protected UserFeedbackService userFeedbackService;

	@InjectMocks
	protected UserFeedbackController userFeedbackController;
}
