// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.LoginUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.security.JwtUtils;
import org.oceanpebble.educc.service.UserService;
import org.oceanpebble.educc.testutil.TestObjectFactory;
import org.springframework.security.authentication.AuthenticationManager;

public class AuthenticationControllerTestBase {

	@Mock
	protected AuthenticationManager authenticationManager;

	@Mock
	protected UserService userService;

	@Mock
	protected JwtUtils jwtUtils;

	@InjectMocks
	protected AuthenticationController authenticationController;

	final RegisterUserRequest registerUserRequest = TestObjectFactory.defaultRegisterUserRequest();
	final LoginUserRequest loginUserRequest = TestObjectFactory.defaultLoginUserRequest();
	final DeleteUserRequest deleteUserRequest = TestObjectFactory.defaultDeleteUserRequest();
}
