// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyStore;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.service.StoreService;

public class StoreControllerTestBase {

	protected static final String apiPrefixStores = "/stores";
	protected static final String apiSuffixId1 = "/1";

	@Mock
	protected StoreService storeService;

	@InjectMocks
	protected StoreController storeController;

	protected final Store storeFoo = emptyStore(1L, "foo store");
	protected final Store storeBar = emptyStore(2L, "bar store");
}
