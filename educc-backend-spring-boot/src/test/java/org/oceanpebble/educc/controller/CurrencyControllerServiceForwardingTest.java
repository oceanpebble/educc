// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CurrencyControllerServiceForwardingTest extends CurrencyControllerTestBase {

	@Test
	public void whenSaveCurrency_thenControllerForwardsToCorrectServiceMethod() {
		currencyController.saveCurrency(currencyFoo);
		verify(currencyService).saveCurrency(currencyFoo);
		verifyNoMoreInteractions(currencyService);
	}

	@Test
	public void whenGetCurrencyById_thenControllerForwardsToCorrectServiceMethod() {
		currencyController.getCurrencyById(1L);
		verify(currencyService).getCurrencyById(1L);
		verifyNoMoreInteractions(currencyService);
	}

	@Test
	public void whenFetchCurrencyList_thenControllerForwardsToCorrectServiceMethod() {
		currencyController.fetchCurrencyList();
		verify(currencyService).fetchCurrencyList();
		verifyNoMoreInteractions(currencyService);
	}

	@Test
	public void whenUpdateCurrencyById_thenControllerForwardsToCorrectServiceMethod() {
		currencyController.updateCurrency(currencyFoo, 1L);
		verify(currencyService).updateCurrency(currencyFoo, 1L);
		verifyNoMoreInteractions(currencyService);
	}

	@Test
	public void whenDeleteCurrencyById_thenControllerForwardsToCorrectServiceMethod() {
		currencyController.deleteCurrencyById(1L);
		verify(currencyService).deleteCurrencyById(1L);
		verifyNoMoreInteractions(currencyService);
	}
}
