// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SavePriceRequest;

@ExtendWith(MockitoExtension.class)
class PriceControllerServiceForwardingTest extends PriceControllerTestBase {

	@Test
	public void whenSavePrice_thenControllerForwardsToCorrectServiceMethod() {
		final SavePriceRequest savePriceRequest = new SavePriceRequest(1L, "1337", 1L, Date.valueOf("2023-10-01"), 1L);
		priceController.savePrice(savePriceRequest);
		verify(priceService).savePrice(savePriceRequest);
		verifyNoMoreInteractions(priceService);
	}

	@Test
	public void whenGetPriceById_thenControllerForwardsToCorrectServiceMethod() {
		priceController.getPriceById(1L);
		verify(priceService).getPriceById(1L);
		verifyNoMoreInteractions(priceService);
	}

	@Test
	public void whenFetchPriceList_thenControllerForwardsToCorrectServiceMethod() {
		priceController.fetchPriceList();
		verify(priceService).fetchPriceList();
		verifyNoMoreInteractions(priceService);
	}

	@Test
	public void whenUpdatePriceById_thenControllerForwardsToCorrectServiceMethod() {
		priceController.updatePrice(priceFoo, 1L);
		verify(priceService).updatePrice(priceFoo, 1L);
		verifyNoMoreInteractions(priceService);
	}

	@Test
	public void whenDeletePriceById_thenControllerForwardsToCorrectServiceMethod() {
		priceController.deletePriceById(1L);
		verify(priceService).deletePriceById(1L);
		verifyNoMoreInteractions(priceService);
	}
}
