// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TagControllerServiceForwardingTest extends TagControllerTestBase {

	@Test
	public void whenSaveTag_thenControllerForwardsToCorrectServiceMethod() {
		tagController.saveTag(tagFoo);
		verify(tagService).saveTag(tagFoo);
		verifyNoMoreInteractions(tagService);
	}

	@Test
	public void whenGetTagById_thenControllerForwardsToCorrectServiceMethod() {
		tagController.getTagById(1L);
		verify(tagService).getTagById(1L);
		verifyNoMoreInteractions(tagService);
	}

	@Test
	public void whenFetchTagList_thenControllerForwardsToCorrectServiceMethod() {
		tagController.fetchTagList();
		verify(tagService).fetchTagList();
		verifyNoMoreInteractions(tagService);
	}

	@Test
	public void whenUpdateTagById_thenControllerForwardsToCorrectServiceMethod() {
		tagController.updateTag(tagFoo, 1L);
		verify(tagService).updateTag(tagFoo, 1L);
		verifyNoMoreInteractions(tagService);
	}

	@Test
	public void whenDeleteTagById_thenControllerForwardsToCorrectServiceMethod() {
		tagController.deleteTagById(1L);
		verify(tagService).deleteTagById(1L);
		verifyNoMoreInteractions(tagService);
	}
}
