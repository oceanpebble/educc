// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PriceControllerRestAccessTest extends PriceControllerTestBase {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PriceService priceService;

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenGet_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixPrices)).andExpect(status().isOk());
	}

	@Test
	@WithMockUser
	void givenAuthorizedUser_whenGet_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixPrices)).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenGetWithId_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixPrices + apiSuffixId1)).andExpect(status().isOk());
	}

	@Test
	@WithMockUser
	void givenAuthorizedUser_whenGetWithId_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixPrices + apiSuffixId1)).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenPost_thenStatusIsUnauthorized() throws Exception {
		mockMvc.perform(post(apiPrefixPrices)).andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser()
	void givenAuthorizedUser_whenPost_thenStatusIsOk() throws Exception {
		mockMvc.perform(post(apiPrefixPrices).header("Content-Type", "application/json").content("{\"id\": \"0\"}"))
				.andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenPut_thenStatusIsUnauthorized() throws Exception {
		mockMvc.perform(post(apiPrefixPrices)).andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser
	void givenAuthorizedUser_whenPut_thenStatusIsOk() throws Exception {
		mockMvc.perform(post(apiPrefixPrices).header("Content-Type", "application/json").content("{\"id\": \"0\"}"))
				.andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenDelete_thenStatusIsUnauthorized() throws Exception {
		mockMvc.perform(post(apiPrefixPrices)).andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser
	void givenAthorizedUser_whenDelete_thenStatusIsOk() throws Exception {
		mockMvc.perform(post(apiPrefixPrices).header("Content-Type", "application/json").content("{\"id\": \"0\"}"))
				.andExpect(status().isOk());
	}
}
