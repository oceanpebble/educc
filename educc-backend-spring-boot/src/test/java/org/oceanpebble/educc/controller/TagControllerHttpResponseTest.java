// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Tag;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class TagControllerHttpResponseTest extends TagControllerTestBase {

	private MockMvc mvc;
	private JacksonTester<Tag> tagJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(tagController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenSaveTag_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(post(apiPrefixTags)
				.content(tagJacksonTester.write(tagFoo).getJson()).contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenGetTagById_givenExistingId_thenReturnTheTag() throws Exception {
		when(tagService.getTagById(1L)).thenReturn(tagFoo);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixTags + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(tagJacksonTester.write(tagFoo).getJson());
	}

	@Test
	public void whenGetTagById_givenNonExistingId_thenReturnEmptyJsonString() throws Exception {
		when(tagService.getTagById(1L)).thenReturn(null);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixTags + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("");
	}

	@Test
	public void whenFetchTagList_givenThereAreTags_thenReturnTheListOfTags() throws Exception {
		when(tagService.fetchTagList()).thenReturn(asList(tagFoo, tagBar));

		final MockHttpServletResponse response = mvc.perform(get(apiPrefixTags).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		final String jsonFoo = tagJacksonTester.write(tagFoo).getJson();
		final String jsonBar = tagJacksonTester.write(tagBar).getJson();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"List of all tags retrieved successfully\"");
		assertThat(response.getContentAsString()).contains("\"tags\":" + jsonArrayWithElements(jsonFoo, jsonBar));
	}

	@Test
	public void whenFetchTagList_givenThereAreNoTags_thenReturnEmptyListJsonString() throws Exception {
		final MockHttpServletResponse response = mvc.perform(get(apiPrefixTags).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"List of all tags retrieved successfully\"");
		assertThat(response.getContentAsString()).contains("\"tags\":[]");
	}

	@Test
	public void whenUpdateTag_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(put(apiPrefixTags + apiSuffixId1)
				.content(tagJacksonTester.write(tagFoo).getJson()).contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenDeleteTagById_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(delete(apiPrefixTags + apiSuffixId1)
				.content(tagJacksonTester.write(tagFoo).getJson()).contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("Deleted successfully");
	}
}
