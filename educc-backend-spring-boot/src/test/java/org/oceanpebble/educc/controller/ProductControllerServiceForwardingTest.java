// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveProductRequest;

@ExtendWith(MockitoExtension.class)
class ProductControllerServiceForwardingTest extends ProductControllerTestBase {

	@Test
	public void whenSaveProduct_thenControllerForwardsToCorrectServiceMethod() {
		final SaveProductRequest saveProductRequest = new SaveProductRequest("foo", "bar", 1L, new Long[] { 2L, 3L },
				100, 200);
		productController.saveProduct(saveProductRequest);
		verify(productService).saveProduct(saveProductRequest);
		verifyNoMoreInteractions(productService);
	}

	@Test
	public void whenGetProductById_thenControllerForwardsToCorrectServiceMethod() {
		productController.getProductById(1L);
		verify(productService).getProductById(1L);
		verifyNoMoreInteractions(productService);
	}

	@Test
	public void whenFetchProductList_thenControllerForwardsToCorrectServiceMethod() {
		productController.fetchProductList();
		verify(productService).fetchProductList();
		verifyNoMoreInteractions(productService);
	}

	@Test
	public void whenUpdateProductById_thenControllerForwardsToCorrectServiceMethod() {
		productController.updateProduct(productFoo, 1L);
		verify(productService).updateProduct(productFoo, 1L);
		verifyNoMoreInteractions(productService);
	}

	@Test
	public void whenDeleteProductById_thenControllerForwardsToCorrectServiceMethod() {
		productController.deleteProductById(1L);
		verify(productService).deleteProductById(1L);
		verifyNoMoreInteractions(productService);
	}
}
