// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Currency;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class CurrencyControllerHttpResponseTest extends CurrencyControllerTestBase {

	private MockMvc mvc;
	private JacksonTester<Currency> currencyJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(currencyController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenSaveCurrency_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(post(apiPrefixCurrencies)
				.content(currencyJacksonTester.write(currencyFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenGetCurrencyById_givenExistingId_thenReturnTheCurrency() throws Exception {
		when(currencyService.getCurrencyById(1L)).thenReturn(currencyFoo);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixCurrencies + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(currencyJacksonTester.write(currencyFoo).getJson());
	}

	@Test
	public void whenGetCurrencyById_givenNonExistingId_thenReturnEmptyJsonString() throws Exception {
		when(currencyService.getCurrencyById(1L)).thenReturn(null);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixCurrencies + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("");
	}

	@Test
	public void whenFetchCurrencyList_givenThereAreCurrencys_thenReturnTheListOfCurrencys() throws Exception {
		when(currencyService.fetchCurrencyList()).thenReturn(asList(currencyFoo, currencyBar));

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixCurrencies).accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

		final String jsonFoo = currencyJacksonTester.write(currencyFoo).getJson();
		final String jsonBar = currencyJacksonTester.write(currencyBar).getJson();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString())
				.contains("\"message\":\"List of all currencies retrieved successfully\"");
		assertThat(response.getContentAsString()).contains("\"currencies\":" + jsonArrayWithElements(jsonFoo, jsonBar));
	}

	@Test
	public void whenFetchCurrencyList_givenThereAreNoCurrencys_thenReturnEmptyListJsonString() throws Exception {
		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixCurrencies).accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString())
				.contains("\"message\":\"List of all currencies retrieved successfully\"");
		assertThat(response.getContentAsString()).contains("\"currencies\":[]");
	}

	@Test
	public void whenUpdateCurrency_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(put(apiPrefixCurrencies + apiSuffixId1)
				.content(currencyJacksonTester.write(currencyFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenDeleteCurrencyById_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(delete(apiPrefixCurrencies + apiSuffixId1)
				.content(currencyJacksonTester.write(currencyFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("Deleted successfully");
	}
}
