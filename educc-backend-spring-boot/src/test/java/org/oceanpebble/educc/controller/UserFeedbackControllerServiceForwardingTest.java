// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveUserFeedbackRequest;
import org.springframework.data.domain.PageImpl;

import io.jsonwebtoken.lang.Collections;

@ExtendWith(MockitoExtension.class)
class UserFeedbackControllerServiceForwardingTest extends UserFeedbackControllerTestBase {

	@Test
	public void whenSaveUserFeedback_thenControllerForwardsToCorrectServiceMethod() {
		final SaveUserFeedbackRequest userFeedbackRequest = new SaveUserFeedbackRequest("wololo");

		userFeedbackController.saveUserFeedback(userFeedbackRequest);

		verify(userFeedbackService).saveUserFeedback(userFeedbackRequest);
		verifyNoMoreInteractions(userFeedbackService);
	}

	@Test
	public void whenGetLatestUserFeedback_thenControllerForwardsToCorrectServiceMethod() {
		when(userFeedbackService.getLatestUserFeedback(anyInt())).thenReturn(new PageImpl<>(Collections.emptyList()));

		userFeedbackController.getLatestUserFeedback(1);

		verify(userFeedbackService).getLatestUserFeedback(1);
		verifyNoMoreInteractions(userFeedbackService);
	}
}
