// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveStoreRequest;

@ExtendWith(MockitoExtension.class)
class StoreControllerServiceForwardingTest extends StoreControllerTestBase {

	@Test
	public void whenSaveStore_thenControllerForwardsToCorrectServiceMethod() {
		final SaveStoreRequest saveStoreRequest = new SaveStoreRequest("foo city", "foo country", "foo houseNumber",
				"foo store", "foo state", "foo street", "foo zip");
		storeController.saveStore(saveStoreRequest);
		verify(storeService).saveStore(saveStoreRequest);
		verifyNoMoreInteractions(storeService);
	}

	@Test
	public void whenGetStoreById_thenControllerForwardsToCorrectServiceMethod() {
		storeController.getStoreById(1L);
		verify(storeService).getStoreById(1L);
		verifyNoMoreInteractions(storeService);
	}

	@Test
	public void whenFetchStoreList_thenControllerForwardsToCorrectServiceMethod() {
		storeController.fetchStoreList();
		verify(storeService).fetchStoreList();
		verifyNoMoreInteractions(storeService);
	}

	@Test
	public void whenUpdateStoreById_thenControllerForwardsToCorrectServiceMethod() {
		storeController.updateStore(storeFoo, 1L);
		verify(storeService).updateStore(storeFoo, 1L);
		verifyNoMoreInteractions(storeService);
	}

	@Test
	public void whenDeleteStoreById_thenControllerForwardsToCorrectServiceMethod() {
		storeController.deleteStoreById(1L);
		verify(storeService).deleteStoreById(1L);
		verifyNoMoreInteractions(storeService);
	}
}
