// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyCurrency;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.service.CurrencyService;

public class CurrencyControllerTestBase {

	protected static final String apiPrefixCurrencies = "/currencies";
	protected static final String apiSuffixId1 = "/1";

	@Mock
	protected CurrencyService currencyService;

	@InjectMocks
	protected CurrencyController currencyController;

	protected final Currency currencyFoo = emptyCurrency(1L, "foo currency");
	protected final Currency currencyBar = emptyCurrency(2L, "bar currency");
}
