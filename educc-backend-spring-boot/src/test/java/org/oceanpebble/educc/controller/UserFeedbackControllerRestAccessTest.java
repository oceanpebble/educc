// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.service.UserFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import io.jsonwebtoken.lang.Collections;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class UserFeedbackControllerRestAccessTest extends UserFeedbackControllerTestBase {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserFeedbackService userFeedbackService;

	@BeforeEach
	void setUp() {
		when(userFeedbackService.getLatestUserFeedback(anyInt())).thenReturn(new PageImpl<>(Collections.emptyList()));
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenGetWithId_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixUserFeedback + apiPathLatest + apiSuffixCount1)).andExpect(status().isOk());
	}

	@Test
	@WithMockUser
	void givenAuthorizedUser_whenGetWithId_thenStatusIsOk() throws Exception {
		mockMvc.perform(get(apiPrefixUserFeedback + apiPathLatest + apiSuffixCount1)).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	void givenUnauthorizedUser_whenPost_thenStatusIsUnauthorized() throws Exception {
		mockMvc.perform(post(apiPrefixUserFeedback)).andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser()
	void givenAuthorizedUser_whenPost_thenStatusIsOk() throws Exception {
		mockMvc.perform(
				post(apiPrefixUserFeedback).header("Content-Type", "application/json").content("{\"id\": \"0\"}"))
				.andExpect(status().isOk());
	}
}
