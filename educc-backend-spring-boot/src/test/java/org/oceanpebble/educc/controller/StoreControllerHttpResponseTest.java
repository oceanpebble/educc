// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Store;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class StoreControllerHttpResponseTest extends StoreControllerTestBase {

	private MockMvc mvc;
	private JacksonTester<Store> storeJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(storeController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenSaveStore_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(post(apiPrefixStores)
				.content(storeJacksonTester.write(storeFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"Store saved successfully\"");
	}

	@Test
	public void whenGetStoreById_givenExistingId_thenReturnTheStore() throws Exception {
		when(storeService.getStoreById(1L)).thenReturn(storeFoo);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixStores + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(storeJacksonTester.write(storeFoo).getJson());
	}

	@Test
	public void whenGetStoreById_givenNonExistingId_thenReturnEmptyJsonString() throws Exception {
		when(storeService.getStoreById(1L)).thenReturn(null);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixStores + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("");
	}

	@Test
	public void whenFetchStoreList_givenThereAreStores_thenReturnTheListOfStores() throws Exception {
		when(storeService.fetchStoreList()).thenReturn(asList(storeFoo, storeBar));

		final MockHttpServletResponse response = mvc.perform(get(apiPrefixStores).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		final String jsonFoo = storeJacksonTester.write(storeFoo).getJson();
		final String jsonBar = storeJacksonTester.write(storeBar).getJson();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements(jsonFoo, jsonBar));
	}

	@Test
	public void whenFetchStoreList_givenThereAreNoStores_thenReturnEmptyListJsonString() throws Exception {
		final MockHttpServletResponse response = mvc.perform(get(apiPrefixStores).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements());
	}

	@Test
	public void whenUpdateStore_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(put(apiPrefixStores + apiSuffixId1)
				.content(storeJacksonTester.write(storeFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenDeleteStoreById_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(delete(apiPrefixStores + apiSuffixId1)
				.content(storeJacksonTester.write(storeFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("Deleted successfully");
	}
}
