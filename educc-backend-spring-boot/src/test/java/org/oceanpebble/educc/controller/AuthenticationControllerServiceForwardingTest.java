// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerServiceForwardingTest extends AuthenticationControllerTestBase {

	@Test
	public void whenRegister_GivenNonExistingUsername_thenControllerForwardsToCorrectServiceMethods() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);

		authenticationController.registerUser(registerUserRequest);

		verify(userService).existsByUsername("foo");
		verify(userService).saveUser(registerUserRequest);
		verifyNoMoreInteractions(userService);
	}

	@Test
	public void whenRegister_GivenExistingUsername_thenControllerForwardsToCorrectServiceMethod() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		authenticationController.registerUser(registerUserRequest);

		verify(userService).existsByUsername("foo");
		verifyNoMoreInteractions(userService);
	}

	@Test
	public void whenLogin_thenControllerForwardsToCorrectServiceMethod() throws Exception {
		authenticationController.loginUser(loginUserRequest);

		verify(userService).existsByUsername("username");
		verifyNoMoreInteractions(userService);
	}

	@Test
	public void whenDelete_GivenNonExistingUsername_thenControllerForwardsToCorrectServiceMethods() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);

		authenticationController.deleteUser(deleteUserRequest);

		verify(userService).existsByUsername("foo");
		verifyNoMoreInteractions(userService);
	}

	@Test
	public void whenDelete_GivenExistingUsername_thenControllerForwardsToCorrectServiceMethod() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		authenticationController.deleteUser(deleteUserRequest);

		verify(userService).existsByUsername("foo");
		verify(userService).deleteUser(deleteUserRequest);
		verifyNoMoreInteractions(userService);
	}
}
