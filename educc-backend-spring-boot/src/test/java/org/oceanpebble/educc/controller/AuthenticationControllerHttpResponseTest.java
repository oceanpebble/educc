// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.LoginUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerHttpResponseTest extends AuthenticationControllerTestBase {

	private static final String apiPrefixRegister = "/auth/register";
	private static final String apiPrefixLogin = "/auth/login";
	private static final String apiPrefixDelete = "/auth/delete";

	private MockMvc mvc;

	private JacksonTester<RegisterUserRequest> registerUserRequestJacksonTester;
	private JacksonTester<LoginUserRequest> loginUserRequestJacksonTester;
	private JacksonTester<DeleteUserRequest> deleteUserRequestJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(authenticationController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenRegister_GivenNonExistingUsername_thenHttpResponseIsOK() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);
		final MockHttpServletResponse response = mvc.perform(
				post(apiPrefixRegister).content(registerUserRequestJacksonTester.write(registerUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"User registered successfully\"");
	}

	@Test
	public void whenRegister_GivenExistingUsername_thenHttpResponseIsBadRequest() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);
		final MockHttpServletResponse response = mvc.perform(
				post(apiPrefixRegister).content(registerUserRequestJacksonTester.write(registerUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"BAD_REQUEST\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"A user with this name already exists\"");
	}

	@Test
	public void whenLogin_GivenNonExistingUsername_thenHttpResponseIsBadRequest() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);

		final MockHttpServletResponse response = mvc
				.perform(post(apiPrefixLogin).content(loginUserRequestJacksonTester.write(loginUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"BAD_REQUEST\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"A user with this name does not exist\"");
		assertThat(response.getContentAsString()).contains("\"jwt\":null");
	}

	@Test
	public void whenLogin_GivenExistingUsernameButInvalidPassword_thenHttpResponseIsBadRequest() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);
		when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);

		final MockHttpServletResponse response = mvc
				.perform(post(apiPrefixLogin).content(loginUserRequestJacksonTester.write(loginUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"BAD_REQUEST\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"There was an error during login\"");
		assertThat(response.getContentAsString()).contains("\"jwt\":null");
	}

	@Test
	public void whenLogin_GivenValidCredentials_thenHttpResponseIsOK() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);
		final Authentication authenticationMock = mock(Authentication.class);
		when(authenticationManager.authenticate(any())).thenReturn(authenticationMock);
		when(jwtUtils.generateJwt(authenticationMock)).thenReturn("mock jwt");

		final MockHttpServletResponse response = mvc
				.perform(post(apiPrefixLogin).content(loginUserRequestJacksonTester.write(loginUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"User logged in successfully\"");
		assertThat(response.getContentAsString()).contains("\"jwt\":\"mock jwt\"");
	}

	@Test
	public void whenDelete_GivenNonExistingUsername_thenHttpResponseIsBadRequest() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(false);

		final MockHttpServletResponse response = mvc.perform(
				post(apiPrefixDelete).content(deleteUserRequestJacksonTester.write(deleteUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"BAD_REQUEST\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"A user with this name does not exist\"");
	}

	@Test
	public void whenDelete_GivenExistingUsername_thenHttpResponseIsOK() throws Exception {
		when(userService.existsByUsername(anyString())).thenReturn(true);

		final MockHttpServletResponse response = mvc.perform(
				post(apiPrefixDelete).content(deleteUserRequestJacksonTester.write(deleteUserRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"User deleted successfully\"");
	}
}