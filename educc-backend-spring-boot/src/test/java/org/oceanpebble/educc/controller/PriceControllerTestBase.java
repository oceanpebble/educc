// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyPrice;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.service.PriceService;

public class PriceControllerTestBase {

	protected static final String apiPrefixPrices = "/prices";
	protected static final String apiSuffixId1 = "/1";

	@Mock
	protected PriceService priceService;

	@InjectMocks
	protected PriceController priceController;

	protected final Price priceFoo = emptyPrice(1L, 1337);
	protected final Price priceBar = emptyPrice(2L, 815);
}
