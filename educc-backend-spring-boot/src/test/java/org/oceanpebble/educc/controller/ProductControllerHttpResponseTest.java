// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Product;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class ProductControllerHttpResponseTest extends ProductControllerTestBase {

	private MockMvc mvc;
	private JacksonTester<Product> productJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(productController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenSaveProduct_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(post(apiPrefixProducts)
				.content(productJacksonTester.write(productFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"Product saved successfully\"");
	}

	@Test
	public void whenGetProductById_givenExistingId_thenReturnTheProduct() throws Exception {
		when(productService.getProductById(1L)).thenReturn(productFoo);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixProducts + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(productJacksonTester.write(productFoo).getJson());
	}

	@Test
	public void whenGetProductById_givenNonExistingId_thenReturnEmptyJsonString() throws Exception {
		when(productService.getProductById(1L)).thenReturn(null);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixProducts + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("");
	}

	@Test
	public void whenFetchProductList_givenThereAreProducts_thenReturnTheListOfProducts() throws Exception {
		when(productService.fetchProductList()).thenReturn(asList(productFoo, productBar));

		final MockHttpServletResponse response = mvc.perform(get(apiPrefixProducts).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		final String jsonFoo = productJacksonTester.write(productFoo).getJson();
		final String jsonBar = productJacksonTester.write(productBar).getJson();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements(jsonFoo, jsonBar));
	}

	@Test
	public void whenFetchProductList_givenThereAreNoProducts_thenReturnEmptyListJsonString() throws Exception {
		final MockHttpServletResponse response = mvc.perform(get(apiPrefixProducts).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements());
	}

	@Test
	public void whenUpdateProduct_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(put(apiPrefixProducts + apiSuffixId1)
				.content(productJacksonTester.write(productFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenDeleteProductById_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(delete(apiPrefixProducts + apiSuffixId1)
				.content(productJacksonTester.write(productFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("Deleted successfully");
	}
}
