// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyProduct;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.service.ProductService;

public class ProductControllerTestBase {

	protected static final String apiPrefixProducts = "/products";
	protected static final String apiSuffixId1 = "/1";

	@Mock
	protected ProductService productService;

	@InjectMocks
	protected ProductController productController;

	protected final Product productFoo = emptyProduct(1L, "foo product");
	protected final Product productBar = emptyProduct(2L, "bar product");
}
