// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyTag;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.service.TagService;

public class TagControllerTestBase {

	protected static final String apiPrefixTags = "/tags";
	protected static final String apiSuffixId1 = "/1";

	@Mock
	protected TagService tagService;

	@InjectMocks
	protected TagController tagController;

	protected final Tag tagFoo = emptyTag(1L, "foo tag");
	protected final Tag tagBar = emptyTag(2L, "bar tag");
}
