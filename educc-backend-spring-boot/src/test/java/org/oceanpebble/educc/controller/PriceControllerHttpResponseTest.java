// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.controller;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.sql.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SavePriceRequest;
import org.oceanpebble.educc.entity.Price;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class PriceControllerHttpResponseTest extends PriceControllerTestBase {

	private MockMvc mvc;
	private JacksonTester<Price> priceJacksonTester;
	private JacksonTester<SavePriceRequest> savePriceRequestJacksonTester;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(priceController).build();
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void whenSavePrice_thenHttpResponseIsOK() throws Exception {
		final SavePriceRequest savePriceRequest = new SavePriceRequest(1L, "13.37", 1L, Date.valueOf("2023-10-01"), 1L);

		final MockHttpServletResponse response = mvc
				.perform(post(apiPrefixPrices).content(savePriceRequestJacksonTester.write(savePriceRequest).getJson())
						.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).contains("\"httpStatus\":\"OK\"");
		assertThat(response.getContentAsString()).contains("\"message\":\"Price saved successfully\"");
	}

	@Test
	public void whenGetPriceById_givenExistingId_thenReturnThePrice() throws Exception {
		when(priceService.getPriceById(1L)).thenReturn(priceFoo);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixPrices + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(priceJacksonTester.write(priceFoo).getJson());
	}

	@Test
	public void whenGetPriceById_givenNonExistingId_thenReturnEmptyJsonString() throws Exception {
		when(priceService.getPriceById(1L)).thenReturn(null);

		final MockHttpServletResponse response = mvc
				.perform(get(apiPrefixPrices + apiSuffixId1).accept(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("");
	}

	@Test
	public void whenFetchPriceList_givenThereArePrices_thenReturnTheListOfPrices() throws Exception {
		when(priceService.fetchPriceList()).thenReturn(asList(priceFoo, priceBar));

		final MockHttpServletResponse response = mvc.perform(get(apiPrefixPrices).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();

		final String jsonFoo = priceJacksonTester.write(priceFoo).getJson();
		final String jsonBar = priceJacksonTester.write(priceBar).getJson();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements(jsonFoo, jsonBar));
	}

	@Test
	public void whenFetchPriceList_givenThereAreNoPrices_thenReturnEmptyListJsonString() throws Exception {
		final MockHttpServletResponse response = mvc.perform(get(apiPrefixPrices).accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonArrayWithElements());
	}

	@Test
	public void whenUpdatePrice_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(put(apiPrefixPrices + apiSuffixId1)
				.content(priceJacksonTester.write(priceFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void whenDeletePriceById_thenHttpResponseIsOK() throws Exception {
		final MockHttpServletResponse response = mvc.perform(delete(apiPrefixPrices + apiSuffixId1)
				.content(priceJacksonTester.write(priceFoo).getJson()).contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo("Deleted successfully");
	}
}
