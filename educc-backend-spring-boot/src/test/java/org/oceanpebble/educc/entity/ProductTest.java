// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.testutil.TestObjectFactory;

class ProductTest {

	@Test
	void givenAProductWithStore_whenGettingStoreId_thenReturnStoreId() {
		final Product product = TestObjectFactory.emptyProduct(1L, "foo product");
		product.setStore(TestObjectFactory.emptyStore(2L, "foo store"));

		final Long storeId = product.getStoreId();

		assertThat(storeId).isEqualTo(2L);
	}

	@Test
	void givenAProductWithoutStore_whenGettingStoreId_thenReturnNull() {
		final Product product = TestObjectFactory.emptyProduct(1L, "foo product");
		product.setStore(null);

		final Long storeId = product.getStoreId();

		assertThat(storeId).isNull();
	}

	@Test
	void givenAProductWithStore_whenGettingStoreName_thenReturnStoreName() {
		final Product product = TestObjectFactory.emptyProduct(1L, "foo product");
		product.setStore(TestObjectFactory.emptyStore(1L, "foo store"));

		final String storeName = product.getStoreName();

		assertThat(storeName).isEqualTo("foo store");
	}

	@Test
	void givenAProductWithoutStore_whenGettingStoreName_thenReturnNull() {
		final Product product = TestObjectFactory.emptyProduct(1L, "foo product");
		product.setStore(null);

		final String storeName = product.getStoreName();

		assertThat(storeName).isNull();
	}
}
