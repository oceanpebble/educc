// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyProduct;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.oceanpebble.educc.testutil.TestUtil.quotedString;

import java.io.StringWriter;
import java.io.Writer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Product;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

class ProductsToIdsSerializerTest {

	private final ProductsToIdsSerializer serializer = new ProductsToIdsSerializer();
	private Writer jsonWriter;
	private JsonGenerator jsonGenerator;
	private SerializerProvider serializerProvider;

	private final Product productFoo = emptyProduct(1L, "foo product");
	private final Product productBar = emptyProduct(2L, "bar product");

	@BeforeEach
	void setUp() throws Exception {
		jsonWriter = new StringWriter();
		jsonGenerator = new JsonFactory().createGenerator(jsonWriter);
		jsonGenerator.setCodec(new ObjectMapper());
		serializerProvider = new ObjectMapper().getSerializerProvider();
	}

	@Test
	void givenANullList_whenSerializing_thenTheResultIsEmpty() throws Exception {
		serializer.serialize(null, jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(quotedString(""));
	}

	@Test
	void givenAnEmptyList_whenSerializing_thenTheResultIsEmpty() throws Exception {
		serializer.serialize(emptyList(), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(jsonArrayWithElements());
	}

	@Test
	void givenAListWithOneProduct_whenSerializing_thenTheResultContainsExactlyTheIdOfThatProduct() throws Exception {
		serializer.serialize(asList(productFoo), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(jsonArrayWithElements(String.valueOf(productFoo.getId())));
	}

	@Test
	void givenAListWithTwoProducts_whenSerializing_thenTheResultContainsExactlyTheIdsOfThoseProducts()
			throws Exception {
		serializer.serialize(asList(productFoo, productBar), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(
				jsonArrayWithElements(String.valueOf(productFoo.getId()), String.valueOf(productBar.getId())));
	}
}
