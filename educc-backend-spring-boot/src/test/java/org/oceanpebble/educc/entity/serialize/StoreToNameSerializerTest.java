// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import static org.assertj.core.api.Assertions.assertThat;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyStore;
import static org.oceanpebble.educc.testutil.TestUtil.quotedString;

import java.io.StringWriter;
import java.io.Writer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Store;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

class StoreToNameSerializerTest {

	private final StoreToNameSerializer serializer = new StoreToNameSerializer();
	private Writer jsonWriter;
	private JsonGenerator jsonGenerator;
	private SerializerProvider serializerProvider;

	private final Store storeFoo = emptyStore(1L, "foo Store");

	@BeforeEach
	void setUp() throws Exception {
		jsonWriter = new StringWriter();
		jsonGenerator = new JsonFactory().createGenerator(jsonWriter);
		jsonGenerator.setCodec(new ObjectMapper());
		serializerProvider = new ObjectMapper().getSerializerProvider();
	}

	@Test
	void givenANullStore_whenSerializing_thenTheResultIsEmpty() throws Exception {
		serializer.serialize(null, jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(quotedString(""));
	}

	@Test
	void givenAStore_whenSerializing_thenTheResultContainsExactlyTheNameOfThatStore() throws Exception {
		serializer.serialize(storeFoo, jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(quotedString(storeFoo.getName()));
	}
}
