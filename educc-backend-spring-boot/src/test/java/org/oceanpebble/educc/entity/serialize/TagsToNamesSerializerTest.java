// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.entity.serialize;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyTag;
import static org.oceanpebble.educc.testutil.TestUtil.jsonArrayWithElements;
import static org.oceanpebble.educc.testutil.TestUtil.quotedString;

import java.io.StringWriter;
import java.io.Writer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Tag;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

class TagsToNamesSerializerTest {

	private final TagsToNamesSerializer serializer = new TagsToNamesSerializer();
	private Writer jsonWriter;
	private JsonGenerator jsonGenerator;
	private SerializerProvider serializerProvider;

	private final Tag tagFoo = emptyTag(1L, "foo tag");
	private final Tag tagBar = emptyTag(2L, "bar tag");

	@BeforeEach
	void setUp() throws Exception {
		jsonWriter = new StringWriter();
		jsonGenerator = new JsonFactory().createGenerator(jsonWriter);
		jsonGenerator.setCodec(new ObjectMapper());
		serializerProvider = new ObjectMapper().getSerializerProvider();
	}

	@Test
	void givenANullList_whenSerializing_thenTheResultIsEmpty() throws Exception {
		serializer.serialize(null, jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(quotedString(""));
	}

	@Test
	void givenAnEmptyList_whenSerializing_thenTheResultIsEmpty() throws Exception {
		serializer.serialize(emptyList(), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(jsonArrayWithElements());
	}

	@Test
	void givenAListWithOneTag_whenSerializing_thenTheResultContainsExactlyTheNameOfThatTag() throws Exception {
		serializer.serialize(asList(tagFoo), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result).isEqualTo(jsonArrayWithElements(quotedString(tagFoo.getName())));
	}

	@Test
	void givenAListWithTwoTags_whenSerializing_thenTheResultContainsExactlyTheNamesOfThoseTag() throws Exception {
		serializer.serialize(asList(tagFoo, tagBar), jsonGenerator, serializerProvider);
		jsonGenerator.flush();

		final String result = jsonWriter.toString();
		assertThat(result)
				.isEqualTo(jsonArrayWithElements(quotedString(tagFoo.getName()), quotedString(tagBar.getName())));
	}
}
