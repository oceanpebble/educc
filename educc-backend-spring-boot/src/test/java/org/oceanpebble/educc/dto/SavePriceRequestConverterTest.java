// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyStore;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.entity.Product;

public class SavePriceRequestConverterTest {

	private final SavePriceRequestConverter converter = new SavePriceRequestConverter();
	private final SavePriceRequest savePriceRequest1 = new SavePriceRequest(1L, "13.37", 2L, new Date(0L), 3L);
	private final SavePriceRequest savePriceRequest2 = new SavePriceRequest(1L, "13.37", 2L, new Date(0L), 30L);
	private final Product product = new Product(3L, null, null, emptyStore(3L, null), emptyList(), emptyList());
	private final Currency currency = new Currency();

	@Test
	void givenASavePriceRequestWithMatchingStoreIds_whenMakeEntityFrom_thenReturnAPriceWithTheDataFromTheRequest() {
		final Price price = converter.makeEntityFrom(savePriceRequest1, product, currency);

		assertThat(price.getId()).isEqualTo(0);
		assertThat(price.getPriceInCurrencyMinor()).isEqualTo(1337);
		assertThat(price.getPriceInCurrencyMinorPerKilogram()).isEqualTo(1337);
		assertThat(price.getPriceInCurrencyMinorPerLiter()).isEqualTo(1337);
		assertThat(price.getPriceInCurrencyMinorPerPiece()).isEqualTo(1337);
		assertThat(price.getValidFrom()).isEqualTo(new Date(0L));
		assertThat(price.getValidTo()).isNull();
		assertThat(price.getProduct()).isEqualTo(product);
		assertThat(price.getCurrency()).isEqualTo(currency);
	}

	@Test
	void givenASavePriceDRequestWithNonMatchingStoreIds_whenMakeEntityFrom_thenReturnNull() {
		final Price price = converter.makeEntityFrom(savePriceRequest2, product, currency);
		assertThat(price).isNull();
	}
}
