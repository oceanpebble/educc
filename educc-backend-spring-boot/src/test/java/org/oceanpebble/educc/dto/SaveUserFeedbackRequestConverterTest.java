// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.oceanpebble.educc.entity.UserFeedback;

class SaveUserFeedbackRequestConverterTest {

	private final SaveUserFeedbackRequestConverter converter = new SaveUserFeedbackRequestConverter();

	@Test
	void givenASaveUserFeedbackRequest_whenMakeEntityFrom_thenReturnAUserFeedbackWithTheDataFromTheRequest() {
		final Clock clock = Clock.fixed(Instant.parse("2024-09-20T14:51:43Z"), ZoneId.of("UTC"));
		final Instant instant = Instant.now(clock);

		try (MockedStatic<Instant> mockedStatic = mockStatic(Instant.class)) {
			mockedStatic.when(Instant::now).thenReturn(instant);
			final SaveUserFeedbackRequest saveUserFeedbackRequest = new SaveUserFeedbackRequest("this web site rules!");

			final UserFeedback userFeedback = converter.makeEntityFrom(saveUserFeedbackRequest);

			assertThat(userFeedback.getId()).isEqualTo(0);
			assertThat(userFeedback.getFeedbackText()).isEqualTo("this web site rules!");
			assertThat(userFeedback.getTimeStamp()).isEqualTo(Date.from(instant));
		}
	}
}
