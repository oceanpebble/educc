// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.User;
import org.oceanpebble.educc.testutil.TestObjectFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

public class RegisterUserRequestConverterTest {

	private final RegisterUserRequestConverter converter = new RegisterUserRequestConverter();

	@Test
	void givenARegisterUserRequest_whenMakeEntityFrom_thenReturnAUserWithTheDataFromTheRequest() {
		final RegisterUserRequest registerUserRequest = TestObjectFactory.defaultRegisterUserRequest();
		final PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class);

		final User user = converter.makeEntityFrom(registerUserRequest, passwordEncoderMock);

		assertThat(user.getId()).isEqualTo(0);
		assertThat(user.getUsername()).isEqualTo(registerUserRequest.getUsername());
		verify(passwordEncoderMock).encode(registerUserRequest.getPassword());
		assertThat(user.getPassword()).isEqualTo(passwordEncoderMock.encode(registerUserRequest.getPassword()));
	}
}
