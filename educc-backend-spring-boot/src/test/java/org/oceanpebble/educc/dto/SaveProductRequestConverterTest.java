// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyStore;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.embeddable.Container;

public class SaveProductRequestConverterTest {

	private final SaveProductRequestConverter converter = new SaveProductRequestConverter();

	@Test
	void givenASaveProductRequest_whenMakeEntityFrom_thenReturnAProductWithTheDataFromTheRequest() {
		final SaveProductRequest saveProductRequest = new SaveProductRequest("foo brand", "foo name", 1L, new Long[] {},
				100, 200);
		final Store store = emptyStore(31L, null);

		final Product product = converter.makeEntityFrom(saveProductRequest, store, emptyList());

		assertThat(product.getId()).isEqualTo(0);
		assertThat(product.getName()).isEqualTo("foo name");
		assertThat(product.getContainer()).isEqualTo(new Container("foo brand", 100, 200));
		assertThat(product.getStore()).isEqualTo(store);
		assertThat(product.getPrices()).isEqualTo(emptyList());
		assertThat(product.getTags()).isEqualTo(emptyList());
	}
}
