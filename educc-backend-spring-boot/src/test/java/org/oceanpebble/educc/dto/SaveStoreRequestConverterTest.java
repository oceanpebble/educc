// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.dto;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.embeddable.Address;

public class SaveStoreRequestConverterTest {

	private final SaveStoreRequestConverter converter = new SaveStoreRequestConverter();

	@Test
	void givenASaveStoreRequest_whenMakeEntityFrom_thenReturnAStoreWithTheDataFromTheRequest() {
		final SaveStoreRequest saveStoreRequest = new SaveStoreRequest("foo city", "foo country", "foo houseNumber",
				"foo name", "foo state", "foo street", "foo zip");

		final Store store = converter.makeEntityFrom(saveStoreRequest);

		assertThat(store.getId()).isEqualTo(0);
		assertThat(store.getName()).isEqualTo("foo name");
		assertThat(store.getAddress()).isEqualTo(
				new Address("foo city", "foo country", "foo houseNumber", "foo state", "foo street", "foo zip"));
		assertThat(store.getProducts()).isEqualTo(emptyList());
	}
}
