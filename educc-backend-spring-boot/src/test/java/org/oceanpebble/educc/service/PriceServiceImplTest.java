// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyCurrency;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyPrice;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyProduct;

import java.sql.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SavePriceRequest;
import org.oceanpebble.educc.dto.SavePriceRequestConverter;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.entity.Price;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.repository.CurrencyRepository;
import org.oceanpebble.educc.repository.PriceRepository;
import org.oceanpebble.educc.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
class PriceServiceImplTest {

	@Mock
	private PriceRepository priceRepository;

	@Mock
	private ProductRepository productRepository;

	@Mock
	private CurrencyRepository currencyRepository;

	@InjectMocks
	private PriceServiceImpl priceService;

	private final Price priceFoo = emptyPrice(1L, 1337);
	private final SavePriceRequestConverter savePriceRequestConverter = new SavePriceRequestConverter();

	@Test
	public void whenSavePrice_thenServiceForwardsToCorrectRepositoryMethod() {
		final Product product = emptyProduct(1L, "foo product");
		final Currency currency = emptyCurrency(2L, "foo currency");
		doReturn(Optional.of(product)).when(productRepository).findById(any());
		doReturn(Optional.of(currency)).when(currencyRepository).findById(any());

		final SavePriceRequest savePriceDTO = new SavePriceRequest(1L, "1337", 1L, Date.valueOf("2023-10-01"), 1L);
		priceService.savePrice(savePriceDTO);

		product.getPrices().add(null);
		final Price price = savePriceRequestConverter.makeEntityFrom(savePriceDTO, product, currency);
		verify(priceRepository).save(price);
		verify(priceRepository).flush();
		verifyNoMoreInteractions(priceRepository);
	}

	@Test
	public void whenGetPriceById_thenServiceForwardsToCorrectRepositoryMethod() {
		priceService.getPriceById(1L);
		verify(priceRepository).findById(1L);
		verifyNoMoreInteractions(priceRepository);
	}

	@Test
	public void whenGetPricebyId_forExistingId_thenReturnThePrice() throws Exception {
		when(priceRepository.findById(1L)).thenReturn(Optional.of(priceFoo));
		final Price fetchedPrice = priceService.getPriceById(1L);
		assertThat(fetchedPrice).isEqualTo(priceFoo);
	}

	@Test
	public void whenGetPricebyId_forNonExistingId_thenReturnNull() throws Exception {
		lenient().when(priceRepository.findById(1L)).thenReturn(Optional.of(priceFoo));
		final Price fetchedPrice = priceService.getPriceById(2L);
		assertThat(fetchedPrice).isNull();
	}

	@Test
	public void whenFetchPriceList_thenServiceForwardsToCorrectRepositoryMethod() {
		priceService.fetchPriceList();
		verify(priceRepository).findAll();
		verifyNoMoreInteractions(priceRepository);
	}

	@Test
	public void whenUpdatePrice_thenServiceForwardsToCorrectRepositoryMethods_thenUpdateName() {
		final Price priceBar = emptyPrice(2L, 815);
		when(priceRepository.findById(1L)).thenReturn(Optional.of(priceFoo));

		priceService.updatePrice(priceBar, 1L);

		verify(priceRepository).findById(1L);
		verify(priceRepository).save(priceFoo);
		verifyNoMoreInteractions(priceRepository);
	}

	@Test
	public void whenDeletePrice_thenServiceForwardsToCorrectRepositoryMethod() {
		priceService.deletePriceById(1L);
		verify(priceRepository).deleteById(1L);
		verifyNoMoreInteractions(priceRepository);
	}
}
