// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyCurrency;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Currency;
import org.oceanpebble.educc.repository.CurrencyRepository;

@ExtendWith(MockitoExtension.class)
class CurrencyServiceImplTest {

	@Mock
	private CurrencyRepository currencyRepository;

	@InjectMocks
	private CurrencyServiceImpl currencyService;

	final Currency currencyFoo = emptyCurrency(1L, "foo currency");

	@Test
	public void whenSaveCurrency_thenServiceForwardsToCorrectRepositoryMethod() {
		currencyService.saveCurrency(currencyFoo);
		verify(currencyRepository).save(currencyFoo);
		verifyNoMoreInteractions(currencyRepository);
	}

	@Test
	public void whenGetCurrencyById_thenServiceForwardsToCorrectRepositoryMethod() {
		currencyService.getCurrencyById(1L);
		verify(currencyRepository).findById(1L);
		verifyNoMoreInteractions(currencyRepository);
	}

	@Test
	public void whenGetCurrencybyId_forExistingId_thenReturnTheCurrency() throws Exception {
		when(currencyRepository.findById(1L)).thenReturn(Optional.of(currencyFoo));
		final Currency fetchedCurrency = currencyService.getCurrencyById(1L);
		assertThat(fetchedCurrency).isEqualTo(currencyFoo);
	}

	@Test
	public void whenGetCurrencybyId_forNonExistingId_thenReturnNull() throws Exception {
		lenient().when(currencyRepository.findById(1L)).thenReturn(Optional.of(currencyFoo));
		final Currency fetchedCurrency = currencyService.getCurrencyById(2L);
		assertThat(fetchedCurrency).isNull();
	}

	@Test
	public void whenFetchCurrencyList_thenServiceForwardsToCorrectRepositoryMethod() {
		currencyService.fetchCurrencyList();
		verify(currencyRepository).findAll();
		verifyNoMoreInteractions(currencyRepository);
	}

	@Test
	public void whenUpdateCurrency_thenServiceForwardsToCorrectRepositoryMethods_thenUpdateName() {
		final Currency currencyBar = emptyCurrency(2L, "bar currency");
		when(currencyRepository.findById(1L)).thenReturn(Optional.of(currencyFoo));

		currencyService.updateCurrency(currencyBar, 1L);

		assertThat(currencyFoo.getName()).isEqualTo(currencyBar.getName());
		verify(currencyRepository).findById(1L);
		verify(currencyRepository).save(currencyFoo);
		verifyNoMoreInteractions(currencyRepository);
	}

	@Test
	public void whenDeleteCurrency_thenServiceForwardsToCorrectRepositoryMethod() {
		currencyService.deleteCurrencyById(1L);
		verify(currencyRepository).deleteById(1L);
		verifyNoMoreInteractions(currencyRepository);
	}
}
