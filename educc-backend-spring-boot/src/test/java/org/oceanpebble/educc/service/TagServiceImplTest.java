// SPDX-FileCopyrightText: 2023 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyTag;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.repository.TagRepository;

@ExtendWith(MockitoExtension.class)
class TagServiceImplTest {

	@Mock
	private TagRepository tagRepository;

	@InjectMocks
	private TagServiceImpl tagService;

	final Tag tagFoo = emptyTag(1L, "foo tag");

	@Test
	public void whenSaveTag_thenServiceForwardsToCorrectRepositoryMethod() {
		tagService.saveTag(tagFoo);
		verify(tagRepository).save(tagFoo);
		verifyNoMoreInteractions(tagRepository);
	}

	@Test
	public void whenGetTagById_thenServiceForwardsToCorrectRepositoryMethod() {
		tagService.getTagById(1L);
		verify(tagRepository).findById(1L);
		verifyNoMoreInteractions(tagRepository);
	}

	@Test
	public void whenGetTagbyId_forExistingId_thenReturnTheTag() throws Exception {
		when(tagRepository.findById(1L)).thenReturn(Optional.of(tagFoo));
		final Tag fetchedTag = tagService.getTagById(1L);
		assertThat(fetchedTag).isEqualTo(tagFoo);
	}

	@Test
	public void whenGetTagbyId_forNonExistingId_thenReturnNull() throws Exception {
		lenient().when(tagRepository.findById(1L)).thenReturn(Optional.of(tagFoo));
		final Tag fetchedTag = tagService.getTagById(2L);
		assertThat(fetchedTag).isNull();
	}

	@Test
	public void whenFetchTagList_thenServiceForwardsToCorrectRepositoryMethod() {
		tagService.fetchTagList();
		verify(tagRepository).findAll();
		verifyNoMoreInteractions(tagRepository);
	}

	@Test
	public void whenUpdateTag_thenServiceForwardsToCorrectRepositoryMethods_thenUpdateName() {
		final Tag tagBar = emptyTag(2L, "bar tag");
		when(tagRepository.findById(1L)).thenReturn(Optional.of(tagFoo));

		tagService.updateTag(tagBar, 1L);

		assertThat(tagFoo.getName()).isEqualTo(tagBar.getName());
		verify(tagRepository).findById(1L);
		verify(tagRepository).save(tagFoo);
		verifyNoMoreInteractions(tagRepository);
	}

	@Test
	public void whenDeleteTag_thenServiceForwardsToCorrectRepositoryMethod() {
		tagService.deleteTagById(1L);
		verify(tagRepository).deleteById(1L);
		verifyNoMoreInteractions(tagRepository);
	}
}
