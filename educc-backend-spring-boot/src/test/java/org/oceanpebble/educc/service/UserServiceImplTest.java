// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.DeleteUserRequest;
import org.oceanpebble.educc.dto.RegisterUserRequest;
import org.oceanpebble.educc.entity.User;
import org.oceanpebble.educc.repository.UserRepository;
import org.oceanpebble.educc.testutil.TestObjectFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@InjectMocks
	private UserServiceImpl userService;

	@Test
	public void whenExistsByUsername_thenServiceForwardsToCorrectRepositoryMethod() {
		userService.existsByUsername("");
		verify(userRepository).existsByUsername("");
		verifyNoMoreInteractions(userRepository);
	}

	@Test
	public void whenFindByUsername_thenServiceForwardsToCorrectRepositoryMethod() {
		userService.findByUsername("");
		verify(userRepository).findByUsername("");
		verifyNoMoreInteractions(userRepository);
	}

	@Test
	public void whenSaveUser_thenServiceForwardsToCorrectRepositoryMethod() {
		final RegisterUserRequest registerUserRequest = TestObjectFactory.defaultRegisterUserRequest();

		userService.saveUser(registerUserRequest);

		verify(passwordEncoder).encode(registerUserRequest.getPassword());
		verify(userRepository).save(new User(0L, registerUserRequest.getUsername(),
				passwordEncoder.encode(registerUserRequest.getPassword())));
		verifyNoMoreInteractions(userRepository);
	}

	@Test
	public void whenDeleteByUsername_thenServiceForwardsToCorrectRepositoryMethod() {
		final DeleteUserRequest deleteUserRequest = TestObjectFactory.defaultDeleteUserRequest();

		userService.deleteUser(deleteUserRequest);

		verify(userRepository).deleteByUsername(deleteUserRequest.getUsername());
		verifyNoMoreInteractions(userRepository);
	}
}
