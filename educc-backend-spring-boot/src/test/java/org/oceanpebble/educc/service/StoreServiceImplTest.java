// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyStore;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveStoreRequest;
import org.oceanpebble.educc.dto.SaveStoreRequestConverter;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.repository.StoreRepository;

@ExtendWith(MockitoExtension.class)
class StoreServiceImplTest {

	@Mock
	private StoreRepository storeRepository;

	@InjectMocks
	private StoreServiceImpl storeService;

	private final Store storeFoo = emptyStore(1L, "foo store");
	private final SaveStoreRequestConverter saveStoreDTOConverter = new SaveStoreRequestConverter();

	@Test
	public void whenSaveStore_thenServiceForwardsToCorrectRepositoryMethod() {
		final SaveStoreRequest saveStoreRequest = new SaveStoreRequest("foo store", "foo street", "foo houseNumber",
				"foo zip", "foo city", "foo state", "foo country");
		storeService.saveStore(saveStoreRequest);

		final Store store = saveStoreDTOConverter.makeEntityFrom(saveStoreRequest);
		verify(storeRepository).save(store);
		verify(storeRepository).flush();
		verifyNoMoreInteractions(storeRepository);
	}

	@Test
	public void whenGetStoreById_thenServiceForwardsToCorrectRepositoryMethod() {
		storeService.getStoreById(1L);
		verify(storeRepository).findById(1L);
		verifyNoMoreInteractions(storeRepository);
	}

	@Test
	public void whenGetStorebyId_forExistingId_thenReturnTheStore() throws Exception {
		when(storeRepository.findById(1L)).thenReturn(Optional.of(storeFoo));
		final Store fetchedStore = storeService.getStoreById(1L);
		assertThat(fetchedStore).isEqualTo(storeFoo);
	}

	@Test
	public void whenGetStorebyId_forNonExistingId_thenReturnNull() throws Exception {
		lenient().when(storeRepository.findById(1L)).thenReturn(Optional.of(storeFoo));
		final Store fetchedStore = storeService.getStoreById(2L);
		assertThat(fetchedStore).isNull();
	}

	@Test
	public void whenFetchStoreList_thenServiceForwardsToCorrectRepositoryMethod() {
		storeService.fetchStoreList();
		verify(storeRepository).findAll();
		verifyNoMoreInteractions(storeRepository);
	}

	@Test
	public void whenUpdateStore_thenServiceForwardsToCorrectRepositoryMethods_thenUpdateName() {
		final Store storeBar = emptyStore(2L, "bar store");
		when(storeRepository.findById(1L)).thenReturn(Optional.of(storeFoo));

		storeService.updateStore(storeBar, 1L);

		assertThat(storeFoo.getName()).isEqualTo(storeBar.getName());
		verify(storeRepository).findById(1L);
		verify(storeRepository).save(storeFoo);
		verifyNoMoreInteractions(storeRepository);
	}

	@Test
	public void whenDeleteStore_thenServiceForwardsToCorrectRepositoryMethod() {
		storeService.deleteStoreById(1L);
		verify(storeRepository).deleteById(1L);
		verifyNoMoreInteractions(storeRepository);
	}
}
