// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.entity.User;
import org.oceanpebble.educc.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserDetailsServiceImpl userDetailsService;

	@Test
	void givenANonExistingUser_whenLoadUserByUsername_thenThrowException() {
		when(userRepository.findByUsername(anyString())).thenReturn(Optional.ofNullable(null));
		assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(""));
	}

	@Test
	void givenAnExistingUser_whenLoadUserByUsername_thenReturnAUserDetailsWithTheUsersInfo() {
		final User user = new User(1L, "username", "password");
		when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

		final UserDetails userDetails = userDetailsService.loadUserByUsername(anyString());

		assertThat(userDetails.getUsername()).isEqualTo("username");
		assertThat(userDetails.getPassword()).isEqualTo("password");
	}
}
