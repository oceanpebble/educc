// SPDX-FileCopyrightText: 2024 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveUserFeedbackRequest;
import org.oceanpebble.educc.dto.SaveUserFeedbackRequestConverter;
import org.oceanpebble.educc.entity.UserFeedback;
import org.oceanpebble.educc.repository.UserFeedbackRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@ExtendWith(MockitoExtension.class)
class UserFeedbackServiceImplTest {

	@Mock
	private UserFeedbackRepository userFeedbackRepository;

	@InjectMocks
	private UserFeedbackServiceImpl userFeedbackService;

	private final SaveUserFeedbackRequestConverter saveUserFeedbackRequestConverter = new SaveUserFeedbackRequestConverter();

	@Test
	public void whenSaveUserFeedback_thenServiceForwardsToCorrectRepositoryMethod() {
		final Clock clock = Clock.fixed(Instant.parse("2024-09-20T14:51:43Z"), ZoneId.of("UTC"));
		final Instant instant = Instant.now(clock);

		try (MockedStatic<Instant> mockedStatic = mockStatic(Instant.class)) {
			mockedStatic.when(Instant::now).thenReturn(instant);
			final SaveUserFeedbackRequest saveUserFeedbackRequest = new SaveUserFeedbackRequest("foo");

			userFeedbackService.saveUserFeedback(saveUserFeedbackRequest);

			final UserFeedback userFeedback = saveUserFeedbackRequestConverter.makeEntityFrom(saveUserFeedbackRequest);
			verify(userFeedbackRepository).save(userFeedback);
			verifyNoMoreInteractions(userFeedbackRepository);
		}
	}

	@Test
	public void whenGetRecentUserFeedback_thenServiceForwardsToCorrectRepositoryMethod() {
		userFeedbackService.getLatestUserFeedback(42);
		verify(userFeedbackRepository).findAll(PageRequest.of(0, 42, Sort.by(Direction.DESC, "timeStamp")));
		verifyNoMoreInteractions(userFeedbackRepository);
	}

	@Test
	public void givenNoFeedbackEntries_whenGetRecentUserFeedback_thenReturnAnEmptyPage() {
		when(userFeedbackRepository.findAll(PageRequest.of(0, 1, Sort.by(Direction.DESC, "timeStamp"))))
				.thenReturn(new PageImpl<>(Collections.emptyList()));

		final Page<UserFeedback> resultPage = userFeedbackService.getLatestUserFeedback(1);

		assertThat(resultPage.getSize()).isZero();
	}

	@Test
	public void givenAtLeastOneFeedbackEntry_whenGetRecentUserFeedback_thenReturnTheResultFromTheRepository() {
		final Page<UserFeedback> feedbackPage = new PageImpl<>(List.of(new UserFeedback(), new UserFeedback()));
		when(userFeedbackRepository.findAll(PageRequest.of(0, 5, Sort.by(Direction.DESC, "timeStamp"))))
				.thenReturn(feedbackPage);

		final Page<UserFeedback> resultPage = userFeedbackService.getLatestUserFeedback(5);

		assertThat(resultPage).isSameAs(resultPage);
	}
}
