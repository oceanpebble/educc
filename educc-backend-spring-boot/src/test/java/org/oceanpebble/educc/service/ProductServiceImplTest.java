// SPDX-FileCopyrightText: 2022 The educc authors
//
// SPDX-License-Identifier: MIT

package org.oceanpebble.educc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyProduct;
import static org.oceanpebble.educc.testutil.TestObjectFactory.emptyTag;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oceanpebble.educc.dto.SaveProductRequest;
import org.oceanpebble.educc.dto.SaveProductRequestConverter;
import org.oceanpebble.educc.entity.Product;
import org.oceanpebble.educc.entity.Store;
import org.oceanpebble.educc.entity.Tag;
import org.oceanpebble.educc.repository.ProductRepository;
import org.oceanpebble.educc.repository.StoreRepository;
import org.oceanpebble.educc.repository.TagRepository;
import org.oceanpebble.educc.testutil.TestObjectFactory;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

	@Mock
	private ProductRepository productRepository;

	@Mock
	private StoreRepository storeRepository;

	@Mock
	private TagRepository tagRepository;

	@InjectMocks
	private ProductServiceImpl productService;

	private final Product productFoo = emptyProduct(1L, "foo product");
	private final SaveProductRequestConverter saveProductRequestConverter = new SaveProductRequestConverter();

	@Test
	public void whenSaveProduct_thenServiceForwardsToCorrectRepositoryMethods() {
		final Store store = TestObjectFactory.emptyStore(1L, "foo store");
		final List<Tag> tags = Arrays.asList(emptyTag(2L, "foo tag"), emptyTag(3L, "bar tag"));
		doReturn(Optional.of(store)).when(storeRepository).findById(any());
		doReturn(tags).when(tagRepository).findAllById(any());

		final SaveProductRequest saveProductRequest = new SaveProductRequest("foo", "bar", 1L, new Long[] { 2L, 3L }, 100,
				200);
		productService.saveProduct(saveProductRequest);

		final Product product = saveProductRequestConverter.makeEntityFrom(saveProductRequest, store, tags);
		verify(productRepository).save(product);
		verify(productRepository).flush();
		verifyNoMoreInteractions(productRepository);
	}

	@Test
	public void whenGetProductById_thenServiceForwardsToCorrectRepositoryMethod() {
		productService.getProductById(1L);
		verify(productRepository).findById(1L);
		verifyNoMoreInteractions(productRepository);
	}

	@Test
	public void whenGetProductbyId_forExistingId_thenReturnTheProduct() throws Exception {
		when(productRepository.findById(1L)).thenReturn(Optional.of(productFoo));
		final Product fetchedProduct = productService.getProductById(1L);
		assertThat(fetchedProduct).isEqualTo(productFoo);
	}

	@Test
	public void whenGetProductbyId_forNonExistingId_thenReturnNull() throws Exception {
		lenient().when(productRepository.findById(1L)).thenReturn(Optional.of(productFoo));
		final Product fetchedProduct = productService.getProductById(2L);
		assertThat(fetchedProduct).isNull();
	}

	@Test
	public void whenFetchProductList_thenServiceForwardsToCorrectRepositoryMethod() {
		productService.fetchProductList();
		verify(productRepository).findAll();
		verifyNoMoreInteractions(productRepository);
	}

	@Test
	public void whenUpdateProduct_thenServiceForwardsToCorrectRepositoryMethods_thenUpdateName() {
		final Product productBar = emptyProduct(1L, "bar product");
		when(productRepository.findById(1L)).thenReturn(Optional.of(productFoo));

		productService.updateProduct(productBar, 1L);

		assertThat(productFoo.getName()).isEqualTo(productBar.getName());
		verify(productRepository).findById(1L);
		verify(productRepository).save(productFoo);
		verifyNoMoreInteractions(productRepository);
	}

	@Test
	public void whenDeleteProduct_thenServiceForwardsToCorrectRepositoryMethod() {
		productService.deleteProductById(1L);
		verify(productRepository).deleteById(1L);
		verifyNoMoreInteractions(productRepository);
	}
}
